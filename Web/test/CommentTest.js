'use strict';

var mongoose = require('mongoose'),
    Comment  = require('../js/models/Comment'),
    User     = require('../js/models/User'),
    request  = require('supertest'),
    assert   = require('chai').assert,
    express  = require('express');

var configDB = require('../config/config.js');

// import the moongoose helper utilities
var utils = require('./utils');
var agent = utils.agent;

describe('Comment with login', function(){
    it('uri that requires user to be logged in', function(done){
        utils.initUser(function() {
            agent
                .post('/comment')
                .send({
                        'fk_user': utils.user()._id, 
                        'fk_item': 'Wally item',
                        'text'   : 'Where is wally?'
                })                   
                .expect(200)
                .end(function(err, res){
                    if (err) return done(err);
                    done();
                });
        });
    });
});

describe('CommentTests', function(){
    beforeEach(function(done){
        utils.initUser(function() {
            createComment(utils.user()._id, 'item_id', 'text', done);
        });
    });

    describe('Comment verification', function(){
        it('Find correctly', function(done){
            Comment.findOne({ fk_user: utils.user()._id}, function(err, item) {
                assert.isNull(err, 'there was no error');
                assert.equal(item.fk_user, utils.user()._id);
                assert.equal(item.fk_item, 'item_id');
                assert.equal(item.text, 'text');
                done();
            });
        });

        it('Wrong text, so long', function(done){
            var longComment = new Comment({
                fk_user: utils.user()._id,
                fk_item: 'item_long',
                text   : 'This comment is so long... I like tomatoes! My name is ralph! I cannot fly because I am a penguin! But I can drill ice with mind...'
            });

            longComment.save(function(err, item) {
                assert.equal(err.errors.message, Comment.ERROR_TEXT_LONG);
                assert.isUndefined(item, 'there was no item, because was wrong');
                
                Comment.findOne({fk_user: utils.user()._id, fk_item: longComment.fk_item}, function(err, item) {
                    assert.isNull(err, 'there was no error');
                    assert.isNull(item, 'there was no item, because was wrong');
                    done();
                });
            });
        });
    });

    describe('Remove comments', function() {
        it('Remove correctly - Owner', function(done) { 
            utils.initUser(function() {
                createComment(utils.user()._id, "id", "text", function(item) {
                    agent
                    .post('/comment/remove/wally/' + utils.user()._id + '/' + item._id)
                    .send()                   
                    .expect(200)
                    .end(function(err, res){
                        if (err) return done(err);
                        done();
                    });
                }, true);
            });
        });
    });

    describe('Get comments', function() {
        beforeEach(function(done){
            createComment(utils.user()._id, 'item_id', 'text-1', function() {
            createComment(utils.user()._id, 'item_id', 'text-2', function() { 
            createComment(utils.user()._id, 'item_id', 'text-3', done)})});
        });

        it('Get all', function(done) { 
            utils.initUser(function() {
                agent
                    .get('/comment/item_id')
                    .send()                   
                    .expect(200)
                    .end(function(err, res){
                        assert.equal(res.body.length, 4); //We obtain 4 comments from item_id
                        if (err) return done(err);
                        done();
                    });
            });
        });
    });

    afterEach(function(done){    
        //delete all the comments records
        Comment.remove({}, function() {   
            done();
        });  
    });
});

function createComment(userId, commentId, text, callback) {
    createComment(userId, commentId, text, callback, false);
}

function createComment(userId, commentId, text, callback, argument) {
    var testItem = new Comment({
        fk_user: userId,
        fk_item: commentId,
        text   : text
    });

    testItem.save(function(err, item) {
        assert.isNull(err, 'there was no error');
        if(argument) callback(item);
        else callback();
    });
}