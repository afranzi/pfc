'use strict';

var mongoose = require('mongoose'),
    Folder = require('../js/models/Folder');

var assert = require('chai').assert;

// import the moongoose helper utilities
var utils = require('./utils');
var agent = utils.agent;

describe('FolderTest', function(){
    var userID = null;

    beforeEach(function(done){
         utils.createTestUser(function(user) {
            userID = user._id;
            createFolder('Italian Coffes', userID, done);
         });
    });

    describe('Tags verification', function(){
        it('Test two tags', function(done){
            Folder.findOne({ owner: userID}, function(err, item) {
                if (err) throw err;
                // test a matching password
                item.tags.push("First tag");
                item.tags.push("Second tag");
                item.save();
                done();
            });
        });
        it('Fail with more than 5 tags', function(done){
            Folder.findOne({ owner: userID}, function(err, item) {
                if (err) throw err;

                // test a matching password
                for(var i = 0; i < 10; ++i) {
                    item.tags.push("Tag: ");
                }

                item.save(function(err) {
                    Folder.findOne({ owner: userID}, function(err, item) {
                        if (err) throw err;

                        assert.lengthOf(item.tags, 0, 'Tags size must be 0');

                        done();
                    });
                });
            });
        });

        it('Test API update', function(done) {
            utils.initUser(function() {
                //console.log(utils.user());
                createFolder("Folder name", utils.user()._id, function(folder) {
                    //console.log(folder);
                    var tags = ['tag1', 'tag2'];
                    agent
                        .post('/tags/folders/' + folder._id)
                        .send(tags)                   
                        .expect(200)
                        .end(function(err, res){
                            Folder.findOne({'_id': folder._id}, function(err, item) {
                                if (err) throw err;
                                assert.equal(item.tags[0], tags[0]);
                                assert.equal(item.tags[1], tags[1]);
                                done();                                
                            });
                        });
                }, true);
            });
        });

        it('Test API update with more than 5 tags', function(done) {
            utils.initUser(function() {
                createFolder("Folder name", utils.user()._id, function(folder) {
                    var tags = ['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6', 'tag7'];
                    agent
                        .post('/tags/folders/' + folder._id)
                        .send(tags)                   
                        .expect(403)
                        .end(function(err, res){
                            Folder.findOne({'_id': folder._id}, function(err, item) {
                                if (err) throw err;
                                assert.lengthOf(item.tags, 0, 'Tags size must be 0');
                                done();                                
                            });
                        });
                }, true);
            });
        });
    });



    afterEach(function(done){    
        //delete all the customer records
        Folder.remove({}, function() {   
            done();    
        });  
    });  
});


function createFolder(name, owner, callback) {
    createFolder(name, owner, callback, false);
}

function createFolder(name, owner, callback, returnItem) {
    var testItem = new Folder({
        owner: owner,
        name: name
    });

    testItem.save(function(err, item) {
        if (err) throw err;
        if(returnItem) {
            callback(item);
        } else {
                callback();
        }
    });
}