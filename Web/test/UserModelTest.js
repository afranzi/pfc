'use strict';

var mongoose = require('mongoose'),
    User = require('../js/models/User');

// import the moongoose helper utilities
var utils = require('./utils');

describe('Connection', function(){

    beforeEach(function(done){
        var testUser = new User({
            local : {
                email   : 'Wally',
                username: 'Wally',
                password: 'Password'
            }
        });

        testUser.save(function(err) {
            if (err) throw err;
            done();
        });
    });

    describe('PasswordVerification', function(){
        it('test a matching password', function(done){

            // fetch user and test password verification
            User.findOne({'local.username': 'Wally' }, function(err, user) {
                if (err) throw err;

                // test a matching password
                user.comparePassword('Password123', function(err, isMatch) {
                    if (err) throw err;
                    done();
                });
            });
        });

        it('test a failing password', function(done){
            // fetch user and test password verification
            User.findOne({'local.username': 'Wally' }, function(err, user) {
                if (err) throw err;

                // test a failing password
                user.comparePassword('123Password', function(err, isMatch) {
                    if (err) throw err;
                    done();
                });                
            });
        });
    });

    afterEach(function(done){    
        //delete all the customer records
        User.remove({}, function() {   
            done();    
        });  
    });  
});