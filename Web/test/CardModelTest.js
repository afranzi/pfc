'use strict';

var mongoose = require('mongoose'),
    Card = require('../js/models/Card');

var assert = require('chai').assert;

// import the moongoose helper utilities
var utils = require('./utils');

describe('CardTests', function(){
    var userID = null;

    beforeEach(function(done){
        utils.createTestUser(function(user) {
            userID = user._id;
            var testItem = new Card({
                owner: user._id,
                address : {
                    street  : 'Av. Diagonal',
                    city    : 'Barcelona',
                    country : 'Catalonia',
                },
                company : 'Wally Wallet',
                email   : 'wally@mail.com',
                phone   : '930000001'
            });

            testItem.save(function(err) {
                if (err) throw err;
                done();
            });
        })
    });

    describe('Email verification', function(){
        it('Wrong mail', function(done){
            Card.findOne({ owner: userID }, function(err, item) {
                if (err) throw err;
                
                item.email = 'mailito.com';
                item.save(function(err) {
                    assert.equal(err.errors.email.message, 'Invalid email');
                    done();
                });
            });
        });
    });

    afterEach(function(done){    
        //delete all the customer records
        Card.remove({}, function() {   
            done();    
        });  
    });  
});