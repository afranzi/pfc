'use strict';

/*
* Modified from https://github.com/elliotf/mocha-mongoose
*/
var config = require('../config/config');
var mongoose = require('mongoose');
var express = require('express');

var User     = require('../js/models/User');

var request  = require('supertest');
var server = request.agent('http://localhost:3002');

process.env.NODE_ENV = 'testing'
process.env.PORT = 3002
var app = require('../app').app;
var agent = request.agent(app);

beforeEach(function (done) {
  function clearDB() {
    for (var i in mongoose.connection.collections) {
      mongoose.connection.collections[i].remove(function() {});
    }
    return done();
  }

  if (mongoose.connection.readyState === 0) {
    mongoose.connect(config.db.test, function (err) {
      if (err) {
        throw err;
      }
      return clearDB();
    });
  } else {
    return clearDB();
  }
});

afterEach(function (done) {
  mongoose.disconnect();
  return done();
});

var cookie;
var user;

var initUser = function(callback) {
  var testUser = new User({
    local : {
      email   : 'test@mail.com',
      username: 'test',
      password: 'test'
    },
  });

  User.exists('test@mail.com', function(exists) {
    if(exists) {
      login(callback);
    } else {
      testUser.save(function(err, item) {
        if (err) throw err; 
        else {
          user = item;
          login(callback);
        }
      });
    }
  });    
}

function login(callback) {
  agent
    .post('/login')
    .field('password', 'test')
    .field('email', 'test@mail.com')
    .expect(302)
    .expect('Location', '/content')
    .end(onResponse);

    function onResponse(err, res) {
      if (err) return callback(err);
      return callback();
    }
};

var getUser = function() {
  return user;
}

var createTestUser = function(callback) {
  var testUser = new User({
    local : {
        email   : Math.random()+'_test@mail.com',
        username: Math.random()+'test',
        password: 'test'
    }
  });

  testUser.save(function(err, newUser) {
      if (err) throw err;
      callback(newUser);
  });
}

exports.initUser = initUser;
exports.user = getUser;
exports.agent = agent;
exports.createTestUser = createTestUser;