'use strict';

var mongoose = require('mongoose'),
    User     = require('../js/models/User'),
    Factory  = require('../js/models/Factory');

// import the moongoose helper utilities
var utils = require('./utils');

describe('Connection', function(){

    it('Create so user', function(done) {
        Factory.generateUsers(2, 1, 10, function(usersToGenerate, foldersGenerate, cardsGenerate) {
            console.log("All done");
            console.log("Total users   : ", usersToGenerate);
            console.log("Total folders : ", foldersGenerate);
            console.log("Total cards   : ", cardsGenerate);
            done();
        });
        
    });

    afterEach(function(done){    
        //delete all the customer records
        User.remove({}, function() {   
            done();    
        });  
    });  
});
