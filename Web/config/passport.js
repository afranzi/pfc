// config/passport.js

// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

// load up the user model
var User       		= require('../js/models/User');

var FB = require('facebook-node-sdk');
var configAuth = require('./auth'); // load the auth variables

// expose this function to our app using module.exports
module.exports = function(passport) {

	// =========================================================================
	// passport session setup ==================================================
	// =========================================================================
	// required for persistent login sessions
	// passport needs ability to serialize and unserialize users out of session

	// used to serialize the user for the session
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// used to deserialize the user
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use(new FacebookStrategy({
		// pull in our app id and secret from our auth.js file
		clientID        : configAuth.facebookAuth.clientID,
		clientSecret    : configAuth.facebookAuth.clientSecret,
		callbackURL     : configAuth.facebookAuth.callbackURL
	 },

	// facebook will send back the token and profile
	function(token, refreshToken, profile, done) {
		var fb = new FB({
			appId: configAuth.facebookAuth.clientID,
			secret:  configAuth.facebookAuth.clientSecret
		}).setAccessToken(token);

		// asynchronous
		process.nextTick(function() {
			// find the user in the database based on their facebook id
			User.findOne({'facebook.id' : profile.id}, function(err, user) {
				// if there is an error, stop everything and return that
				if (err) return done(err);
				else loginUser(fb, user, token, refreshToken, profile, false, done);
			});
		});
	}));

	function loginUser(fb, user, token, refreshToken, profile, isFromMobile, callback) {
		if (user) {	//Refresh info			
			if(isFromMobile) user.facebook.mobileToken = token;
			else user.facebook.token = token;
			if(refreshToken) user.facebook.token = refreshToken;

			if(profile.displayName) user.facebook.name = profile.displayName;
			else user.facebook.name = profile.first_name + ' ' + profile.last_name;

			saveUser(fb, user, callback);
		} else { // if there is no user found with that facebook id, create them
			var newUser = new User();

			// set all of the facebook information in our user model
			newUser.facebook.id    = profile.id; // set the users facebook id                   
			//newUser.facebook.token = token; // we will save the token that facebook provides to the user     
			if(isFromMobile) newUser.facebook.mobileToken = token;
			else newUser.facebook.token = token;

			if(profile.displayName) newUser.facebook.name = profile.displayName;
			else newUser.facebook.name = profile.first_name + ' ' + profile.last_name;
			if(profile.emails) {
				newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
			}
			// save our user to the database
			saveUser(fb, newUser, callback);
		}
	}

	function saveUser(fb, user, done) {
		fb.api('/'+ user.facebook.id +'/picture?redirect=0&height=200&type=normal&width=200', function(err, data) {
			if (data) {
				user.facebook.photo = data.data.url;
			}
			user.save(function(err) {
				if (err) throw err;
				return done(null, user);
			});
		});
	}

	// =========================================================================
	// LOCAL LOGIN =============================================================
	// =========================================================================
	// we are using named strategies since we have one for login and one for signup
	// by default, if there was no name, it would just be called 'local'

	passport.use('local-login', new LocalStrategy({
		// by default, local strategy uses username and password, we will override with email
		usernameField : 'facebookID',
		passwordField : 'token',
		passReqToCallback : true // allows us to pass back the entire request to the callback
	},
	function(req, facebookID, token, done) { // callback with email and password from our form
		// find a user whose email is the same as the forms email
		// we are checking to see if the user trying to login already exists

		var fb = new FB({
			appId: configAuth.facebookAuth.clientID,
			secret:  configAuth.facebookAuth.clientSecret
		}).setAccessToken(token);

		fb.api('/me', function(err, profile) {
			console.log(profile);
			if(profile.id == facebookID) { //Correct facebook id for given token
				User.findOne({ 'facebook.id' :  facebookID}, function(err, user) {
					if (err) return done(err);
					else loginUser(fb, user, token, null, profile, true, done);
				});
			} else {
				return done(null, false, req.flash('message', 'Wrong token'));
			}
		});
	}));
};