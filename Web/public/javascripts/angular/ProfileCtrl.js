var app = angular.module('web', ['ui.bootstrap', 'ngRoute']);

function ProfileCtrl($scope, $http, $window) {

  $scope.userId = null;
  $scope.userInfo = null;
  $scope.showIcons = false;
  $scope.showPass = false;

  $scope.init = function(userId) {
    $scope.userId = userId;
    $scope.getUserInfo(userId);
  }

  $scope.getUserInfo = function(userId) {
    $http.get('/info/user/' + userId ).success(function(info, code) {
      $scope.userInfo = info;
    });
  }

  $scope.toggleIcons = function() {
    $scope.showIcons = !$scope.showIcons;
  }

  $scope.togglePass = function() {
    $scope.showPass = !$scope.showPass;
  }

  $scope.updateIcon = function(num) {
    var json = {
      id : $scope.userId,
      icon : num,
    };

    $http.post('/user/save/', json).success(function(info, code) {
      $scope.userInfo.icon = info.user.icon;
    });
  }

  $scope.removeUser = function() {
    $http.post('/user/remove/' + $scope.userId).success(function(info, code) {
      console.log(info);

      $window.location.href = '/logout';
    }).error(function(err) {
      console.log(err);
      $window.location.href = '/error';
    });
  }

  $scope.updatePassword = function(password) {
    var json = {
      id : $scope.userId,
      password : password
    };

    $http.post('/user/save/password', json).success(function(info, code) {
    });
  }
}