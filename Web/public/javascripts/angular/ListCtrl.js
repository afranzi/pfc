function PageListCtrl($scope, $http ) {

	/**** [START] LIST CTRL *****/
	$scope.getTotalItems = function(userId, parentId) {
		$http.get('/items/count' + buildUrl(userId, parentId))
		.success(function(data, status) {
			$scope.totalCards = data.cardCount;
			$scope.totalFolders = data.totalFolders;
		}).error(function(data, status) {
				console.log(data, status);
		});
	}

	$scope.getCards = function(owner, page, parentId, callback) {
		$http.get('/cards/' + owner + '/' + page + '/' + $scope.pageSize + '/' + parentId).success(function(list, code) {
			$scope.cards = $scope.cards.concat(list);
			++$scope.pagesLoaded;
		});
	}

	$scope.getFolders = function(owner, page, parentId) {
		$http.get('/folders/' + owner + '/' + page + '/' + parentId).success(function(list, code) {
			$scope.folders = list;
		});
	} 

	$scope.pagesLoaded = 0;
	$scope.currentPage = 0;
	$scope.pageSize = 6;
	$scope.nextButton = {color: 'color: #0088cc;'};
	$scope.prevButton = {color: 'color: #0088cc;'};

	$scope.numberOfPages = function(){
		return Math.ceil($scope.totalCards/$scope.pageSize);                
	}

	$scope.correctPage = function() {
		if($scope.numberOfPages() == 0) return 0;
		else return $scope.currentPage + 1;
	}

	$scope.$watch('currentPage + totalCards', function() {
		if($scope.currentPage >= $scope.numberOfPages() - 1) {
			$scope.nextButton = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.nextButton = {color: 'color: #0088cc;'};
		}
		if($scope.currentPage == 0) {
			$scope.prevButton = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.prevButton = {color: 'color: #0088cc;'};
		}

		if($scope.pagesLoaded < $scope.numberOfPages()) {
			$scope.getCards($scope.currentUser, $scope.pagesLoaded, $scope.parentId);
		}
	});
	/**** [ END ] LIST CTRL *****/
}




function buildUrl(userId, parentId) {
	var url = '/' + userId;
	if(parentId)  url += '/' + parentId;
	return url;
}