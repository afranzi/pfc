var app = angular.module('web', ['ui.bootstrap', 'ngRoute']);

function SearchCtrl($scope, $http, $location) {

	$scope.filter = null;
	$scope.items = [];
	$scope.userInfo = null;
	$scope.numItems = 0;

	$scope.init = function(filter) {
		if(!filter) {
			filter = "";
		}

		$scope.filter = decodeURI(filter);
		$scope.getUserResults($scope.filter);	
		$scope.getUserInfo($scope.filter, 0, $scope.pageSizeSearch);
	}

	$scope.getUserInfo = function(filter, page, items) {
		$scope.getItems('/search/users', filter, page, items);
	}

	$scope.getCardsInfo = function(filter, page, items) {
		$scope.getItems('/search/cards', filter, page, items);
	}

	$scope.getTagsInfo = function(filter, page, items) {
		if(!filter) return;
		$scope.getItems('/search/tags', filter, page, items);
	}

	$scope.getItems = function(url, filter, page, pageSize) {
		if(filter) {
			$http.get(url + buildSearchRequest(filter, page, pageSize)).success(function(items, code) {
				$scope.items = $scope.items.concat(items);
				++$scope.pagesLoaded;


				if($scope.pagesLoaded < $scope.numberOfPagesSearch()) {
					$http.get(url + buildSearchRequest(filter, page + 1, pageSize)).success(function(items, code) {  //Paginate 2 pages
						$scope.items = $scope.items.concat(items);
						++$scope.pagesLoaded;
					});
				}
			});
		}
	}

	$scope.getFriendsResults = function(filter) {
		console.log("GetFriendsResults");
		$scope.getItemsCount('/search/friends/count/', filter);
	}

	$scope.getUserResults = function(filter) {
		console.log("GetUserResults");
		$scope.getItemsCount('/search/users/count/', filter);
	}

	$scope.getCardsResults = function(filter) {
		$scope.getItemsCount('/search/cards/count/', filter);
	}

	$scope.getTagsResults = function(filter) {
		$scope.getItemsCount('/search/tags/count/', filter);
	}

	$scope.getItemsCount = function(url, filter) {
		if(filter) {
			$http.get(url + encodeURIComponent(filter)).success(function(data, code) {
				$scope.numItems = data.items;
			});
		}
	}

	$scope.searchPanel = {
		icon : 'user-icon-64.png',
		text : 'Users',
		type : 1
	}

	$scope.searchUsers = function() {
		$scope.searchPanel = {
			icon : 'user-icon-64.png',
			text : 'Users',
			type : 1
		}
		$scope.changeSearch(1, $scope.filter);
	}

	$scope.searchCards = function() {
		$scope.searchPanel = {
			icon : 'folder-open-64.png',
			text : 'Cards',
			type : 2
		}
		$scope.changeSearch(2, $scope.filter);
	}

	$scope.searchTags = function() {
		$scope.searchPanel = {
			icon : 'Bookmarks-icon-48.png',
			text : 'Tags',
			type : 3
		}		
		$scope.changeSearch(3, $scope.filter);	
	}

	$scope.changeSearch = function(type, filterToSearch) {
		$scope.items = [];
		$scope.filter = filterToSearch;
		$scope.pagesLoaded = 0;
		$scope.currentPageSearch = 0;
		$scope.getMoreResults(type);
	}

	$scope.getMoreResults = function(type) {
		switch(type) {
			case 1:
				$scope.getUserResults($scope.filter);	
				$scope.getUserInfo($scope.filter, $scope.pagesLoaded, $scope.pageSizeSearch);
				break;
			case 2:
				$scope.getCardsResults($scope.filter);	
				$scope.getCardsInfo($scope.filter, $scope.pagesLoaded, $scope.pageSizeSearch);
				break;
			case 3:
				$scope.getTagsResults($scope.filter);	
				$scope.getTagsInfo($scope.filter, $scope.pagesLoaded, $scope.pageSizeSearch);
				break;
		}
	}

	$scope.pagesLoaded = 0;
	$scope.currentPageSearch = 0;
	$scope.pageSizeSearch = 21;
	$scope.nextButtonSearch = {color: 'color: #0088cc;'};
	$scope.prevButtonSearch = {color: 'color: #0088cc;'};

	$scope.numberOfPagesSearch = function(){
		return Math.ceil($scope.numItems/$scope.pageSizeSearch);                
	}

	$scope.correctPageSearch = function() {
		if($scope.numberOfPagesSearch() == 0) return 0;
		else return $scope.currentPageSearch + 1;
	}

	$scope.changePageSearch = function(increment) {
		$scope.currentPageSearch += increment;

		if($scope.currentPageSearch < 0) $scope.currentPageSearch = 0;
		else if($scope.currentPageSearch > $scope.numberOfPagesSearch()) $scope.currentPageSearch = $scope.numberOfPagesSearch();


		console.log("watch", $scope.numItems, $scope.pageSizeSearch,$scope.currentPageSearch );

		if($scope.currentPageSearch >= $scope.numberOfPagesSearch() - 1) {
			$scope.nextButtonSearch = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.nextButtonSearch = {color: 'color: #0088cc;'};
		}
		if($scope.currentPageSearch == 0) {
			$scope.prevButtonSearch = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.prevButtonSearch = {color: 'color: #0088cc;'};
		}

		if($scope.pagesLoaded < $scope.numberOfPagesSearch()) {
			$scope.getMoreResults($scope.searchPanel.type);
		}
	};


	$scope.getCardImage = function(card) {
		if(card.guy.name) {
			return "info-icon";
		} else {
			return "info-company";
		}
	}

}

function buildSearchRequest(search, page, items) {
	var request = '/' + page + '/' + items + '/' + encodeURIComponent(search);
	return request;
}