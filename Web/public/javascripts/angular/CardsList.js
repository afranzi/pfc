function CardsListsCtrl($scope, $http, $location, $window) {

	$scope.cards = []; 
	$scope.user; 		//Our user id
	$scope.currentUser; //User id whom we are looking his page.
	$scope.parentId;
	$scope.parentFolder = {name: "Home", _id: ""};
	$scope.currentFolder = null;
	$scope.userInfo = null;
	$scope.toSearch = '';

	$scope.errorMsg = '';
	$scope.isProblem = false;
	$scope.successMsg = '';
	$scope.isSuccess = false;

	$scope.initList = function(userId, currentUser, parentId, granpaId) {
		$scope.user = userId;
		$scope.currentUser = currentUser;
		$scope.parentId = parentId;

		if(granpaId) {
			$scope.getFolderInfo(currentUser, granpaId, function(folder) {
				if(folder) $scope.parentFolder = folder;
			});
		} else {
			$scope.getRootInfo(currentUser, true);
		} 
		if(parentId) {
			$scope.getFolderInfo(currentUser, parentId, function(folder) {
				if(folder) $scope.currentFolder = folder;
			});
		} else {
			$scope.getRootInfo(currentUser, false);
		}

		$scope.getTotalItems(currentUser, parentId);
		$scope.refresh();
	}

	$scope.closeAlert = function() {
		$scope.isProblem = false;
	}

	$scope.closeSuccess= function() {
		$scope.isSuccess = false;
	}

	$scope.setAlert = function(msg) {
		console.log("SetAlert", msg);
		$scope.errorMsg = msg;
		$scope.isProblem = true;
	}

	$scope.setSuccess = function(msg) {
		console.log("setSuccess", msg);
		$scope.successMsg = msg;
		$scope.isSuccess = true;
	}

	$scope.refresh = function() {
		$scope.pagesLoaded = 0;
		$scope.pagesLoadedFolder = 0;
		$scope.cards = [];
		$scope.folders = [];

		$scope.getCards($scope.currentUser, 0, $scope.parentId);
		$scope.getFolders($scope.currentUser, 0, $scope.parentId);
		$scope.getUserInfo($scope.currentUser);
	}

	$scope.initSearch = function(userId) {
		$scope.user = userId;
	}

	$scope.getUserInfo = function(userId) {
		$http.get('/info/user/' + userId ).success(function(info, code) {
			$scope.userInfo = info;
		});
	}

	$scope.getFolderInfo = function(owner, folderId, callback) {
		$http.get('/folder/' + owner + '/' + folderId).success(function(folder, code) {
			callback(folder);
		}).error(function() {
			$scope.setAlert("Cannot get info about folder with id " + folderId);
		});
	}

	$scope.getRootInfo = function(owner, isRoot) {
		$http.get('/rootinfo/' + owner ).success(function(info, code) {
			if(!info) return;
			if(isRoot) {
				$scope.parentFolder.foldersCount = info.foldersCount;
				$scope.parentFolder.cardsCount = info.cardsCount;
			} else {
				$scope.currentFolder = {
					name         : "Home",
					foldersCount : info.foldersCount,
					cardsCount   : info.cardsCount
				}
			}
		}).error(function() {
			$scope.setAlert("Cannot get info about user " + owner);
		});;
	}

	$scope.createCard = function(card, callback) {
		$http.post('/cards', card).success(callback).error(callback);
	};

	$scope.updateCard = function(card, callback) {
		$http.post('/cards/save', card).success(callback).error(callback);
	};


	$scope.createFolder = function(folder, callback) {
		$http.post('/folders', folder).success(callback).error(callback);
	};

	$scope.itemFactory = {
		getters : {
			getCards : $scope.getCards,
			getFolders: $scope.getFolders,
			refresh: $scope.refresh
		},
		builders : {
			createCard : $scope.createCard,
			createFolder : $scope.createFolder,
			updateCard : $scope.updateCard
		},
		setAlert : $scope.setAlert
	}


	$scope.cardShowed = {};
	$scope.commentsShowed = null;

	$scope.showCard = function(card) {
		$scope.cardShowed = card;
		$scope.commentsShowed = null;
		//TODO: refresh Comments

		$scope.getComments();
	}

	$scope.deleteCard = function() {
		$http.post('/cards/delete/' + $scope.cardShowed._id)
			.success(function(data, status) {
				$scope.setSuccess("Card removed successfuly");
				$scope.refresh();
		}).error(function(data, status) {
			$scope.setAlert("Cannot delete this card " + $scope.cardShowed._id + " - " + data);
		});
	}

	$scope.import = function(withRefresh) {
		$http.post('/card/import/' + $scope.cardShowed._id + '/' + $scope.cardShowed.owner)
			.success(function(data, status) {
				var name = ($scope.cardShowed.guy.name) ? $scope.cardShowed.guy.name : $scope.cardShowed.company; 
				$scope.setSuccess('Card ' + name + ' imported correctly.');
				if(withRefresh) $scope.refresh();
		}).error(function(data, status) {
			$scope.setAlert("Cannot import this card " + $scope.cardShowed._id + " - " + data);
		});
	}

	$scope.folderToDelete = null;

	$scope.setFolderToDelete = function(folder) {
		$scope.folderToDelete = folder;
	}

	$scope.deleteFolder = function(folderToDelete) {
		if(folderToDelete != null && folderToDelete != undefined && folderToDelete.foldersCount <= 0) {
			$http.post('/folders/delete/' + folderToDelete._id)
				.success(function(data, status) {
					$scope.setSuccess("Folder " + folderToDelete.name +" removed successfuly");
					$scope.refresh();
			}).error(function(data, status) {
				$scope.setAlert("Cannot delete this folder " + folderToDelete + " - " + data);
			});
		}
	}

	$scope.saveCard = function() {
		$scope.updateCard($scope.cardShowed, function(msg, code) {
			if(code == 200) {
			} else {
				$scope.setAlert("Cannot save this card, " + msg);
			}
		});
	}

	$scope.modifyTags = function(event) {
		if(event.which === 13) { //Enter event
			var tagsTmp = [];
			if($scope.tagsInput) {
				tagsTmp = removeInvalidChars($scope.tagsInput);

				var oldTags = $scope.cardShowed.tags;
				tagsTmp = tagsTmp.filter(function(elem, pos) {
					return tagsTmp.indexOf(elem) == pos;
				});

				$scope.tagsInput = tagsTmp.toString();
			}  

			$http.post('/tags/cards/' + $scope.cardShowed._id, tagsTmp)
			.success(function(data, status) {
				$scope.cardShowed.tags = data.card.tags;
			}).error(function(data, status) {
				$scope.cardShowed.tags = oldTags;
				$scope.setAlert("Cannot uptade card tags, " + data);
			});
		}
	};


	$scope.comment = "";

	$scope.createComment = function(comment) {
		var value = $scope.comment.toString().trim().substr(0,120);
		var newComment = {
				fk_item: $scope.cardShowed._id,
				text   : value
		};

		$http.post('/comment', newComment)
			.success(function(data, status) {
				$scope.cardComments.push(data);
				$scope.comment = "";
			}).error(function(data, status) {
				$scope.setAlert("Cannot comment...");
			});
	}

	$scope.deleteComment = function(comments) {
		$http.post('/comment/remove/' + $scope.currentUser + '/' + comments.fk_user + '/' + comments._id)
			.success(function(data, status) {      
				// Find and remove item from an array
				var i = $scope.cardComments.indexOf(comments);
				if(i != -1) {
					$scope.cardComments.splice(i, 1);
				}
		}).error(function(data, status) {
				$scope.setAlert("Cannot remove comment...");
		});

		//TODO: Delete comment in DBB
	}

	$scope.cardComments = new Array();
	$scope.getComments = function() {
		$http.get('/comment/' + $scope.cardShowed._id)
		.success(function(data, status) {
			$scope.cardComments = data;
		}).error(function(data, status) {
			$scope.setAlert("Cannot get comments from this card.");
		});
	}

	$scope.equalToZero = function(number) {
		return number == 0;
	}

	$scope.buildFolderURL = function(folderId, parentId) {
		var url = "/";
		if(folderId) {
			url += folderId;
			if(parentId) url += "/" + parentId;
		}
		return url;
	}

	$scope.search = function(toSearch) {
		$window.location.href = '/search/'+toSearch;
	}

	/**** [START] LIST CTRL *****/
	$scope.getTotalItems = function(userId, parentId) {
		$http.get('/items/count' + buildUrl(userId, parentId))
		.success(function(data, status) {
			$scope.totalCards = data.cardCount;
			$scope.totalFolders = data.folderCount;
		}).error(function(data, status) {

		});
	}

	$scope.getCards = function(owner, page, parentId) {	
		$http.get('/cards/' + owner + '/' + page + '/' + $scope.pageSize + '/' + parentId).success(function(list, code) {
			$scope.cards = $scope.cards.concat(list);
			++$scope.pagesLoaded;

			if($scope.pagesLoaded < $scope.numberOfPages()) { //Load two pages so we can navigate directly to the next page 
				$http.get('/cards/' + owner + '/' + (page + 1) + '/' + $scope.pageSize + '/' + parentId).success(function(list, code) {
					$scope.cards = $scope.cards.concat(list);
					++$scope.pagesLoaded;
				});
			}
		});
	}

	$scope.getFolders = function(owner, page, parentId) {
		$http.get('/folders/' + owner + '/' + page + '/' + $scope.pageSize + '/' + parentId).success(function(list, code) {
			$scope.folders = $scope.folders.concat(list);
			++$scope.pagesLoadedFolder;

			if($scope.pagesLoadedFolder < $scope.numberOfPagesFolder()) {
				$http.get('/folders/' + owner + '/' + (page + 1) + '/' + $scope.pageSize + '/' + parentId).success(function(list, code) {
					$scope.folders = $scope.folders.concat(list);
					++$scope.pagesLoadedFolder;
				});
			}
		});
	} 

	$scope.pagesLoaded = 0;
	$scope.currentPage = 0;
	$scope.pageSize = 6;
	$scope.nextButton = {color: 'color: #0088cc;'};
	$scope.prevButton = {color: 'rgb(179, 179, 179)'};

	$scope.numberOfPages = function(){
		return Math.ceil($scope.totalCards/$scope.pageSize);                
	}

	$scope.correctPage = function() {
		if($scope.numberOfPages() == 0) return 0;
		else return $scope.currentPage + 1;
	}

	$scope.changePage = function(increment) {
		$scope.currentPage += increment;

		if($scope.currentPage >= $scope.numberOfPages() - 1) {
			$scope.nextButton = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.nextButton = {color: 'color: #0088cc;'};
		}
		if($scope.currentPage == 0) {
			$scope.prevButton = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.prevButton = {color: 'color: #0088cc;'};
		}

		if($scope.pagesLoaded < $scope.numberOfPages()) {
			$scope.getCards($scope.currentUser, $scope.pagesLoaded, $scope.parentId);
		}
	};
	/**** [ END ] LIST CTRL *****/

	$scope.pagesLoadedFolder = 0;
	$scope.currentPageFolder = 0;
	$scope.nextButtonFolder = {color: 'color: #0088cc;'};
	$scope.prevButtonFolder = {color: 'rgb(179, 179, 179)'};

	$scope.numberOfPagesFolder = function(){
		return Math.ceil($scope.totalFolders/$scope.pageSize);                
	}

	$scope.correctPageFolder = function() {
		if($scope.numberOfPagesFolder() == 0) return 0;
		else return $scope.currentPageFolder + 1;
	}

	$scope.changePageFolder = function(increment) {
		$scope.currentPageFolder += increment;
		if($scope.currentPageFolder >= $scope.numberOfPagesFolder() - 1) {
			$scope.nextButtonFolder = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.nextButtonFolder = {color: 'color: #0088cc;'};
		}
		if($scope.currentPageFolder == 0) {
			$scope.prevButtonFolder = {color: 'rgb(179, 179, 179)'};
		}
		else {
			$scope.prevButtonFolder = {color: 'color: #0088cc;'};
		}

		if($scope.pagesLoadedFolder < $scope.numberOfPagesFolder()) {
			$scope.getFolders($scope.currentUser, $scope.pagesLoadedFolder, $scope.parentId);
		}
	};

	$scope.isOwner = function() {
		return $scope.currentUser == $scope.user;
	}

	$scope.isOurs = function(userId) {
		return $scope.user == userId;
	}

	$scope.isOwnerOrItsOurs = function(userId) {
		return $scope.isOwner() || $scope.isOurs(userId);
	}

}

var deleteSpecialChars = new RegExp(/[\.,-\/+#!$%€?¿<>…"“”‘’—–\^&\*;:{}=\-_`~()]/g);

//"This., -/ is #! an $ % ^ & * example ;: {} of a = -_ string with `~)() punctuation"
function removeInvalidChars(sentence) {
	return sentence.toLowerCase().replace(deleteSpecialChars, ' ').replace(/\s{2,}/g,' ').trim().split(' ')
}

function buildUrl(userId, parentId) {
	var url = '/' + userId;
	if(parentId)  url += '/' + parentId;
	return url;
}

