'use strict';


angular.module('index', [
    'ngRoute',
    'index.controllers'
]).
config(function ($routeProvider, $locationProvider) {
  $routeProvider.
    when('/', {
      templateUrl: 'partials/index',
      controller: 'HomeController'
    }).
    when('/login', {
      templateUrl: 'partials/login',
      controller: 'HomeController'
    }).
    otherwise({
      redirectTo: '/signup'
    });
 
  $locationProvider.html5Mode(true);
});

angular.module('index.controllers', []).
  controller('HomeController', function ($scope) {
 
  });

// var todoApp = angular.module('index', ['ngRoute'])
//   .config(['$routeProvider', function($routeProvider, $locationProvider) {
//       $routeProvider
//       .when('/', {
//         templateUrl: 'partials/index', controller: 'UsersCtrl'}
//       )
//       .otherwise({ redirectTo: '/' });

//       $locationProvider.html5Mode(true);
//   }]);

// todoApp.controller('UsersCtrl', function($scope) {
//     $scope.users = [];
// });

// todoApp.controller('TodoCtrl', function($scope) {
//     $scope.todos = [];
// });

// var todoApp = angular.module('index', ['ngRoute']);

// todoApp.config(['$routeProvider', function($routeProvider) {
//     $routeProvider
//     when('/', {
//       templateUrl: 'views/index',
//       controller: IndexCtrl
//     }).
//     when('/login', {
//       templateUrl: 'views/login',
//       controller: IndexCtrl
//     }).
//     when('/signup', {
//       templateUrl: 'views/index',
//       controller: IndexCtrl
//     }).
//     otherwise({
//       redirectTo: '/'
//     });
// }]);

// function IndexCtrl($scope) {
//   console.log("HELLO");
// }

// /*global angular */
// define(['angular'], function() {

//   var app = angular
//   .module('app', ['index'])

//   .config(['$routeProvider', function($routeProvider) {
//     $routeProvider
//     when('/', {
//       templateUrl: 'views/index',
//       controller: IndexCtrl
//     }).
//     when('/login', {
//       templateUrl: 'views/login',
//       controller: IndexCtrl
//     }).
//     when('/signup', {
//       templateUrl: 'views/index',
//       controller: IndexCtrl
//     }).
//     otherwise({
//       redirectTo: '/'
//     });

//   });


//   app.controller('IndexCtrl', ['$scope',
//     function IndexCtrl($scope) {

//       console.log("HELLO");
//     }
    
//   ]);

//   return app;
// });