var app = angular.module('web', ['ui.bootstrap', 'ngRoute']);

/*
This directive allows us to pass a function in on an enter key to do what we want.
 */
app.directive('ngEnter', function () {
	return function (scope, element, attrs) {
		element.bind("keydown keypress", function (event) {
			if(event.which === 13) {
				scope.$apply(function (){
					scope.$eval(attrs.ngEnter);
				});

				event.preventDefault();
			}
		});
	};
});

app.filter('startFrom', function() { // Filter used at lists to specify position which starts
	return function(input, start) {         
			return input.slice(start);
	}
});

app.filter('range', function() {
	return function(input, total) {
		total = parseInt(total);
		for (var i = 1; i <= total; ++i)
			input.push(i);
		return input;
	};
});