var app = angular.module('web', ['ui.bootstrap', 'ngRoute']);

var ModalCreateItems = function ($scope, $modal, $log, $http) {

  $scope.open = function (itemFactory) {
    $scope.modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      backdrop: true,
      windowClass: 'modal',
      controller: function ($scope, $modalInstance, $log, $routeParams) {       

        $scope.submit = function () {
          $modalInstance.close('submit');
          
          $scope.createFolder();
        }

        $scope.cancel = function () {
          $modalInstance.close('cancel');
        };

        $scope.folder = {
          name : ''
        };

        $scope.card = {
          adress : {
            street : '',
            city : '',
            country : ''
          },
          company : '',
          email : '',
          phone : '',
          guy : {
            name : '',
            appointmen : ''
          },
          owner: ''
        }; 

        $scope.createCard = function(parentID) {
          if(parentID != null && parentID != undefined) {
            $scope.card.parent = parentID;
          }

          itemFactory.builders.createCard($scope.card, function(msg, code) {
            if(code == 200) {
              itemFactory.getters.refresh();
            } else {
              itemFactory.setAlert("Cannot create this card, " + msg);
            }
            $modalInstance.close('submit');
          });
        };

        $scope.createFolder = function(parentID) {
          if(parentID != null && parentID != undefined) {
            $scope.folder.parent = parentID;
          }

          itemFactory.builders.createFolder($scope.folder, function(msg, code) {
            if(code == 200) {
              itemFactory.getters.refresh();
            } else {
              itemFactory.setAlert("Cannot create this folder, " + msg);
            }
            $modalInstance.close('submit');
          });
        };
      },
      resolve: {
        user: function () { 
          console.log("User resolve");
          return $scope.user;
        }
      }
    });
  };
}; 
