
var Card    = require('../js/models/Card'),
	Folder  = require('../js/models/Folder'),
	Comment = require('../js/models/Comment'),
	User    = require('../js/models/User'),
	Factory = require('../js/models/Factory'),
	async   = require("async");

module.exports = function(app, passport) {
	//TODO: afegir isLoggedIn a totes les funcions.

	var pageSize = 20;

	app.get('/error', function(req, res) {
		res.render('error.ejs');
	});

	/** [START] Cards **/

	app.post('/cards', isLoggedIn, function (request, response) {
		var cardJson = request.body;
		cardJson.owner = request.user.facebook.id;

		var newCard = new Card(cardJson);
		newCard.save(function(err) {
			if (err) {
				response.send(403);
			} 
			else {		
				response.send(200);
			}
		});
	});

	app.post('/cards/save', isLoggedIn, function (request, response) {
		var cardJson = request.body;
		cardJson.owner = request.user.facebook.id;

		Card.findById(cardJson._id, cardJson.owner, function(err, item) {
			var newCard = new Card(cardJson);

			Card.updateItem(cardJson._id, newCard, function(err, item) {
				if (err) {
					response.send(403);
				} 
				else {
					response.status(200).send({card: newCard});
				}
			});
		});		
	});

	app.post('/cards/delete/:id', isLoggedIn, function (request, response) {
		var ownerId = request.user.facebook.id;
		var cardId = request.params.id;

		Card.findById(cardId, ownerId, function(err, item) {
			if(isNull(item)) {
				response.send(400);
			}
			if(item.owner == ownerId) {
				item.remove(function (err, item) {
					if(err) response.send(403);
					else response.send(200);
				});
			} else {
				response.status(403).send("User not allowed to remove this card");
			}
		});		
	});

	/** [ END ] Cards **/

	/** [START] Folders **/	

	app.post('/folders', isLoggedIn, function (request, response) {
		var folderJson = request.body;
		folderJson.owner = request.user.facebook.id;
		var newFolder = new Folder(folderJson);
		newFolder.save(function(err) {
			if (err) {
				response.send(403);
			} 
			else {		
				response.send(200);
			}
		});
	});

	app.post('/folders/save', isLoggedIn, function (request, response) {
		var folderJson = request.body;
		folderJson.owner = request.user.facebook.id;

		Folder.findById(folderJson._id, folderJson.owner, function(err, item) {
			item.name = folderJson.name

			Folder.updateItem(folderJson._id, item, function(err, item) {
				if (err) {
					response.send(403);
				} 
				else {
					response.status(200).send({folder: item});
				}
			});
		});		
	});

	app.post('/folders/delete/:id', isLoggedIn, function (request, response) {
		var ownerId = request.user.facebook.id;
		var folderId = request.params.id;

		Folder.findById(folderId, ownerId, function(err, item) {
			if(isNull(item)) {
				response.send(400);
			}
			if(item.owner == ownerId) {
				item.remove(function (err, item) {
					if(err) response.send(403);
					else { //Remove each card with his own comments.
						Card.find({'parent': folderId}, function(err, cardsToRemove)  {
							async.each(cardsToRemove,
							  function(card, callback){
								card.remove(function (err, item){
								  if(err) callback(err);
								  else callback();
								});
							  },
							  function(err) { // All tasks are done now
								if(err) response.status(403).send("Someting wrong: ", err);
								else response.send(200);
							  }
							);
						});
					}
				});
			} else {
				response.status(403).send("User not allowed to remove this folder");
			}
		});		
	});

	/** [ END ] Folders **/

	/** [START] Comments**/

	/* Remove comentari */
	app.post('/comment/remove/:owner/:publisher/:id', isLoggedIn, function (request, response) {
		var userID = request.user.facebook.id;
		if(userID == request.params.owner || userID == request.params.publisher) {
			Comment.findById(request.params.id, function(comment) {
				if(isNull(comment)) {
					response.send(400);
				}
				comment.remove(function (err, comment) {
					if(err) response.send(403);
					else response.send(200);
				});
			})
		} else {
			response.send(403);
		}
	});

	/* Obté comentari */
	app.get('/comment/:id', isLoggedIn, function (request, response) {
		var containerId = request.params.id;
		if(containerId != null && containerId != undefined) {
			Comment.findAllById(containerId, function (comments) {
				response.send(comments);
			});
		} else {
			response.send(403);
		}
	});

	/* Crea comentari */
	app.post('/comment', isLoggedIn, function (request, response) {
		var commentJson = request.body;
		commentJson.fk_user = request.user.facebook.id;

		var newComment = new Comment(commentJson);
		newComment.save(function(err, comment) {
			if (err) {
				response.send(403);
			} else {		
				response.send(comment).status(200);
			}
		});
	});

	/** [ END ] Comments **/
	/** [START] Tags **/

	app.post('/tags/cards/:id', isLoggedIn, function (request, response) {
		var tags = request.body;

		if(isNull(tags)) {
			response.send(400);
		}

		Card.updateTags(request.params.id, request.user.facebook.id, tags, function(err, result) {
			if (err) {
				response.status(403).send({card: result, error: err});
			} else {		
				response.status(200).send({card: result});
				//response.send(200);
			}
		})
	});

	app.post('/tags/folders/:id', isLoggedIn, function (request, response) {
		var tags = request.body;

		if(isNull(tags)) {
			response.send(400);
		}

		Folder.updateTags(request.params.id, request.user.facebook.id, tags, function(err, result) {
			if (err) {
				response.send(403);
			} else {
				response.send(200);
			}
		})
	});

	/** [START] GETTERS CARDS & FOLDERS **/

	app.get('/items/count/:owner', isLoggedIn, function (req, res) { //Return how many cards has one folder
		var owner = req.params.owner;
		var query = {'owner': owner, 'parent': null};
		getItemsCounts(req, res, query);
	});

	app.get('/items/count/:owner/:parent', isLoggedIn, function (req, res) { //Return how many cards has one folder
		var owner = req.params.owner;
		var parent = req.params.parent;

		var query = {'owner': owner, 'parent': parent};
		getItemsCounts(req, res, query);
	});

	function getItemsCounts(req, res, query) {
		Card.count(query, function(err, cardCount) {
			Folder.count(query, function(err, folderCount) {
				if(err) res.status(403).send(err);
				else res.status(200).send({
					cardCount : cardCount,
					folderCount : folderCount
				});
			});
		});
	}

	app.post('/card/import/:id/:owner', isLoggedIn, function (req, res) {
		var currentUser = req.user.facebook.id;

		Card.findById(req.params.id,  req.params.owner, function(err, card) {
			if(card == null) return res.status(403).send("Card not found, cannot import this target");
			if(err) return res.status(403).send("Cannot import this target");
			else {
				Folder.findOne({'owner' : currentUser, 'parent' : null, 'name' : 'Library'}, null, function(err, folder) {
					if(err) return res.status(403).send("Cannot import this target");
					else if(folder == null || folder == undefined) { //We must to create new library folder to import cards from other users

						var newFolder = new Folder({owner : currentUser, name : 'Library', parent : null, cardsCount: 1});
						User.modifyCardCounter(currentUser, 1, function(err, user) {
							console.log("user");
						});
						newFolder.save(function(err, libraryFolder) {
							if (err) return res.status(403).send("Cannot import this target");
							else {
								delete card._id;
								card.owner = currentUser;
								card.parent = libraryFolder._id;
								card.save(function(err, card) {
									if (err) {
										res.send(403);
									} 
									else {		
										res.send(200);
									}
								});
							}
						});
					}
					else {
						var tmpCard = card.toObject(); // swap for a plain javascript object instance	
						delete tmpCard._id;
						delete tmpCard.__v;
						tmpCard.owner = currentUser;
						tmpCard.parent = folder._id;

						var duplicate = new Card(tmpCard);
						duplicate.save(function(err, card) {
							if (err) {
								res.send(403);
							} 
							else {		
								res.send(200);
							}
						});
					}
				});
			}
		})

	});

	app.get('/cards/:owner/:page/:items', isLoggedIn, function (req, res) {
		getCards(req, res);
	});

	app.get('/cards/:owner/:page/:items/:parent', isLoggedIn, function (req, res) {
		getCards(req, res);
	});

	app.get('/folders/:owner/:page/:items', isLoggedIn, function (req, res) {	    
		getFolders(req, res);
	});

	app.get('/folders/:owner/:page/:items/:parent', isLoggedIn, function (req, res) {	    
		getFolders(req, res);
	});

	app.get('/folder/:owner/:folderId', isLoggedIn, function (req, res) {	    
		var folderId = req.params.folderId;
		var owner = req.params.owner;

		Folder.findOne( {"owner": owner, "_id": folderId}, null, function(err, folder) {
			res.send(folder);
		});
	});

	app.get('/rootinfo/:owner', isLoggedIn, function(req, res) {
		var owner = req.params.owner;

		Folder.count({"owner": owner, "parent": null}, function(err, folderCount) {
			if(err) res.status(403).send("Cannot get folder counts");
			else {
				Card.count({"owner": owner, "parent": null}, function(err, cardCount) {
					if(err) res.status(403).send("Cannot get card counts");
					res.status(200).send({foldersCount: folderCount, cardsCount: cardCount})
				});
			}
		});
	});

	function getCards(request, response) {
		var page = parseInt(request.params.page);
		var parent = request.params.parent;
		var items = parseInt(request.params.items);

		var query = getCardFolderQuery(request.params.owner, parent);
		Card.find(query, null, {skip: page*items, limit: items, sort: {company: 1}}, function(err, cards) {
			response.send(cards);
		});
	}

	function getFolders(request, response) {
		var page = parseInt(request.params.page);
		var parent = request.params.parent;
		var items = parseInt(request.params.items);

		var query = getCardFolderQuery(request.params.owner, parent);

		Folder.find(query, null, {skip: page*items, limit: items, sort: {name: 1}}, function(err, folders) {
			response.send(folders);
		});
	}

	/** [ END ] GETTERS CARDS & FOLDERS **/
}

function getCardFolderQuery(owner, parent) {
	var query = {"owner": owner};
	if(parent != null && parent != undefined && parent != '') {
		query['parent'] = parent;
	} else {
		query['parent'] = null;
	}
	return query;
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();

	res.status(400).send("Wrong acction");
}

function isNull(item) {
	return (item === null || item === undefined);
}