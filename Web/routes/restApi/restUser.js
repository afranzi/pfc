
var Card    = require('../../js/models/Card'),
	Folder  = require('../../js/models/Folder'),
	Comment = require('../../js/models/Comment'),
	User    = require('../../js/models/User'),
	Factory = require('../../js/models/Factory'),
	async   = require("async");

var FB = require('facebook-node-sdk');
var configAuth = require('../../config/auth'); // load the auth variables

module.exports = function(app, passport) {
	//TODO: afegir isLoggedIn a totes les funcions.

	/** [START] Factory **/

	app.get('/factory/user/:userID/:folders/:cards', isLoggedIn, function (request, response) {
		var numFolders = request.params.folders;
		var numCards   = request.params.cards;
		var userId     = request.params.userID;

		Factory.generateThings(userId, numFolders, numCards, function(foldersGenerate, cardsGenerate) {
            console.log("All done");
            console.log("Total folders : ", foldersGenerate);
            console.log("Total cards   : ", cardsGenerate);
            response.status(200).send({
            	foldersGenerate : foldersGenerate,
            	cardsGenerate   : cardsGenerate
            });
        });
	});

	app.get('/factory/:users/:folders/:cards', isLoggedIn, function (request, response) {
		var numUsers   = request.params.users;
		var numFolders = request.params.folders;
		var numCards   = request.params.cards;

		Factory.generateUsers(numUsers, numFolders, numCards, function(usersToGenerate, foldersGenerate, cardsGenerate) {
            console.log("All done");
            console.log("Total users   : ", usersToGenerate);
            console.log("Total folders : ", foldersGenerate);
            console.log("Total cards   : ", cardsGenerate);
            response.status(200).send({
            	usersToGenerate : usersToGenerate,
            	foldersGenerate : foldersGenerate,
            	cardsGenerate   : cardsGenerate
            });
        });
	});



	app.get('/factory/clear', isLoggedIn, function (request, response) {
		User.remove({'dummy': true}, function(err) {
			Folder.remove({'dummy': true}, function(err) {
				Card.remove({'dummy': true}, function(err) {
					if(err) response.status(304).send("Cannot clear dummy users");
					response.send(200);
				});
			});
		});
	});

	app.post('/user/remove/:userID', isLoggedIn, function (request, response) {
		var userId = request.params.userID;
		var currentUser = request.user.facebook.id;

		if(userId == currentUser) {
			User.findByID(userId, function(err, user) {
				if(err || !user) response.status(400).send('Doesn\'t exist this user!');
				else {
					Folder.remove({'owner': userId}, function(err) {
						if(err) response.status(400).send('Cannot remove folder from user ' + userId);
						else {
							Card.remove({'owner': userId}, function(err) {
								if(err) response.status(400).send('Cannot remove cards from user ' + userId);
								else {
									Comment.remove({$or : [{'fk_user': userId}, {'publisher': userId}]}, function(err) {
										var fb = new FB({
											appId: configAuth.facebookAuth.clientID,
											secret:  configAuth.facebookAuth.clientSecret
										}).setAccessToken(user.facebook.token);

										fb.api('/'+ user.facebook.id +'/permissions', 'DELETE', function(err, data) {
											if(err) response.status(400).send('Cannot remove user facebook' + userId);
											else {
												User.remove({'facebook.id': userId}, function(err) { //Remove user!
													if(err) response.status(400).send('Cannot remove user');
													else response.status(200).send("User removed correctly");
												});
											}
										});
									});
								}
							});
						}
					});
				}
			});
		} else {
			response.status(400).send("Cannot delete other users... This would be a murder!");
		}
	});
	
	/** [ END ] Factory **/

	/** [START] GETTERS **/
	app.get('/info/user/:userID', isLoggedIn, function (req, res) {
		var userID = req.params.userID;
		if(!userID) {
			res.status(403).send("Empty userID");
		} else {
			User.findOne({'facebook.id': userID}, function(err, user) {
				if(err) res.status(403).send(err);
				else {
					if(!user) res.status(403).send("User not found");
					var userInfo = {
						name    : user.facebook.name,
						cards   : user.cardCount,
						folders : user.folderCount,
						mail    : user.facebook.email,
						icon    : user.facebook.photo
					};
					res.status(200).send(userInfo);
				}
			});
		}
	});

	app.post('/user/save/', isLoggedIn, function (request, response) {
	    var userJson = request.body;

		User.findByID(userJson.id, function(err, user) {
			user.icon = userJson.icon;

			User.updateItem(userJson.id, false, user, function(err, item) {
				if (err) {
					response.send(403);
				} 
				else {
		    		response.status(200).send({user: user});
		    	}
			});
		});		
	});

	app.post('/user/save/password', isLoggedIn, function (request, response) {
	    var userJson = request.body;

		User.findByID(userJson.id, function(err, user) {
			user.local.password = userJson.password;

			User.updateItem(userJson.id, true, user, function(err, item) {
				if (err) {
					response.send(403);
				} 
				else {
		    		response.status(200).send({user: user});
		    	}
			});
		});		
	});

	/* Search friends in facebook that are using WallyWallet */
	app.get('/search/users/:page/:items/:search', isLoggedIn, function (req, res) {
		var search = decodeURI(req.params.search); //encodeURIComponent
		var page = parseInt(req.params.page);
		var items = parseInt(req.params.items);
		var fb = new FB({
			appId: configAuth.facebookAuth.clientID,
			secret:  configAuth.facebookAuth.clientSecret
		}).setAccessToken(req.user.facebook.token);

		fb.api('/me/friends', function(err, data) {
			var friends = [];
			for(var friend in data.data) {
				friends.push(data.data[friend].id);
			}

			var query = {'facebook.id':  {$in : (friends)}, 'facebook.name': { $regex: search, $options: 'i'}};

			User.find(query, null, {skip: page*items, limit: items, sort: {'facebook.name': 1}}, function(err, wallyFriends) {
				res.status(200).send(wallyFriends);
			});
		});
	});

	app.get('/search/users/count/:search', isLoggedIn, function (req, res) {
		var fb = new FB({
			appId: configAuth.facebookAuth.clientID,
			secret:  configAuth.facebookAuth.clientSecret
		}).setAccessToken(req.user.facebook.token);

		var search = decodeURI(req.params.search); //encodeURIComponent

		fb.api('/me/friends', function(err, data) {
			var friends = [];
			for(var friend in data.data) {
				friends.push(data.data[friend].id);
			}
			User.count({'facebook.id':  {$in : (friends)}, 'facebook.name': { $regex: search, $options: 'i'}}, function(err, wallyFriends) {
				res.status(200).send({items:wallyFriends});
			});
		});
	})

	/*===== Search all users =====*/
	// app.get('/search/users/', isLoggedIn, function (req, res) {
	// 	User.find({}, function(err, users) {
	// 		if(err) res.status(403).send("Cannot execute search: " + search);
	// 		else res.status(200).send(users);
	// 	});
	// });

	// app.get('/search/users/:page/:items/:search', isLoggedIn, function (req, res) {
	// 	var search = decodeURI(req.params.search); //encodeURIComponent
	// 	var query = {'facebook.name': { $regex: search, $options: 'i'}};
	// 	var page = parseInt(req.params.page);
	// 	var items = parseInt(req.params.items);

	// 	User.find(query, null, {skip: page*items, limit: items, sort: {'facebook.name': 1}}, function(err, users) {
	// 		if(err) res.status(403).send("Cannot execute search: " + search);
	// 		else res.status(200).send(users);
	// 	});
	// });

	// app.get('/search/users/count/:search', isLoggedIn, function (req, res) {
	// 	var search = decodeURI(req.params.search); //encodeURIComponent
	// 	var query = {'facebook.name': { $regex: search, $options: 'i'}};
	// 	User.count(query,  function(err, users) {
	// 		if(err) res.status(403).send("Cannot execute search: " + search);
	// 		else res.status(200).send({items:users});
	// 	});
	// })

	app.get('/search/cards/:page/:items/:search', isLoggedIn, function (req, res) {
		var search = decodeURI(req.params.search); //encodeURIComponent
		var query = generateCardQuery(search);

		getCards(query, req, res);
	});

	app.get('/search/tags/:page/:items/:search', isLoggedIn, function (req, res) {
		var search = decodeURI(req.params.search); //encodeURIComponent
		var query = {'tags': { $in: filterToTags(search)}};
		getCards(query, req, res);
	});

	app.get('/search/cards/count/:search', isLoggedIn, function (req, res) {
		var search = decodeURI(req.params.search); //encodeURIComponent
		var query = generateCardQuery(search);

		getCountCards(query, req, res);
	});

	app.get('/search/tags/count/:search', isLoggedIn, function (req, res) {
		var search = decodeURI(req.params.search); //encodeURIComponent
		var query = {'tags': { $in: filterToTags(search)}};

		getCountCards(query, req, res);
	});


	/** [ END ] GETTERS **/
}

function generateCardQuery(search) {
	var query = {$or : [
				{'company': { $regex: search, $options: 'i'}},
				{'guy.name': { $regex: search, $options: 'i'}},
				{'guy,appointment': { $regex: search, $options: 'i'}},
			]};
	return query;
}


function getCountCards(query, req, res) {
	Card.count(query, function(err, cards) {
		if(err) res.status(403).send("Cannot execute search: " + search);
		else res.status(200).send({items:cards});
	});
}

function getCards(query, req, res) {
	var page = parseInt(req.params.page);
	var items = parseInt(req.params.items);

	Card.find(query, null,  {skip: page*items, limit: items, sort: {'company': 1}}, function(err, cards) {
		if(err) res.status(403).send("Cannot execute search: " + search);
		else res.status(200).send(cards);
	});
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {
	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();

	res.status(400).send("Wrong acction, you are not allowed here");
}

function isNull(item) {
	return (item === null || item === undefined);
}

var deleteSpecialChars = new RegExp(/[\.,-\/+#!$%€?¿<>…"“”‘’—–\^&\*;:{}=\-_`~()]/g);

//"This., -/ is #! an $ % ^ & * example ;: {} of a = -_ string with `~)() punctuation"
function removeInvalidChars(sentence) {
	return sentence.toLowerCase().replace(deleteSpecialChars, ' ').replace(/\s{2,}/g,' ').trim().split(' ')
}

function filterToTags(filter) {
	var tags = removeInvalidChars(filter);

	tags = tags.filter(function(elem, pos) {
		return tags.indexOf(elem) == pos;
	});

	return tags;
}