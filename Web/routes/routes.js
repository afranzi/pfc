
/*
 * GET home page.
 */
// app/routes.js
module.exports = function(app, passport) {

	// =====================================
	// LOGIN ===============================
	// =====================================
	app.get('/auth/facebook', passport.authenticate('facebook', {scope: 'email, public_profile, user_friends'})); //Only can be granted these three permissions, others have to be approved for use by Facebook.
	
	app.get('/auth/facebook/callback', passport.authenticate('facebook', {
		successRedirect: '/content/user',
		failureRedirect: '/',
		failureFlash : true // allow flash messages
	}));

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/message', // redirect to the secure profile section
		failureRedirect : '/errorMessage', // redirect back to the signup page if there is an error
		successFlash: 'OK',
		failureFlash: 'Missing credentials'      
	}));

	app.get('/errorMessage', function(req, res) {
		res.render('message.ejs',  { message: req.flash('error') });
	});

	app.get('/message', function(req, res) {
		res.render('message.ejs',  { message: req.flash('success') });
	});

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		if (req.isAuthenticated()) {
			res.redirect('/content');
		} else {
			res.render('index.ejs'); // load the index.ejs file
		}
	});

	// =====================================
	// PROFILE SECTION =====================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	app.get('/search', isLoggedIn,  function(req, res) {
		res.render('users.ejs', {
			user   :  req.user, 
			search : ''
		});
	});

	app.get('/search/:search', isLoggedIn, function(req, res) {
		var search = req.params.search;

		res.render('users.ejs', {
			user   :  req.user, 
			search : search
		});
	});

	app.get('/content', isLoggedIn, function(req, res) { //Otherwise we redirect to index url to avoid unaccepted access and 404.
		res.redirect('/content/user');
	});

	app.get('/content/user', isLoggedIn, function(req, res) { //Redirect to user content root page
		res.redirect('/content/user/' + req.user.facebook.id);
	})


	app.get('/content/user/:user', isLoggedIn,  function(req, res) {
		var currentUser = req.params.user;

		renderContent(res, req.user, "", "", true, currentUser);
	});

	app.get('/content/user/:user/:parent', isLoggedIn,  function(req, res) {
        var parent      = req.params.parent;
		var currentUser = req.params.user;

		renderContent(res, req.user, parent, "", false, currentUser);
	});

	app.get('/content/user/:user/:parent/:granpa', isLoggedIn,  function(req, res) {
        var parent      = req.params.parent;
        var granpa      = req.params.granpa;
        var currentUser = req.params.user;

		renderContent(res, req.user, parent, granpa, false, currentUser);
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.get('/*', function(req, res) { //Otherwise we redirect to index url to avoid unaccepted access and 404.
		//res.status(400).send("Wrong acction");
		res.redirect('/');
	});
};

function renderContent(res, user, parent, granpa, root, currentUser) {
	res.render('content.ejs', {
		user : user, // get the user out of session and pass to template,
		path : {
			parent: parent,
			granpa: granpa,
			root: root
		},
		currentUser: currentUser
	});
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on 
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
	//res.status(400).send("Wrong acction");
}