var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    bcrypt   = require('bcrypt-nodejs');
    SALT_WORK_FACTOR = 10;
 
var UserSchema = new Schema({
    facebook         : {
        id           : { type: String, required: true, index: { unique: true }},
        token        : String,
        email        : String,
        photo        : String,
        name         : { type: String, required: true, index: { unique: false }},
    },
    cardCount        : { type: Number, default: 0},
    folderCount      : { type: Number, default: 0},
    dummy            : { type: Boolean, default: false},
});
 
UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('local.password')) return next();

    // generate a salt
    updatePassword(user, function(user) {
        next();
    });
});

UserSchema.pre('remove', function(next) {
    console.log("Removing user", this);
    next();
});

UserSchema.statics.updateItem = function(id, updatePass, item, callback) {
    var upsertData = item.toObject();
    delete upsertData._id;

    if(updatePass) {
        updatePassword(upsertData, function(upsertData) {
            User.update({_id: id}, upsertData, {upsert: true}, callback);
        });
    } else {
        User.update({_id: id}, upsertData, {upsert: true}, callback);
    }
};

UserSchema.statics.exists = function(email, callback) {
    this.findOne({'local.email': email}, function(err, user) {
        if (err) callback(err);
        callback(user != null);
    });
}

UserSchema.statics.findByID = function(id, callback) {
    this.findOne({'_id': id}, function(err, user) {
        callback(err, user);
    });
}

UserSchema.statics.findByEmail = function(email, callback) {
    this.findOne({'local.email': email}, function(err, user) {
        if (err) callback(err);
        callback(user);
    });
}

function updatePassword(user, callback) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);
    
        // hash the password using our new salt
        bcrypt.hash(user.local.password, salt, null, function(err, hash) {
            if (err) return next(err);
     
            // override the cleartext password with the hashed one
            user.local.password = hash;
            callback(user);
        });
    });
}

// methods ======================
// checking if password is valid (Async Method)
UserSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.local.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};


UserSchema.statics.modifyCardCounter = function(userID, increment, callback) {
    this.findOne({'_id': userID}, function(err, user) {
        if(err) return callback(err);
        else if(user) {
            user.cardCount += increment;
            if(user.cardCount < 0) user.cardCount = 0;
            user.save(function(err, folder) {
                if(err) return callback(err);
                else return callback();
            });
        } else return callback();
    });
}

UserSchema.statics.modifyFolderCounter = function(userID, increment, callback) {
    this.findOne({'_id': userID}, function(err, user) {
        if(err) return callback(err);
        else if(user) {
            user.folderCount += increment;
            if(user.folderCount < 0) user.folderCount = 0;
            user.save(function(err, folder) {
                if(err) return callback(err);
                else return callback();
            });
        } else callback();
    });
}



// generating a hash
// UserSchema.methods.generateHash = function(password) {
//     return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
// };

// checking if password is valid (Sync Method)
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

UserSchema.methods.findByEmail = function(email, success, fail){    
    _model.findOne({"local.email":email}, function(e, doc){      
        if(e){        
            fail(e)      
        }else{
            console.log("Founded " + doc);   
            success(doc);      
        }    
    });
}
 
module.exports = mongoose.model('User', UserSchema);