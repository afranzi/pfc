var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    bcrypt   = require('bcrypt-nodejs');
    SALT_WORK_FACTOR = 10;
 
var UserSchema = new Schema({
    facebook         : {
        id           : { type: String, required: true, index: { unique: true }},
        token        : String,
        mobileToken  : String,
        email        : String,
        photo        : String,
        name         : { type: String, required: true, index: { unique: false }},
    },
    cardCount        : { type: Number, default: 0},
    folderCount      : { type: Number, default: 0},
    dummy            : { type: Boolean, default: false},
});
 
UserSchema.pre('remove', function(next) {
    console.log("Removing user", this);
    next();
});

UserSchema.statics.updateItem = function(id, updatePass, item, callback) {
    var upsertData = item.toObject();
    delete upsertData._id;

    User.update({'facebook.id': id}, upsertData, {upsert: true}, callback);
};

UserSchema.statics.findByID = function(id, callback) {
    this.findOne({'facebook.id': id}, function(err, user) {
        callback(err, user);
    });
}

UserSchema.statics.modifyCardCounter = function(userID, increment, callback) {
    this.findOne({'facebook.id': userID}, function(err, user) {
        if(err) return callback(err);
        else if(user) {
            user.cardCount += increment;
            if(user.cardCount < 0) user.cardCount = 0;
            user.save(function(err, folder) {
                if(err) return callback(err);
                else return callback();
            });
        } else return callback();
    });
}

UserSchema.statics.modifyFolderCounter = function(userID, increment, callback) {
    this.findOne({'facebook.id': userID}, function(err, user) {
        if(err) return callback(err);
        else if(user) {
            user.folderCount += increment;
            if(user.folderCount < 0) user.folderCount = 0;
            user.save(function(err, folder) {
                if(err) return callback(err);
                else return callback();
            });
        } else callback();
    });
}

UserSchema.statics.findByEmail = function(email, callback) {
    this.findOne({'facebook.email': email}, function(err, user) {
        if (err) callback(err);
        callback(user);
    });
}

UserSchema.methods.findByEmail = function(email, success, fail){    
    _model.findOne({"facebook.email":email}, function(e, doc){      
        if(e){        
            fail(e)      
        }else{
            success(doc);      
        }    
    });
}

module.exports = mongoose.model('User', UserSchema);