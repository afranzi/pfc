var mongoose = require('mongoose'),
	util     = require('util'),
	Schema   = mongoose.Schema;

var ItemSchema = function ItemAbstractSchema() {
	Schema.apply(this, arguments);

	this.add({       //Fields
		isPrivate    : { type: Boolean, default: false},
		notesNumber  : { type: Number, default: 0 },
		owner        : { type: String, required: true , index: true},
		parent       : { type: String},
		tags         : { type: [String], index: true },
		dummy        : { type: Boolean, default: false},
	});

	this.index({ owner: 1, tags: -1 }); // schema level
	this.options.discriminatorKey = '_type';

	this.pre('save', function(next) {
		var item = this;
		if(!item.parent) { //Set null to empty parent
			item.parent = null;
		}

		if(!item.isModified('tags')) return next();

		if(item.tags != undefined && item.tags.length > 5) {
			//Not allowed, max tags size == 5
			item.invalidate("tags","Max tag number is 5");
			next(new Error("Max tag number is 5"));
		}
		else {
			next();
		}
	});

	this.statics.updateTags = function(id, owner, tags, callback) {
		this.findOne({'_id': id, 'owner': owner}, function(err, item) {
			if(err) callback(err);
			else {
				item.tags = tags;
				item.save(function(err, item) {
					if(err) callback(err, item);
					else callback(null, item);
				});
			}
		});
	};

	this.statics.findById = function(id, owner, callback) {
		this.findOne({'_id': id, 'owner': owner}, callback);
	};

	this.statics.updateItem = function(id, item, callback) {
		var upsertData = item.toObject();
		delete upsertData._id;
		this.update({_id: id}, upsertData, {upsert: true}, callback);
	};
};



util.inherits(ItemSchema, Schema);

module.exports = ItemSchema;
