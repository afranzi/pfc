var mongoose   = require('mongoose'),
    Schema     = mongoose.Schema,
    extend     = require('mongoose-schema-extend');
    User       = require('./User'),
    ItemSchema = require('./ItemAbstract');
    
var util = require('util');

var FolderSchema = new ItemSchema({
    name         : { type: String, required: true},
    cardsCount   : { type: Number, default: 0 },
    foldersCount : { type: Number, default: 0 },
});

FolderSchema.pre('save', function(next) {
    var folder = this;

    if (!folder.isNew)  {
        return next();
    } else {
        if(folder.parent != undefined && folder.parent != null) {
            modifyFolderCounter(folder.parent, 1, next);
        } else {
            User.modifyFolderCounter(folder.owner, 1, next);
        } 
    }
});

FolderSchema.pre('remove', function(next) {
    var folder = this;
    if(folder.parent != undefined && folder.parent != null) {
        modifyFolderCounter(folder.parent, -1, next);
    } else {
        User.modifyFolderCounter(folder.owner, -1, next);
    }
});

function modifyCardCounter(parent, increment, callback) {
    Folder.findOne({'_id': parent}, function(err, folder) {
        if(err) return callback(err);
        else if(folder) {
            folder.cardsCount += increment;
            if(folder.cardsCount < 0) folder.cardsCount = 0;
            folder.save(function(err, folder) {
                if(err) return callback(err);
                else return callback();
            });
        } else return callback();
    });
}

function modifyFolderCounter(parent, increment, callback) {
    Folder.findOne({'_id': parent}, function(err, folder) {
        if(err) return callback(err);
        else if(folder) {
            User.modifyFolderCounter(folder.owner, increment, function(err) {
                folder.foldersCount += increment;
                if(folder.foldersCount < 0) folder.foldersCount = 0;
                folder.save(function(err, folder) {
                    if(err) return callback(err);
                    else return callback();
                });
            });
        } else return callback();
    });
}

module.exports = mongoose.model('Folder', FolderSchema);
module.exports.modifyCardCounter = modifyCardCounter;