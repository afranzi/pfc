var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    extend   = require('mongoose-schema-extend');
    ItemSchema = require('./ItemAbstract')
    Folder   = require('./Folder'),
    User     = require('./User'),
    Comment  = require('./Comment');
    
var util = require('util');

var CardSchema = new ItemSchema({
    address         : {
    	street      : { type: String, required: true },
    	city        : { type: String, required: true },
    	country     : { type: String, required: true }
    },
    company         : { type: String, required: true},
    email           : { type: String, required: true},
    phone           : { type: String },
    guy             : {
        name        : { type: String },
        appointment : { type: String }
    }
});

CardSchema.pre('save', function(next) {
    var card = this;
    if (card.isNew) {
        User.modifyCardCounter(card.owner, 1, function(err) {
            if(err) return next(err);
            if(card.parent != undefined && card.parent != null) {
                Folder.modifyCardCounter(card.parent, 1, next);
            } else return next();
        });
    } else return next();
});

CardSchema.pre('remove', function(next) {
    var card = this;
    User.modifyCardCounter(card.owner, -1, function(err) {
        if(err) return next(err);
        if(card.parent != undefined && card.parent != null) {
            Folder.modifyCardCounter(card.parent, -1, function(err) {
                if(err) return next(err);
                Comment.remove({'fk_item': card._id}, function(err) {
                    if(err) return next(err);
                    else return next();
                });
            });
        } else return next();
    });
});

var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

CardSchema.path('email').validate(function(email, respond) {
    respond(emailRegex.test(email));
}, 'Invalid email');

module.exports = mongoose.model('Card', CardSchema);