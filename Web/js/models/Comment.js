var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    User     = require('./User');

var CommentSchema = new Schema({
    text      : { type: String, required: true }, //Text del comentari
    fk_user   : { type: String, required: true }, //Id del usuari que ha creat el comentari.
    fk_item   : { type: String, required: true }, //Id del objecte que conté el comentari.
    publisher : { type: String                 }, //Name del usuari que ha publicat el comentari.
    date      : { type: Date,   required: true  , default: Date.now }
});

CommentSchema.index({ fk_item: 1, fk_user: -1 }); // schema level

CommentSchema.pre('save', function(next) {
    var comment = this;

    User.findByID(comment.fk_user, function(err, user) {
        if(err) throw err;
        else if (!user) next(new Error("Publisher doesn't exists"));
        else {
            comment.publisher = user.facebook.name;
             // only hash the password if it has been modified (or is new)
            if (!comment.isModified('date')) {
                comment.date = Date.now();
            }
            next();   
        }
    });
});

CommentSchema.statics.findByPublisher = function(publisher, callback) {
    this.findOne({'fk_user': publisher}, function(err, comment) {
        if (err) callback(err);
        callback(comment);
    });
}

CommentSchema.statics.findById = function(idItem, callback) {
    this.findOne({'_id': idItem}, function(err, comment) {
        if (err) callback(err);
        callback(comment);
    });
}

/* Busca el primer comentari que trobi del container */
CommentSchema.statics.findByContainer = function(idItem, callback) {
    this.findOne({'fk_item': idItem}, function(err, comment) {
        if (err) callback(err);
        callback(comment);
    });
}

CommentSchema.statics.findAllById = function(idItem, callback) {
    this.find({'fk_item': idItem}, function(err, comments) {
        if (err) callback(err);
        callback(comments);
    });
}

var ERROR_TEXT_LONG = 'Wrong comment, text so long (+120)';

CommentSchema.path('text').validate(function(text, respond) {
    respond(text.length <= 120);
}, ERROR_TEXT_LONG);


module.exports = mongoose.model('Comment', CommentSchema);