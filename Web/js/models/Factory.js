var User   = require('./User'),
	Folder = require('./Folder'),
	Card   = require('./Card'),
	async  = require('async');


var words		= [
					'Patata', 'Pastanaga', 'Ratoli', 'Sapastre', 'Gandul', 'Garrafal',
					'Superman', 'Forca', 'Andromeda', 'Diagonal', 'Triangle', 'Robot',
					'Metall', 'Rock', 'Nan', 'Pointer', 'Null', 'Void', 'Charmander', 
					'Worker', 'Software', 'Engineer', 'Cakes', 'Muffins', 'Carrot',
					'Coconut', 'Apple', 'Developer', 'Marketing', 'Restaurant', 'Dentist'
				];
var domains   	= ['com', 'net', 'es', 'cat', 'org', 'io'];
var companies 	= [	'Trovit', 'Google', 'Youtube', 'Atrapalo', 'Decathlon', 
					'Nostrum', 'BMW', 'Seat', 'Sant Pau', 'Masia',
					'Pull&Bear', 'H&M', 'Zara', 'Maximo Dutti', 'McDonalds',
					'El Raco', 'Restauracio'
				];
var names     	= [
					'John', 'James', 'Silvia', 'Aura', 'Berta', 'Albert', 'Carles',
					'Jaume', 'Ferran', 'Marina', 'Judith', 'Anna', 'Maria', 'Toni',
					'Andreu', 'Raquel', 'Maria Antonia', 'Jofre', 'Helena', 'Patri',
					'Ignasi', 'Nuria', 'Miquel', 'Xavier', 'Elisabeth', 'Raul'
				];
var cities 	 	= [
					'Barcelona', 'Girona', 'S\'Agaró', 'Sant Feliu de Guíxols', 'Sabadell',
					'Terrassa', 'Sant Cugat', 'London', 'Manresa', 'Mataró', 'Igualada', 'Cambríls',
					'Cases d\'Alcanar', 'Prat del Llobregat', 'Roma', 'Milà', 'New Delhi', 'Andorra',
					'Platja d\'Aró', 'Palamós', 'Palafrugell', 'L\'Escala'
				];
var countries 	= [
					'Andorra', 'Belgium', 'Catalonia', 'Denmark', 'England', 'France', 'Germany', 'Holand',
					'Spain',  'Unitated States', 'Norway', 'Scotland'
				];
var roles 		= [
					'Staff', 'Office', 'Manager', 'Developer', 'Marketing', 'Resources', 'Engineer', 
					'Supervisor', 'Product', 'Driver', 'Researchers', 'Designer', 'Human', 'Alien',
					'Muggle', 'Unemployed', 'Cook', 'Builder', 'Researcher', 'Nurse', 'Comercial'
				];


var foldersGenerate = 0;
var cardsGenerate = 0;

var generateUsers = function(usersToGenerate, maxFolders, maxCards, callback) {
	foldersGenerate = 0;
	cardsGenerate = 0;

	var count = 0;
	async.whilst(
		function () { return count < usersToGenerate; },
		function (callback) {
			++count;			
			createUser(count, maxFolders, maxCards, function(user) {
				callback();
			});
		},
		function (err) {
			callback(usersToGenerate, foldersGenerate, cardsGenerate); //Call done function and finalize :D
		}
	);
}

var generateThings = function(userId, maxFolders, maxCards, callback) {
	foldersGenerate = 0;
	cardsGenerate = 0;

	createFolder(null, userId, maxFolders - getRandomInt(0, maxFolders), maxCards, 0, function(folder) {
		createCards(null,userId, maxCards, function() {
			callback(foldersGenerate, cardsGenerate);
		});
	});
}


function createUser(number, maxFolders, maxCards, callback) {
	var name = generateName(getRandomInt(1,2));
	var user = new User({
		facebook : {
			id 		: number + '_' + getRandomInt(1, 1000),
			email   : generateEmail(name),
			name 	: '#T' + number + ' ' + name,
			token 	: 'Password',
			photo 	: 'http://wallywallet.net/images/profile-'+getRandomInt(1, 20)+'.png',
		},
		icon: getRandomInt(1, 20),
		dummy : true
	});

	user.save(function(err, user) {
		if(err) throw err;
		createFolder(null, user.facebook.id, maxFolders - getRandomInt(0, maxFolders), maxCards, 0, function(folder) {
			createCards(null, user.facebook.id, maxCards, function() {
				callback(user);
			});
		});
	});
}


function createFolder(parentID, ownerID, maxSubFolders, maxCards, level, callbackParent) {
	var folder = new Folder({
		name    : generateName(getRandomInt(1,3)),
		owner   : ownerID,
		parent  : parentID,
		tags    : generateTags(),
		dummy : true
	});

	folder.save(function(err, folder) {
		++foldersGenerate;
		//console.log(Array(level+1).join('   |') + '---> ', folder.name, "  (", maxSubFolders,")");

		createCards(folder._id, ownerID, maxCards, function(number) {
			if(err) throw err;
			if(maxSubFolders > 0) {
				var count = 0;
				async.whilst(
					function () { return count < maxSubFolders; },
					function (callback) {
						++count;

						createFolder(folder._id, ownerID, maxSubFolders - getRandomInt(1, maxSubFolders),  maxCards, level + 1, function(subFolder) {
							callback();
						});
					},
					function (err) {
						callbackParent(folder);
					}
				);				
			} else {
				callbackParent(folder);
			}
		});
	});
}

function createCards(parentID, ownerID, maxCards, callbackParent) {
	var count = 0;
	var cardsToGenerate = getRandomInt(1, maxCards);
	async.whilst(
		function () { return count < cardsToGenerate; },
		function (callback) {
			++count;
			createCard(parentID, ownerID, function(card) {
				callback();
			});
		},
		function (err) {
			callbackParent(cardsToGenerate);
		}
	);		
}

function createCard(parentID, ownerID, callback) {
	var name = generateNameWithPrefix(2, names, words);
	var company = getRandomItem(companies);
	++cardsGenerate;

	var card = new Card({
		parent: parentID,
        owner: ownerID,
        address : {
            street  : generateNameWithPrefix(3, words, words),
            city    : getRandomItem(cities),
            country : getRandomItem(countries),
        },
        company : company,
        email   : generateEmailWithCompany(name, company),
        phone   : generatePhone(),
        tags    : generateTags(),
        guy		: {
        	name : name,
        	appointment: generateNameWithPrefix(2, roles, roles),
        },
		dummy : true
    });

    card.save(function(err, card) {
    	if(err) throw err;
    	callback(card);
    })
}

function generateTags() {
	var numTags = getRandomInt(1,5);
	var tags = [];
	for(var i = 0; i < numTags; ++i) {
		tags.push(getRandomItem(words).toLowerCase());
	}
	return tags;
}

function generateEmailWithCompany(name, company) {
	var email = name + '@' + company + '.' + getRandomItem(domains);
	return email.replace(/\s+/g, '').replace('&', '');
}

function generateEmail(name) {
	return generateEmailWithCompany(name, getRandomItem(companies));
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomItem(items) {
	return items[getRandomInt(0, items.length - 1)];
}

function generateName(numWords) {
	return generateNameWithPrefix(numWords, words, words);
}

function generateNameWithPrefix(numWords, prefix, items) {
	var name = getRandomItem(prefix);
	for(var i = 1; i < numWords; ++i) {
		name += ' ' + getRandomItem(items);
	}
	return name;
}

function generatePhone() {
	var phoneNumer = getRandomInt(100000000,999999999).toString();
	phoneNumer = phoneNumer.replace(/(\d{2})(\d{3})(\d{2}(\d{2}))/, "$1-$2-$3-$4");

	return phoneNumer;
}

module.exports.generateUsers = generateUsers;
module.exports.generateThings = generateThings;