
/**
 * Module dependencies.
 */

var express   = require('express');
var app       = express();
var port      = process.env.PORT || 3000;
var mongoose  = require('mongoose');
var flash     = require('connect-flash');

var configDB = require('./config/config.js'); //DBB properties

var passport = require('passport');

// configuration ===============================================================
if(process.env.NODE_ENV == 'testing') {
  console.log("Starting in TEST mode...")
  mongoose.connect(configDB.db.test); // connect to our test database
} else {
  console.log("Starting in PRODUCTION mode...")
  mongoose.connect(configDB.db.prod); // connect to our production database
}

require('./config/passport')(passport); // pass passport for configuration

// Configuration
app.configure(function(){
  app.use(express.logger('dev')); // log every request to the console  
  app.use(express.cookieParser()); // read cookies (needed for auth)
  app.use(express.bodyParser()); // get information from html forms

  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');  

  app.use(express.session({ secret: 'SecretApocalypsisNow Andromeda' }));
  
  //include the Passport middleware
  app.use(passport.initialize());
  app.use(passport.session()); // persistent login sessions
  app.use(flash()); // use connect-flash for flash messages stored in session
  
  app.use(require('stylus').middleware({ src: __dirname + '/public' }));
  app.use(express.methodOverride());
  app.use(express.static(__dirname + '/public'));
  app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// routes ======================================================================
var routes3  = require('./routes/restApi/restUser.js')(app, passport); 
var routes   = require('./routes/restApi.js')(app, passport); // load our routes and pass in our app and fully configured passport
var routes2  = require('./routes/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport


// launch ======================================================================
app.listen(port);
console.log('The magic happens on port ' + port);

exports.app = app;
