package io.realm;


import com.wallywallet.models.beans.*;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.util.*;

public class UserRealmRealmProxy extends UserRealm {

    @Override
    public String get_id() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("_id"));
    }

    @Override
    public void set_id(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("_id"), (String) value);
    }

    @Override
    public int getCardCount() {
        return (int) row.getLong(Realm.columnIndices.get("UserRealm").get("cardCount"));
    }

    @Override
    public void setCardCount(int value) {
        row.setLong(Realm.columnIndices.get("UserRealm").get("cardCount"), (long) value);
    }

    @Override
    public boolean isDummy() {
        return (boolean) row.getBoolean(Realm.columnIndices.get("UserRealm").get("dummy"));
    }

    @Override
    public void setDummy(boolean value) {
        row.setBoolean(Realm.columnIndices.get("UserRealm").get("dummy"), (boolean) value);
    }

    @Override
    public String getEmail() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("email"));
    }

    @Override
    public void setEmail(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("email"), (String) value);
    }

    @Override
    public int getFolderCount() {
        return (int) row.getLong(Realm.columnIndices.get("UserRealm").get("folderCount"));
    }

    @Override
    public void setFolderCount(int value) {
        row.setLong(Realm.columnIndices.get("UserRealm").get("folderCount"), (long) value);
    }

    @Override
    public String getId() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("id"));
    }

    @Override
    public void setId(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("id"), (String) value);
    }

    @Override
    public String getMobileToken() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("mobileToken"));
    }

    @Override
    public void setMobileToken(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("mobileToken"), (String) value);
    }

    @Override
    public String getName() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("name"));
    }

    @Override
    public void setName(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("name"), (String) value);
    }

    @Override
    public String getPhoto() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("photo"));
    }

    @Override
    public void setPhoto(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("photo"), (String) value);
    }

    @Override
    public String getToken() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("UserRealm").get("token"));
    }

    @Override
    public void setToken(String value) {
        row.setString(Realm.columnIndices.get("UserRealm").get("token"), (String) value);
    }

    @Override
    public int getVersion() {
        return (int) row.getLong(Realm.columnIndices.get("UserRealm").get("version"));
    }

    @Override
    public void setVersion(int value) {
        row.setLong(Realm.columnIndices.get("UserRealm").get("version"), (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if(!transaction.hasTable("class_UserRealm")) {
            Table table = transaction.getTable("class_UserRealm");
            table.addColumn(ColumnType.STRING, "_id");
            table.addColumn(ColumnType.INTEGER, "cardCount");
            table.addColumn(ColumnType.BOOLEAN, "dummy");
            table.addColumn(ColumnType.STRING, "email");
            table.addColumn(ColumnType.INTEGER, "folderCount");
            table.addColumn(ColumnType.STRING, "id");
            table.addColumn(ColumnType.STRING, "mobileToken");
            table.addColumn(ColumnType.STRING, "name");
            table.addColumn(ColumnType.STRING, "photo");
            table.addColumn(ColumnType.STRING, "token");
            table.addColumn(ColumnType.INTEGER, "version");
            return table;
        }
        return transaction.getTable("class_UserRealm");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if(transaction.hasTable("class_UserRealm")) {
            Table table = transaction.getTable("class_UserRealm");
            if(table.getColumnCount() != 11) {
                throw new IllegalStateException("Column count does not match");
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for(long i = 0; i < 11; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }
            if (!columnTypes.containsKey("_id")) {
                throw new IllegalStateException("Missing column '_id'");
            }
            if (columnTypes.get("_id") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column '_id'");
            }
            if (!columnTypes.containsKey("cardCount")) {
                throw new IllegalStateException("Missing column 'cardCount'");
            }
            if (columnTypes.get("cardCount") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'cardCount'");
            }
            if (!columnTypes.containsKey("dummy")) {
                throw new IllegalStateException("Missing column 'dummy'");
            }
            if (columnTypes.get("dummy") != ColumnType.BOOLEAN) {
                throw new IllegalStateException("Invalid type 'boolean' for column 'dummy'");
            }
            if (!columnTypes.containsKey("email")) {
                throw new IllegalStateException("Missing column 'email'");
            }
            if (columnTypes.get("email") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'email'");
            }
            if (!columnTypes.containsKey("folderCount")) {
                throw new IllegalStateException("Missing column 'folderCount'");
            }
            if (columnTypes.get("folderCount") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'folderCount'");
            }
            if (!columnTypes.containsKey("id")) {
                throw new IllegalStateException("Missing column 'id'");
            }
            if (columnTypes.get("id") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'id'");
            }
            if (!columnTypes.containsKey("mobileToken")) {
                throw new IllegalStateException("Missing column 'mobileToken'");
            }
            if (columnTypes.get("mobileToken") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'mobileToken'");
            }
            if (!columnTypes.containsKey("name")) {
                throw new IllegalStateException("Missing column 'name'");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'name'");
            }
            if (!columnTypes.containsKey("photo")) {
                throw new IllegalStateException("Missing column 'photo'");
            }
            if (columnTypes.get("photo") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'photo'");
            }
            if (!columnTypes.containsKey("token")) {
                throw new IllegalStateException("Missing column 'token'");
            }
            if (columnTypes.get("token") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'token'");
            }
            if (!columnTypes.containsKey("version")) {
                throw new IllegalStateException("Missing column 'version'");
            }
            if (columnTypes.get("version") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'version'");
            }
        }
    }

    public static List<String> getFieldNames() {
        return Arrays.asList("_id", "cardCount", "dummy", "email", "folderCount", "id", "mobileToken", "name", "photo", "token", "version");
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("UserRealm = [");
        stringBuilder.append("{_id:");
        stringBuilder.append(get_id());
        stringBuilder.append("} ");
        stringBuilder.append("{cardCount:");
        stringBuilder.append(getCardCount());
        stringBuilder.append("} ");
        stringBuilder.append("{dummy:");
        stringBuilder.append(isDummy());
        stringBuilder.append("} ");
        stringBuilder.append("{email:");
        stringBuilder.append(getEmail());
        stringBuilder.append("} ");
        stringBuilder.append("{folderCount:");
        stringBuilder.append(getFolderCount());
        stringBuilder.append("} ");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("} ");
        stringBuilder.append("{mobileToken:");
        stringBuilder.append(getMobileToken());
        stringBuilder.append("} ");
        stringBuilder.append("{name:");
        stringBuilder.append(getName());
        stringBuilder.append("} ");
        stringBuilder.append("{photo:");
        stringBuilder.append(getPhoto());
        stringBuilder.append("} ");
        stringBuilder.append("{token:");
        stringBuilder.append(getToken());
        stringBuilder.append("} ");
        stringBuilder.append("{version:");
        stringBuilder.append(getVersion());
        stringBuilder.append("} ");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        int result = 17;
        String aString_0 = get_id();
        result = 31 * result + (aString_0 != null ? aString_0.hashCode() : 0);
        result = 31 * result + getCardCount();
        result = 31 * result + (isDummy() ? 1 : 0);
        String aString_3 = getEmail();
        result = 31 * result + (aString_3 != null ? aString_3.hashCode() : 0);
        result = 31 * result + getFolderCount();
        String aString_5 = getId();
        result = 31 * result + (aString_5 != null ? aString_5.hashCode() : 0);
        String aString_6 = getMobileToken();
        result = 31 * result + (aString_6 != null ? aString_6.hashCode() : 0);
        String aString_7 = getName();
        result = 31 * result + (aString_7 != null ? aString_7.hashCode() : 0);
        String aString_8 = getPhoto();
        result = 31 * result + (aString_8 != null ? aString_8.hashCode() : 0);
        String aString_9 = getToken();
        result = 31 * result + (aString_9 != null ? aString_9.hashCode() : 0);
        result = 31 * result + getVersion();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRealmRealmProxy aUserRealm = (UserRealmRealmProxy)o;
        if (get_id() != null ? !get_id().equals(aUserRealm.get_id()) : aUserRealm.get_id() != null) return false;
        if (getCardCount() != aUserRealm.getCardCount()) return false;
        if (isDummy() != aUserRealm.isDummy()) return false;
        if (getEmail() != null ? !getEmail().equals(aUserRealm.getEmail()) : aUserRealm.getEmail() != null) return false;
        if (getFolderCount() != aUserRealm.getFolderCount()) return false;
        if (getId() != null ? !getId().equals(aUserRealm.getId()) : aUserRealm.getId() != null) return false;
        if (getMobileToken() != null ? !getMobileToken().equals(aUserRealm.getMobileToken()) : aUserRealm.getMobileToken() != null) return false;
        if (getName() != null ? !getName().equals(aUserRealm.getName()) : aUserRealm.getName() != null) return false;
        if (getPhoto() != null ? !getPhoto().equals(aUserRealm.getPhoto()) : aUserRealm.getPhoto() != null) return false;
        if (getToken() != null ? !getToken().equals(aUserRealm.getToken()) : aUserRealm.getToken() != null) return false;
        if (getVersion() != aUserRealm.getVersion()) return false;
        return true;
    }

}
