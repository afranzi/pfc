package io.realm;


import java.util.Arrays;
import java.util.List;

public class ValidationList {

    public static List<String> getProxyClasses() {
        return Arrays.asList("com.wallywallet.models.beans.UserRealm", "com.wallywallet.models.beans.FolderRealm", "com.wallywallet.models.beans.CardRealm");
    }

}
