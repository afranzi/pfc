package io.realm;


import com.wallywallet.models.beans.*;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.util.*;

public class CardRealmRealmProxy extends CardRealm {

    @Override
    public String getAppointment() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("appointment"));
    }

    @Override
    public void setAppointment(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("appointment"), (String) value);
    }

    @Override
    public String getCity() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("city"));
    }

    @Override
    public void setCity(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("city"), (String) value);
    }

    @Override
    public String getCompany() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("company"));
    }

    @Override
    public void setCompany(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("company"), (String) value);
    }

    @Override
    public String getCountry() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("country"));
    }

    @Override
    public void setCountry(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("country"), (String) value);
    }

    @Override
    public boolean isDummy() {
        return (boolean) row.getBoolean(Realm.columnIndices.get("CardRealm").get("dummy"));
    }

    @Override
    public void setDummy(boolean value) {
        row.setBoolean(Realm.columnIndices.get("CardRealm").get("dummy"), (boolean) value);
    }

    @Override
    public String getEmail() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("email"));
    }

    @Override
    public void setEmail(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("email"), (String) value);
    }

    @Override
    public String getId() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("id"));
    }

    @Override
    public void setId(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("id"), (String) value);
    }

    @Override
    public boolean isIsPrivate() {
        return (boolean) row.getBoolean(Realm.columnIndices.get("CardRealm").get("isPrivate"));
    }

    @Override
    public void setIsPrivate(boolean value) {
        row.setBoolean(Realm.columnIndices.get("CardRealm").get("isPrivate"), (boolean) value);
    }

    @Override
    public String getName() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("name"));
    }

    @Override
    public void setName(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("name"), (String) value);
    }

    @Override
    public String getOwner() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("owner"));
    }

    @Override
    public void setOwner(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("owner"), (String) value);
    }

    @Override
    public String getParent() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("parent"));
    }

    @Override
    public void setParent(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("parent"), (String) value);
    }

    @Override
    public String getPhone() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("phone"));
    }

    @Override
    public void setPhone(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("phone"), (String) value);
    }

    @Override
    public String getStreet() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("CardRealm").get("street"));
    }

    @Override
    public void setStreet(String value) {
        row.setString(Realm.columnIndices.get("CardRealm").get("street"), (String) value);
    }

    @Override
    public int getVersion() {
        return (int) row.getLong(Realm.columnIndices.get("CardRealm").get("version"));
    }

    @Override
    public void setVersion(int value) {
        row.setLong(Realm.columnIndices.get("CardRealm").get("version"), (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if(!transaction.hasTable("class_CardRealm")) {
            Table table = transaction.getTable("class_CardRealm");
            table.addColumn(ColumnType.STRING, "appointment");
            table.addColumn(ColumnType.STRING, "city");
            table.addColumn(ColumnType.STRING, "company");
            table.addColumn(ColumnType.STRING, "country");
            table.addColumn(ColumnType.BOOLEAN, "dummy");
            table.addColumn(ColumnType.STRING, "email");
            table.addColumn(ColumnType.STRING, "id");
            table.addColumn(ColumnType.BOOLEAN, "isPrivate");
            table.addColumn(ColumnType.STRING, "name");
            table.addColumn(ColumnType.STRING, "owner");
            table.addColumn(ColumnType.STRING, "parent");
            table.addColumn(ColumnType.STRING, "phone");
            table.addColumn(ColumnType.STRING, "street");
            table.addColumn(ColumnType.INTEGER, "version");
            return table;
        }
        return transaction.getTable("class_CardRealm");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if(transaction.hasTable("class_CardRealm")) {
            Table table = transaction.getTable("class_CardRealm");
            if(table.getColumnCount() != 14) {
                throw new IllegalStateException("Column count does not match");
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for(long i = 0; i < 14; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }
            if (!columnTypes.containsKey("appointment")) {
                throw new IllegalStateException("Missing column 'appointment'");
            }
            if (columnTypes.get("appointment") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'appointment'");
            }
            if (!columnTypes.containsKey("city")) {
                throw new IllegalStateException("Missing column 'city'");
            }
            if (columnTypes.get("city") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'city'");
            }
            if (!columnTypes.containsKey("company")) {
                throw new IllegalStateException("Missing column 'company'");
            }
            if (columnTypes.get("company") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'company'");
            }
            if (!columnTypes.containsKey("country")) {
                throw new IllegalStateException("Missing column 'country'");
            }
            if (columnTypes.get("country") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'country'");
            }
            if (!columnTypes.containsKey("dummy")) {
                throw new IllegalStateException("Missing column 'dummy'");
            }
            if (columnTypes.get("dummy") != ColumnType.BOOLEAN) {
                throw new IllegalStateException("Invalid type 'boolean' for column 'dummy'");
            }
            if (!columnTypes.containsKey("email")) {
                throw new IllegalStateException("Missing column 'email'");
            }
            if (columnTypes.get("email") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'email'");
            }
            if (!columnTypes.containsKey("id")) {
                throw new IllegalStateException("Missing column 'id'");
            }
            if (columnTypes.get("id") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'id'");
            }
            if (!columnTypes.containsKey("isPrivate")) {
                throw new IllegalStateException("Missing column 'isPrivate'");
            }
            if (columnTypes.get("isPrivate") != ColumnType.BOOLEAN) {
                throw new IllegalStateException("Invalid type 'boolean' for column 'isPrivate'");
            }
            if (!columnTypes.containsKey("name")) {
                throw new IllegalStateException("Missing column 'name'");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'name'");
            }
            if (!columnTypes.containsKey("owner")) {
                throw new IllegalStateException("Missing column 'owner'");
            }
            if (columnTypes.get("owner") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'owner'");
            }
            if (!columnTypes.containsKey("parent")) {
                throw new IllegalStateException("Missing column 'parent'");
            }
            if (columnTypes.get("parent") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'parent'");
            }
            if (!columnTypes.containsKey("phone")) {
                throw new IllegalStateException("Missing column 'phone'");
            }
            if (columnTypes.get("phone") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'phone'");
            }
            if (!columnTypes.containsKey("street")) {
                throw new IllegalStateException("Missing column 'street'");
            }
            if (columnTypes.get("street") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'street'");
            }
            if (!columnTypes.containsKey("version")) {
                throw new IllegalStateException("Missing column 'version'");
            }
            if (columnTypes.get("version") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'version'");
            }
        }
    }

    public static List<String> getFieldNames() {
        return Arrays.asList("appointment", "city", "company", "country", "dummy", "email", "id", "isPrivate", "name", "owner", "parent", "phone", "street", "version");
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("CardRealm = [");
        stringBuilder.append("{appointment:");
        stringBuilder.append(getAppointment());
        stringBuilder.append("} ");
        stringBuilder.append("{city:");
        stringBuilder.append(getCity());
        stringBuilder.append("} ");
        stringBuilder.append("{company:");
        stringBuilder.append(getCompany());
        stringBuilder.append("} ");
        stringBuilder.append("{country:");
        stringBuilder.append(getCountry());
        stringBuilder.append("} ");
        stringBuilder.append("{dummy:");
        stringBuilder.append(isDummy());
        stringBuilder.append("} ");
        stringBuilder.append("{email:");
        stringBuilder.append(getEmail());
        stringBuilder.append("} ");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("} ");
        stringBuilder.append("{isPrivate:");
        stringBuilder.append(isIsPrivate());
        stringBuilder.append("} ");
        stringBuilder.append("{name:");
        stringBuilder.append(getName());
        stringBuilder.append("} ");
        stringBuilder.append("{owner:");
        stringBuilder.append(getOwner());
        stringBuilder.append("} ");
        stringBuilder.append("{parent:");
        stringBuilder.append(getParent());
        stringBuilder.append("} ");
        stringBuilder.append("{phone:");
        stringBuilder.append(getPhone());
        stringBuilder.append("} ");
        stringBuilder.append("{street:");
        stringBuilder.append(getStreet());
        stringBuilder.append("} ");
        stringBuilder.append("{version:");
        stringBuilder.append(getVersion());
        stringBuilder.append("} ");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        int result = 17;
        String aString_0 = getAppointment();
        result = 31 * result + (aString_0 != null ? aString_0.hashCode() : 0);
        String aString_1 = getCity();
        result = 31 * result + (aString_1 != null ? aString_1.hashCode() : 0);
        String aString_2 = getCompany();
        result = 31 * result + (aString_2 != null ? aString_2.hashCode() : 0);
        String aString_3 = getCountry();
        result = 31 * result + (aString_3 != null ? aString_3.hashCode() : 0);
        result = 31 * result + (isDummy() ? 1 : 0);
        String aString_5 = getEmail();
        result = 31 * result + (aString_5 != null ? aString_5.hashCode() : 0);
        String aString_6 = getId();
        result = 31 * result + (aString_6 != null ? aString_6.hashCode() : 0);
        result = 31 * result + (isIsPrivate() ? 1 : 0);
        String aString_8 = getName();
        result = 31 * result + (aString_8 != null ? aString_8.hashCode() : 0);
        String aString_9 = getOwner();
        result = 31 * result + (aString_9 != null ? aString_9.hashCode() : 0);
        String aString_10 = getParent();
        result = 31 * result + (aString_10 != null ? aString_10.hashCode() : 0);
        String aString_11 = getPhone();
        result = 31 * result + (aString_11 != null ? aString_11.hashCode() : 0);
        String aString_12 = getStreet();
        result = 31 * result + (aString_12 != null ? aString_12.hashCode() : 0);
        result = 31 * result + getVersion();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CardRealmRealmProxy aCardRealm = (CardRealmRealmProxy)o;
        if (getAppointment() != null ? !getAppointment().equals(aCardRealm.getAppointment()) : aCardRealm.getAppointment() != null) return false;
        if (getCity() != null ? !getCity().equals(aCardRealm.getCity()) : aCardRealm.getCity() != null) return false;
        if (getCompany() != null ? !getCompany().equals(aCardRealm.getCompany()) : aCardRealm.getCompany() != null) return false;
        if (getCountry() != null ? !getCountry().equals(aCardRealm.getCountry()) : aCardRealm.getCountry() != null) return false;
        if (isDummy() != aCardRealm.isDummy()) return false;
        if (getEmail() != null ? !getEmail().equals(aCardRealm.getEmail()) : aCardRealm.getEmail() != null) return false;
        if (getId() != null ? !getId().equals(aCardRealm.getId()) : aCardRealm.getId() != null) return false;
        if (isIsPrivate() != aCardRealm.isIsPrivate()) return false;
        if (getName() != null ? !getName().equals(aCardRealm.getName()) : aCardRealm.getName() != null) return false;
        if (getOwner() != null ? !getOwner().equals(aCardRealm.getOwner()) : aCardRealm.getOwner() != null) return false;
        if (getParent() != null ? !getParent().equals(aCardRealm.getParent()) : aCardRealm.getParent() != null) return false;
        if (getPhone() != null ? !getPhone().equals(aCardRealm.getPhone()) : aCardRealm.getPhone() != null) return false;
        if (getStreet() != null ? !getStreet().equals(aCardRealm.getStreet()) : aCardRealm.getStreet() != null) return false;
        if (getVersion() != aCardRealm.getVersion()) return false;
        return true;
    }

}
