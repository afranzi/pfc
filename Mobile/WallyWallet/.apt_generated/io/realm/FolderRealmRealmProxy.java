package io.realm;


import com.wallywallet.models.beans.*;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.internal.ColumnType;
import io.realm.internal.ImplicitTransaction;
import io.realm.internal.LinkView;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.util.*;

public class FolderRealmRealmProxy extends FolderRealm {

    @Override
    public int getCardsCounts() {
        return (int) row.getLong(Realm.columnIndices.get("FolderRealm").get("cardsCounts"));
    }

    @Override
    public void setCardsCounts(int value) {
        row.setLong(Realm.columnIndices.get("FolderRealm").get("cardsCounts"), (long) value);
    }

    @Override
    public boolean isDummy() {
        return (boolean) row.getBoolean(Realm.columnIndices.get("FolderRealm").get("dummy"));
    }

    @Override
    public void setDummy(boolean value) {
        row.setBoolean(Realm.columnIndices.get("FolderRealm").get("dummy"), (boolean) value);
    }

    @Override
    public int getFoldersCounts() {
        return (int) row.getLong(Realm.columnIndices.get("FolderRealm").get("foldersCounts"));
    }

    @Override
    public void setFoldersCounts(int value) {
        row.setLong(Realm.columnIndices.get("FolderRealm").get("foldersCounts"), (long) value);
    }

    @Override
    public String getId() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("FolderRealm").get("id"));
    }

    @Override
    public void setId(String value) {
        row.setString(Realm.columnIndices.get("FolderRealm").get("id"), (String) value);
    }

    @Override
    public boolean isIsPrivate() {
        return (boolean) row.getBoolean(Realm.columnIndices.get("FolderRealm").get("isPrivate"));
    }

    @Override
    public void setIsPrivate(boolean value) {
        row.setBoolean(Realm.columnIndices.get("FolderRealm").get("isPrivate"), (boolean) value);
    }

    @Override
    public String getName() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("FolderRealm").get("name"));
    }

    @Override
    public void setName(String value) {
        row.setString(Realm.columnIndices.get("FolderRealm").get("name"), (String) value);
    }

    @Override
    public String getOwner() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("FolderRealm").get("owner"));
    }

    @Override
    public void setOwner(String value) {
        row.setString(Realm.columnIndices.get("FolderRealm").get("owner"), (String) value);
    }

    @Override
    public String getParent() {
        return (java.lang.String) row.getString(Realm.columnIndices.get("FolderRealm").get("parent"));
    }

    @Override
    public void setParent(String value) {
        row.setString(Realm.columnIndices.get("FolderRealm").get("parent"), (String) value);
    }

    @Override
    public int getVersion() {
        return (int) row.getLong(Realm.columnIndices.get("FolderRealm").get("version"));
    }

    @Override
    public void setVersion(int value) {
        row.setLong(Realm.columnIndices.get("FolderRealm").get("version"), (long) value);
    }

    public static Table initTable(ImplicitTransaction transaction) {
        if(!transaction.hasTable("class_FolderRealm")) {
            Table table = transaction.getTable("class_FolderRealm");
            table.addColumn(ColumnType.INTEGER, "cardsCounts");
            table.addColumn(ColumnType.BOOLEAN, "dummy");
            table.addColumn(ColumnType.INTEGER, "foldersCounts");
            table.addColumn(ColumnType.STRING, "id");
            table.addColumn(ColumnType.BOOLEAN, "isPrivate");
            table.addColumn(ColumnType.STRING, "name");
            table.addColumn(ColumnType.STRING, "owner");
            table.addColumn(ColumnType.STRING, "parent");
            table.addColumn(ColumnType.INTEGER, "version");
            return table;
        }
        return transaction.getTable("class_FolderRealm");
    }

    public static void validateTable(ImplicitTransaction transaction) {
        if(transaction.hasTable("class_FolderRealm")) {
            Table table = transaction.getTable("class_FolderRealm");
            if(table.getColumnCount() != 9) {
                throw new IllegalStateException("Column count does not match");
            }
            Map<String, ColumnType> columnTypes = new HashMap<String, ColumnType>();
            for(long i = 0; i < 9; i++) {
                columnTypes.put(table.getColumnName(i), table.getColumnType(i));
            }
            if (!columnTypes.containsKey("cardsCounts")) {
                throw new IllegalStateException("Missing column 'cardsCounts'");
            }
            if (columnTypes.get("cardsCounts") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'cardsCounts'");
            }
            if (!columnTypes.containsKey("dummy")) {
                throw new IllegalStateException("Missing column 'dummy'");
            }
            if (columnTypes.get("dummy") != ColumnType.BOOLEAN) {
                throw new IllegalStateException("Invalid type 'boolean' for column 'dummy'");
            }
            if (!columnTypes.containsKey("foldersCounts")) {
                throw new IllegalStateException("Missing column 'foldersCounts'");
            }
            if (columnTypes.get("foldersCounts") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'foldersCounts'");
            }
            if (!columnTypes.containsKey("id")) {
                throw new IllegalStateException("Missing column 'id'");
            }
            if (columnTypes.get("id") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'id'");
            }
            if (!columnTypes.containsKey("isPrivate")) {
                throw new IllegalStateException("Missing column 'isPrivate'");
            }
            if (columnTypes.get("isPrivate") != ColumnType.BOOLEAN) {
                throw new IllegalStateException("Invalid type 'boolean' for column 'isPrivate'");
            }
            if (!columnTypes.containsKey("name")) {
                throw new IllegalStateException("Missing column 'name'");
            }
            if (columnTypes.get("name") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'name'");
            }
            if (!columnTypes.containsKey("owner")) {
                throw new IllegalStateException("Missing column 'owner'");
            }
            if (columnTypes.get("owner") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'owner'");
            }
            if (!columnTypes.containsKey("parent")) {
                throw new IllegalStateException("Missing column 'parent'");
            }
            if (columnTypes.get("parent") != ColumnType.STRING) {
                throw new IllegalStateException("Invalid type 'String' for column 'parent'");
            }
            if (!columnTypes.containsKey("version")) {
                throw new IllegalStateException("Missing column 'version'");
            }
            if (columnTypes.get("version") != ColumnType.INTEGER) {
                throw new IllegalStateException("Invalid type 'int' for column 'version'");
            }
        }
    }

    public static List<String> getFieldNames() {
        return Arrays.asList("cardsCounts", "dummy", "foldersCounts", "id", "isPrivate", "name", "owner", "parent", "version");
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("FolderRealm = [");
        stringBuilder.append("{cardsCounts:");
        stringBuilder.append(getCardsCounts());
        stringBuilder.append("} ");
        stringBuilder.append("{dummy:");
        stringBuilder.append(isDummy());
        stringBuilder.append("} ");
        stringBuilder.append("{foldersCounts:");
        stringBuilder.append(getFoldersCounts());
        stringBuilder.append("} ");
        stringBuilder.append("{id:");
        stringBuilder.append(getId());
        stringBuilder.append("} ");
        stringBuilder.append("{isPrivate:");
        stringBuilder.append(isIsPrivate());
        stringBuilder.append("} ");
        stringBuilder.append("{name:");
        stringBuilder.append(getName());
        stringBuilder.append("} ");
        stringBuilder.append("{owner:");
        stringBuilder.append(getOwner());
        stringBuilder.append("} ");
        stringBuilder.append("{parent:");
        stringBuilder.append(getParent());
        stringBuilder.append("} ");
        stringBuilder.append("{version:");
        stringBuilder.append(getVersion());
        stringBuilder.append("} ");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + getCardsCounts();
        result = 31 * result + (isDummy() ? 1 : 0);
        result = 31 * result + getFoldersCounts();
        String aString_3 = getId();
        result = 31 * result + (aString_3 != null ? aString_3.hashCode() : 0);
        result = 31 * result + (isIsPrivate() ? 1 : 0);
        String aString_5 = getName();
        result = 31 * result + (aString_5 != null ? aString_5.hashCode() : 0);
        String aString_6 = getOwner();
        result = 31 * result + (aString_6 != null ? aString_6.hashCode() : 0);
        String aString_7 = getParent();
        result = 31 * result + (aString_7 != null ? aString_7.hashCode() : 0);
        result = 31 * result + getVersion();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FolderRealmRealmProxy aFolderRealm = (FolderRealmRealmProxy)o;
        if (getCardsCounts() != aFolderRealm.getCardsCounts()) return false;
        if (isDummy() != aFolderRealm.isDummy()) return false;
        if (getFoldersCounts() != aFolderRealm.getFoldersCounts()) return false;
        if (getId() != null ? !getId().equals(aFolderRealm.getId()) : aFolderRealm.getId() != null) return false;
        if (isIsPrivate() != aFolderRealm.isIsPrivate()) return false;
        if (getName() != null ? !getName().equals(aFolderRealm.getName()) : aFolderRealm.getName() != null) return false;
        if (getOwner() != null ? !getOwner().equals(aFolderRealm.getOwner()) : aFolderRealm.getOwner() != null) return false;
        if (getParent() != null ? !getParent().equals(aFolderRealm.getParent()) : aFolderRealm.getParent() != null) return false;
        if (getVersion() != aFolderRealm.getVersion()) return false;
        return true;
    }

}
