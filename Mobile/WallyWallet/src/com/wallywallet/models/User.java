package com.wallywallet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.wallywallet.models.Item.Type;
import com.wallywallet.models.beans.UserRealm;


public class User implements AbstractItem<UserRealm> {

	@JsonProperty(value="_id")
	private String id;
	
	@JsonProperty(value="__v")
	private Integer version;
	
	@JsonProperty(value="facebook")
	private FacebookUser user;
	
	@JsonProperty(value="cardCount")
	private Integer cardCount;
	
	@JsonProperty(value="folderCount")
	private Integer folderCount;
	
	@JsonProperty(value="dummy")
	private Boolean dummy;
	
	public User() {}
		
	public FacebookUser getUser() {
		return user;
	}

	public void setUser(FacebookUser user) {
		this.user = user;
	}

	public Integer getCardCount() {
		return cardCount;
	}

	public void setCardCount(Integer cardCount) {
		this.cardCount = cardCount;
	}

	public Integer getFolderCount() {
		return folderCount;
	}

	public void setFolderCount(Integer folderCount) {
		this.folderCount = folderCount;
	}

	public Boolean getDummy() {
		return dummy;
	}

	public void setDummy(Boolean dummy) {
		this.dummy = dummy;
	}

	public void setId(String id) {
		this.id = id;
	}

	@JsonIgnore
	public String getTitle() {
		return user.getName();
	}

	@JsonIgnore
	public String getValue() {
		return String.format("Cards %d  |  Folders %d", cardCount, folderCount);
	}

	@JsonIgnore
	public String getOwner() {
		return null;
	}

	@JsonIgnore
	public String getParent() {
		return null;
	}

	@JsonIgnore
	public Type getType() {
		return Type.USER;
	}

	public String getId() {
		return user.getId();
	}
	
	@Override
	public UserRealm clone(UserRealm item) {
		item.setDummy(this.dummy);
		item.setId(this.user.getId());
		item.setVersion(this.version);
		item.setCardCount(this.cardCount);
		item.setFolderCount(folderCount);
		item.setEmail(this.user.getEmail());
		item.set_id(id);
		item.setPhoto(this.user.getPhoto());
		item.setName(user.getName());
		return item;
	}
	
	@Override
	public User copy(UserRealm item) {
		folderCount = item.getFolderCount();
		cardCount = item.getCardCount();
		dummy = item.isDummy();
		id = item.getId();
		version = item.getVersion();
		user = new FacebookUser();
		user.setName(item.getName());
		user.setEmail(item.getEmail());
		user.setPhoto(item.getPhoto());
		return this;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(User.class)
				.add("id",id)
				.add("facebook", user)
				.add("cardCount", cardCount)
				.add("folderCount", folderCount)
				.add("dummy", dummy)
				.toString();
	}
	
}