package com.wallywallet.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

public class CardAddress {
	
	@JsonProperty(value="street")
	private String street;
	
	@JsonProperty(value="city")
	private String city;
	
	@JsonProperty(value="country")
	private String country;
	
	public CardAddress(){}
	
	public CardAddress(String street, String city, String country) {
		this.street = street;
		this.city = city;
		this.country = country;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(CardAddress.class)
				.add("country", country)
				.add("city", city)
				.add("street", street)
				.toString();
	}
}