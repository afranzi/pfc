package com.wallywallet.models;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.wallywallet.models.beans.CardRealm;

public class Card implements AbstractItem<CardRealm> {
	
	@JsonProperty(value="_id")
	private String id;
	
	@JsonProperty(value="__v")
	private int version;
	
	@JsonProperty(value="isPrivate")
	private boolean isPrivate;
	
	@JsonProperty(value="notesNumber")
	private int notesNumber;
	
	@JsonProperty(value="owner")
	private String owner;
	
	@JsonProperty(value="parent")
	private String parent;
	
	@JsonProperty(value="tags")
	private List<String> tags;
	
	@JsonProperty(value="dummy")
	private boolean dummy;
	
	@JsonProperty(value="company")
	private String company;
	
	@JsonProperty(value="email")
	private String email;
	
	@JsonProperty(value="phone")
	private String phone;
	
	@JsonProperty(value="guy")
	private Guy guy = new Guy();
	
	@JsonProperty(value="address")
	private CardAddress address = new CardAddress();

	public Card() {}
	
	public Card(String id, String owner, String parent, Set<String> tags, 
			String street, String city, String country, String company, String email,
			String phone, String name, String appointment) {
		
		this.id = id;
		this.owner = owner;
		this.parent = parent;
		this.tags = Lists.newArrayList(tags);		
		this.address = new CardAddress(street, city, country);
		this.company = company;
		this.email = email;
		this.phone = phone;
		this.guy = new Guy(name, appointment);
	}
	
	@JsonIgnore
	public Item.Type getType() {
		return Item.Type.CARD;
	}
	
	public String getId() {
		return id;
	}

	public String getStreet() {
		return address.getStreet();
	}
	
	public void setStreet(String street) {
		address.setStreet(street);
	}
	
	public String getCity() {
		return address.getCity();
	}

	public void setCity(String city) {
		address.setCity(city);
	}

	public String getCountry() {
		return address.getCountry();
	}

	public void setCountry(String country) {
		address.setCountry(country);
	}
	
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return guy.getName();
	}

	public void setName(String name) {
		if(guy == null) guy = new Guy();
		guy.setName(name);
	}

	public String getAppointment() {
		return guy.getAppointment();
	}

	public void setAppointment(String appointment) {
		if(guy == null) guy = new Guy();
		guy.setAppointment(appointment);
	}
	
	public void setParent(String parent) {
		this.parent = parent;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(Card.class)
				.add("company", company)
				.add("address", address)
				.add("guy", guy)
				.add("email", email)
				.add("owner", owner)
				.add("parent", parent)
				.add("phone", phone)
				.toString();
	}
		
	@Override
	public CardRealm clone(CardRealm item) {
		item.setCompany(getString(company));
		item.setCountry(address.getCountry());
		item.setCity(address.getCity());
		item.setStreet(address.getStreet());
		item.setEmail(email);
		item.setPhone(phone);
		item.setName(getString(getName()));
		item.setAppointment(getString(getAppointment()));
		item.setParent(getString(parent));
		item.setOwner(owner);
		item.setDummy(dummy);
		item.setId(id);
		item.setVersion(version);
		item.setIsPrivate(isPrivate);
		return null;
	}
	
	@Override
	public Card copy(CardRealm item) {
		setCompany(item.getCompany());
		setCountry(item.getCountry());
		setCity(item.getCity());
		setStreet(item.getStreet());
		setEmail(item.getStreet());
		setPhone(item.getPhone());
		setName(item.getName());
		setAppointment(item.getAppointment());
		setParent(item.getParent());
		owner = item.getOwner();
		dummy = item.isDummy();
		id = item.getId();
		version = item.getVersion();
		isPrivate = item.isIsPrivate();
		
		return this;
	}
	
	private String getString(String value) {
		if(value == null) return "";
		return value;
	}
	
	public static class Factory {

		public Factory() {}

		public Card getCard(String id, String owner, String parent, Set<String> tags, 
				String street, String city, String country, String company, String email,
				String phone) {
			return getCard(id, owner, parent, tags, street, city, country, company, email, phone, null, null);
		}

		public Card getCard(String id, String owner, String parent, Set<String> tags, 
				String street, String city, String country, String company, String email,
				String phone, String name, String appointment) {
			return new Card(id, owner, parent, tags, street, city, country, company, email, phone, name, appointment);
		}
	}

	@JsonIgnore
	public String getTitle() {		
		return company;
	}

	@JsonIgnore
	public String getValue() {
		String value = "";
		
		if(guy != null) {
			if(guy.getName() != null && guy.getName().length() > 0) {
				value += guy.getName();
				if(guy.getAppointment() != null) {
					value += "  |  " + guy.getAppointment();
				}
			}
		}
		
		return value;
	}

	public String getOwner() {
		return owner;
	}

	public String getParent() {
		return parent;
	}

}