package com.wallywallet.models;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.wallywallet.models.Item.Type;
import com.wallywallet.models.beans.FolderRealm;

public class Folder implements AbstractItem<FolderRealm> {

	@JsonProperty(value="_id")
	private String id;

	@JsonProperty(value="__v")
	private Integer version;

	@JsonProperty(value="dummy")
	private boolean dummy;

	@JsonProperty(value="isPrivate")
	private boolean isPrivate;

	@JsonProperty(value="notesNumber")
	private Integer notesNumber;

	@JsonProperty(value="owner")
	private String owner;

	@JsonProperty(value="parent")
	private String parent;

	@JsonProperty(value="tags")
	private List<String> tags;

	@JsonProperty(value="name")
	private String name;

	@JsonProperty(value="cardsCount")
	private Integer cardsCounts;

	@JsonProperty(value="foldersCount")
	private Integer foldersCounts;

	public Folder() {};

	Folder(String id, String owner, String parent, Set<String> tags, String name, 
			Integer cardsCounts, Integer folderCounts) {
		this.id = id;
		this.owner = owner;
		this.parent = parent;
		if(tags != null) this.tags = Lists.newArrayList(tags);
		this.name = name;
		this.cardsCounts = cardsCounts;
		this.foldersCounts = folderCounts;
	}

	public String getId() {
		return id;
	}

	public Type getType() {
		return Type.FOLDER;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCardsCounts() {
		return cardsCounts;
	}

	public void setCardsCounts(Integer cardsCounts) {
		this.cardsCounts = cardsCounts;
	}

	public Integer getFoldersCounts() {
		return foldersCounts;
	}

	public void setFoldersCounts(Integer foldersCounts) {
		this.foldersCounts = foldersCounts;
	}

	@Override
	public FolderRealm clone(FolderRealm item) {
		item.setName(getString(name));
		item.setParent(getString(parent));
		item.setFoldersCounts(foldersCounts);
		item.setCardsCounts(cardsCounts);
		item.setOwner(owner);
		item.setDummy(dummy);
		item.setId(id);
		item.setVersion(version);
		item.setIsPrivate(isPrivate);
		return item;
	}

	@Override
	public Folder copy(FolderRealm item) {
		setName(item.getName());
		parent = item.getParent();
		setFoldersCounts(item.getFoldersCounts());
		setCardsCounts(item.getCardsCounts());
		owner = item.getOwner();
		dummy = item.isDummy();
		id = item.getId();
		version = item.getVersion();
		isPrivate = item.isIsPrivate();
		return this;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(Folder.class)
				.add("name", name)
				.add("parent", parent)
				.add("foldersCounts", foldersCounts)
				.add("cardsCounts", cardsCounts)
				.add("owner", owner)
				.add("id", id)
				.add("dummy", dummy)
				.add("version", version)
				.add("isPrivate", isPrivate)
				.toString();
	}

	private String getString(String value) {
		if(value == null) return "";
		return value;
	}

	public static class Factory {

		public Factory() {};

		public Folder getFolder(String id, String owner, String parent, Set<String> tags, String name) {
			return getFolder(id, owner, parent, tags, name, 0, 0);
		}

		public Folder getFolder(String id, String owner, String parent, Set<String> tags, String name, 
				Integer cardsCounts, Integer folderCounts) {
			return new Folder(id, owner, parent, tags, name, cardsCounts, folderCounts);
		}
	}	

	public String getTitle() {
		return name;
	}

	public String getValue() {		
		return String.format("Cards [%d] - Folders [%d]", cardsCounts, foldersCounts);
	}

	public String getOwner() {
		return owner;
	}

	public String getParent() {
		return parent;
	}
}