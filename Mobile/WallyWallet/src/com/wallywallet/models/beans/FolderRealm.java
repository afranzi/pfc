package com.wallywallet.models.beans;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class FolderRealm extends RealmObject {

	private String id;
	private int version;
	private boolean dummy;
	private boolean isPrivate;

	private String owner;
	private String parent;
	private String name;

	private int cardsCounts;
	private int foldersCounts;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public boolean isDummy() {
		return dummy;
	}

	public void setDummy(boolean dummy) {
		this.dummy = dummy;
	}

	public boolean isIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCardsCounts() {
		return cardsCounts;
	}

	public void setCardsCounts(int cardsCounts) {
		this.cardsCounts = cardsCounts;
	}

	public int getFoldersCounts() {
		return foldersCounts;
	}

	public void setFoldersCounts(int foldersCounts) {
		this.foldersCounts = foldersCounts;
	}
}