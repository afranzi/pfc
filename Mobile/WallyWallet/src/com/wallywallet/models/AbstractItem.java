package com.wallywallet.models;

import io.realm.RealmObject;

import com.wallywallet.models.Item.Type;

public interface AbstractItem<T extends RealmObject>  {
	public abstract String getTitle();
	public abstract String getValue();
	public abstract String getOwner();
	public abstract String getParent();
	public abstract Type getType();
	public abstract String getId();
	
	public abstract T clone(T item);
	public abstract AbstractItem<T> copy(T item);
}