package com.wallywallet.models;

import io.realm.RealmObject;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

public class Guy {
	
	@JsonProperty(value="name")
	private String name;
	
	@JsonProperty(value="appointment")
	private String appointment;	
	
	public Guy() {}
	
	public Guy (String name, String appointment) {
		this.name = name;
		this.appointment = appointment;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAppointment() {
		return appointment;
	}

	public void setAppointment(String appointment) {
		this.appointment = appointment;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(Guy.class)
				.add("name", name)
				.add("appointment", appointment)
				.toString();
	}
}