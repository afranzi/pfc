package com.wallywallet.models;

import io.realm.RealmObject;

import java.util.Set;

import com.google.common.collect.Sets;
import com.wallywallet.models.beans.CardRealm;
import com.wallywallet.models.beans.FolderRealm;
import com.wallywallet.models.beans.UserRealm;

public abstract class Item {

	public enum Type {
		FOLDER(Folder.class, FolderRealm.class, "folders"), 
		CARD(Card.class, CardRealm.class, "cards"),
		USER(User.class, UserRealm.class, "users");

		Class<?> typeClazz;
		Class<? extends RealmObject> realmClazz;
		String urlBase;

		Type(Class<?> clazz, Class<? extends RealmObject> realmClazz, String urlBase) {
			this.typeClazz = clazz;
			this.urlBase = urlBase;
			this.realmClazz = realmClazz;
		}

		public Class<?> getTypeClass() {
			return typeClazz;
		}
		
		public Class<? extends RealmObject> getRealmClass() {
			return realmClazz;
		}
		
		public String getUserBase() {
			return urlBase;
		}
	}

	private String id;
	private String owner;
	private String parent;
	private Set<String> tags = Sets.newHashSet();

	public Item (String id, String owner, String parent, Set<String> tags) {
		this.id = id;
		this.owner = owner;
		this.parent = parent;
		this.tags = tags;
	}

	public String getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}

	public String getParent() {
		return parent;
	}

	public Set<String> getTags() {
		return tags;
	}

	public abstract Type getType();
}