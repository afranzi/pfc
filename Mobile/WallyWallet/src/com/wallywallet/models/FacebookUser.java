package com.wallywallet.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;

public class FacebookUser {

	@JsonProperty(value="id")
	private String id;

	@JsonProperty(value="token")
	private String token;

	@JsonProperty(value="mobileToken")
	private String mobileToken;

	@JsonProperty(value="email")
	private String email;

	@JsonProperty(value="photo")
	private String photo;

	@JsonProperty(value="name")
	private String name;

	public FacebookUser() {}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMobileToken() {
		return mobileToken;
	}

	public void setMobileToken(String mobileToken) {
		this.mobileToken = mobileToken;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(FacebookUser.class)
				.add("id", id)
				.add("token", token)
				.add("mobileToken", mobileToken)
				.add("email", email)
				.add("photo", photo)
				.add("name", name)
				.toString();
	}
}