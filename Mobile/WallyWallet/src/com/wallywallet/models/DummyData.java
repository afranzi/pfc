package com.wallywallet.models;

import com.google.common.collect.ImmutableList;

public class DummyData {
	
	private static final String OWNER = "owner";
	
	public static ImmutableList<Folder> items = new ImmutableList.Builder<Folder>()
			.add(new Folder("FA", OWNER, null, null, "Folder A", 0, 0))
			.add(new Folder("FB", OWNER, null, null, "Folder B", 0, 0))
			.add(new Folder("FC", OWNER, "Folder A", null, "Folder C", 0, 0))
			.add(new Folder("FD", OWNER, "Folder C", null, "Folder D", 0, 0))
			.add(new Folder("FE", OWNER, null, null, "Folder E", 0, 0))
			.add(new Folder("FF", OWNER, null, null, "Folder F", 0, 0))
			.add(new Folder("FG", OWNER, "Folder B", null, "Folder G", 0, 0))
			.add(new Folder("FH", OWNER, "Folder E", null, "Folder H", 0, 0))
			.add(new Folder("FI", OWNER, null, null, "Folder I", 0, 0))
			.add(new Folder("FJ", OWNER, "Folder J", null, "Folder J", 0, 0))
			.build();
}