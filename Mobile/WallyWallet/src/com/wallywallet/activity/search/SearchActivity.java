package com.wallywallet.activity.search;

import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.wallywallet.R;
import com.wallywallet.activity.card.CardActivity;
import com.wallywallet.activity.content.ContentActivity;
import com.wallywallet.activity.content.RowAdapter;
import com.wallywallet.models.AbstractItem;
import com.wallywallet.models.Card;
import com.wallywallet.models.Item.Type;
import com.wallywallet.models.User;
import com.wallywallet.network.RestClient;
import com.wallywallet.utils.ActivityUtils;
import com.wallywallet.utils.IntentModule;

public class SearchActivity extends Activity {

	private String TAG = "SEARCH";
	private IntentModule module;
	
	private EditText contentToSearch;
	private Boolean moreResults = true;
	private Boolean flagLoading = false;
	private Integer pages = 0;
	private Integer RESULTS_PAGE = 40;
	
	RowAdapter<AbstractItem> adapter = null;

	enum SearchAction {
		USER, CARD, TAG
	}

	SearchAction activeAction = SearchAction.USER;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);

		// Get the message from the intent
		Intent intent = getIntent();
		module = IntentModule.Factory.build(intent);

		contentToSearch = (EditText) findViewById(R.id.text_to_search);
		contentToSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					executeSearch();
					return true;
				}
				return false;
			}
		});
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public void executeSearch() {
		moreResults = true;
		pages = 0;
		if(adapter != null) adapter.clear(); //New search ---> new list
		executeSearch(0);
	}
	
	public void executeSearch(Integer page) {
		String toSearch = contentToSearch.getText().toString();
		getItems(activeAction, toSearch, page);
	}

	public void search_users(View view) {
		activeAction = SearchAction.USER;
		executeSearch();
	}

	public void search_cards(View view) {
		activeAction = SearchAction.CARD;
		executeSearch();
	}

	public void search_tags(View view) {
		activeAction = SearchAction.TAG;
		executeSearch();
	}

	private void getItems(final SearchAction action, String contentToSearch, Integer page) {
		final RestClient client = RestClient.Factory.build(getApplicationContext());

		String getItemsUrl = "";		
		if(SearchAction.CARD.equals(action)) {
			getItemsUrl = String.format("search/cards/%d/%d/%s", page, RESULTS_PAGE, contentToSearch);
		} else if(SearchAction.TAG.equals(action)) {
			getItemsUrl = String.format("search/tags/%d/%d/%s", page, RESULTS_PAGE, contentToSearch);
		} else {
			getItemsUrl = String.format("search/users/%d/%d/%s", page, RESULTS_PAGE, contentToSearch);
		}

		client.get(getItemsUrl, null, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray items) {
				ObjectMapper mapper = new ObjectMapper();
				List<AbstractItem> itemsList = Lists.newArrayList();
				try {
					for(int i = 0; i < items.length(); ++i) {
						JSONObject jsonCard = items.getJSONObject(i);

						AbstractItem item;
						if(SearchAction.USER.equals(action)) {
							item = mapper.readValue(jsonCard.toString(), User.class); 
						}
						else {
							item = mapper.readValue(jsonCard.toString(), Card.class);
						}						
						itemsList.add(item);
						Log.i(TAG, item.toString());
					}
					flagLoading = false;
					if(itemsList.size() == 0) moreResults = false;
					else refreshList(itemsList);
					
					ActivityUtils.showToast(getApplicationContext(), String.format("Received %d items more", itemsList.size()));
				} catch(Exception e) {
					System.out.println(e.getStackTrace());
					throw Throwables.propagate(e);
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.search, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			onBackPressed();
		}
		return super.onOptionsItemSelected(item);
	}

	private void refreshList(List<AbstractItem> cards) {
		// 1. pass context and data to the custom adapter
		if(adapter != null) {
			adapter.addItems(cards);
		}
		else {
			adapter = new RowAdapter<AbstractItem>(this, cards);

			// 2. Get ListView from activity_main.xml
			final ListView listView = (ListView) findViewById(R.id.list);
			final Activity activity = this;

			// 3. setListAdapter
			listView.setAdapter(adapter);

			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
					AbstractItem item = (AbstractItem) listView.getItemAtPosition(position);
					Log.i(TAG, String.format("Click item [%s] at position [%d]", item, position));

					if(item.getType().equals(Type.USER)) {
						Intent intent = new Intent(activity, ContentActivity.class);
						Log.i(TAG, "Changing to ContentActivity");

						User user = (User) item;
						intent.putExtra(ActivityUtils.CURRENT_USER_ID, user.getUser().getId());
						intent.putExtra(ActivityUtils.CURRENT_USER_NAME, user.getUser().getName());
						IntentModule.Factory.persist(module, intent);
						startActivity(intent);
					} else {
						ObjectMapper mapper = new ObjectMapper();
						try {
							ObjectNode jsonCard = mapper.valueToTree(item);
							Intent intent = new Intent(activity, CardActivity.class);
							Log.i(TAG, "Changing to CardActivity");

							intent.putExtra(ActivityUtils.FACEBOOK_ID, module.getFacebookID());
							intent.putExtra(ActivityUtils.CARD_JSON, jsonCard.toString());
							startActivity(intent);
						} catch (Exception e) {
							throw Throwables.propagate(e);
						}
					}
				}
			});
			
			// 4. set listenes on scroll to load in lazy mode
			listView.setOnScrollListener(new OnScrollListener() {

		        public void onScrollStateChanged(AbsListView view, int scrollState) {}

		        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
		            if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0) {
		                if(!flagLoading && moreResults) {
		                	Log.i(TAG, String.format("First[%d] - Visible[%d] - Total[%d]", firstVisibleItem, visibleItemCount, totalItemCount));
		                	flagLoading = true;
		                	++pages;
		                	executeSearch(pages);
		                }
		            }
		        }
		    });
		}		
	}
}
