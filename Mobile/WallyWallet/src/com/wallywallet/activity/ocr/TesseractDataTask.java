package com.wallywallet.activity.ocr;

import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.util.Log;

import com.wallywallet.utils.AsyncHandler;
import com.wallywallet.utils.FileManager;

public class TesseractDataTask extends AsyncTask <Void, Void, Void> {

    private final AssetManager manager;
    private final String destFolder;
    private String TAG = "CAPTURE";
	private AsyncHandler handler;
	
    public TesseractDataTask(AssetManager manager, String destPath, AsyncHandler handler) {
        this.manager = manager;
        this.destFolder = destPath;
		this.handler = handler;
    }
 
    @Override
    protected void onPreExecute() {
        Log.i(TAG, "PRE_EXECUTE");
    }
     
    @Override
    protected void onPostExecute(Void result) {
    	Log.i(TAG, "POST_EXECUTE");
        if(handler != null) {
        	handler.postExecute(true, "OK");
        }
    }
     
    @Override
    protected Void doInBackground(Void... params) {
    	Log.i(TAG, "START doInBackground");
        FileManager.copyAsset("tesseract", destFolder, manager);
        Log.i(TAG, "END doInBackground");
        return null;
    }
     
}