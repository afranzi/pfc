package com.wallywallet.activity.ocr;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.google.common.base.Throwables;
import com.googlecode.tesseract.android.TessBaseAPI;
import com.wallywallet.utils.AsyncHandler;

public class OCREngine extends AsyncTask<String, Void, Boolean> {

	private TessBaseAPI baseApi;
	private OcrResult ocrResult;

	private long timeRequired;
	private AsyncHandler handler;

	OCREngine(TessBaseAPI baseApi, AsyncHandler handler) {
		this.baseApi = baseApi;
		this.handler = handler;
	}

	@Override
	protected void onPreExecute() {

	}

	@Override
	protected Boolean doInBackground(String... imagePaths) {
		if(imagePaths.length != 1) return false;
		String imagePath = imagePaths[0];

		long start = System.currentTimeMillis();

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;

		Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options );
		// _path = path to the image to be OCRed
		ExifInterface exif;
		try {
			exif = new ExifInterface(imagePath);

			int exifOrientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			int rotate = 0;

			switch (exifOrientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			}

			if (rotate != 0) {
				int w = bitmap.getWidth();
				int h = bitmap.getHeight();

				// Setting pre rotate
				Matrix mtx = new Matrix();
				mtx.preRotate(rotate);

				// Rotating Bitmap & convert to ARGB_8888, required by tess
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
			}
		} catch (Exception e) {
			throw Throwables.propagate(e);
		}
		bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);

		String textResult;

		try {     
			baseApi.setPageSegMode(TessBaseAPI.OEM_TESSERACT_CUBE_COMBINED);
			baseApi.setImage(bitmap);
			textResult = baseApi.getUTF8Text();

			timeRequired = System.currentTimeMillis() - start;

			// Check for failure to recognize text
			if (textResult == null || textResult.equals("")) {
				return false;
			}
			ocrResult = new OcrResult();
			ocrResult.setWordConfidences(baseApi.wordConfidences());
			ocrResult.setMeanConfidence(baseApi.meanConfidence());
			ocrResult.setRegionBoundingBoxes(baseApi.getRegions().getBoxRects());
			ocrResult.setTextlineBoundingBoxes(baseApi.getTextlines().getBoxRects());
			ocrResult.setWordBoundingBoxes(baseApi.getWords().getBoxRects());
			ocrResult.setStripBoundingBoxes(baseApi.getStrips().getBoxRects());
		} catch (RuntimeException e) {
			Log.e("OcrRecognizeAsyncTask", "Caught RuntimeException in request to Tesseract. Setting state to CONTINUOUS_STOPPED.");
			e.printStackTrace();
			try {
				baseApi.clear();
				handler.postExecute(false, "Caught RuntimeException in request to Tesseract");
			} catch (NullPointerException e1) {
				// Continue
			}
			return false;
		}
		timeRequired = System.currentTimeMillis() - start;
		ocrResult.setBitmap(bitmap);
		ocrResult.setText(textResult);
		ocrResult.setRecognitionTimeRequired(timeRequired);
		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (handler != null) {
			// Send results for single-shot mode recognition.
			if (result) {
				handler.postExecute(true, ocrResult);
			} else {
				handler.postExecute(false, ocrResult);
			}
		}
	}
}