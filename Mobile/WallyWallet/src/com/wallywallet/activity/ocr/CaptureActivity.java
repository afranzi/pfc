package com.wallywallet.activity.ocr;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.wallywallet.R;
import com.wallywallet.utils.ActivityUtils;
import com.wallywallet.utils.AsyncHandler;
import com.wallywallet.utils.FileManager;
import com.wallywallet.utils.IntentModule;

public class CaptureActivity extends Activity {

	private String TAG = "CAPTURE";

	private String ACTUAL_PHOTO = "ACTUAL_PHOTO";
	private String ASSETS_LOAD = "ASSETS_LOAD";

	private IntentModule module;
	protected ImageButton takeButton;
	protected ImageView imageView;
	protected TextView textView;
	protected String imagePath;
	protected String tesseractPath;
	protected boolean taken = false;;
	protected boolean assetsLoaded = false;
	private OcrResult ocrResult = null;

	private ProgressDialog dialog;
	private String currentFolder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_capture);

		Intent intent = getIntent();
		module = IntentModule.Factory.build(intent);
		currentFolder = intent.getStringExtra(ActivityUtils.CARD_PARENT);

		imageView = ( ImageView ) findViewById( R.id.image );
		textView = ( TextView ) findViewById( R.id.field );
		takeButton = ( ImageButton ) findViewById( R.id.button );
		takeButton.setOnClickListener(new ButtonClickHandler());

		if( savedInstanceState != null && savedInstanceState.containsKey(ACTUAL_PHOTO)) {
			imagePath = savedInstanceState.getString(ACTUAL_PHOTO, null);
			assetsLoaded = savedInstanceState.getBoolean(ASSETS_LOAD, false);
		}

		if(imagePath == null) {
			String imagesFolder = FileManager.mkdir(FileManager.FOLDER_IMAGES, getApplicationContext());

			if(imagesFolder != null) {
				imagePath = imagesFolder + "/image_" + UUID.randomUUID().toString() + ".jpg";
				Log.i(TAG, String.format("Path [%s]", imagePath));
			}

			enableButtons(R.id.add_card, false);
		}

		dialog = new ProgressDialog(this);
		loadAssets();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void loadAssets() {
		tesseractPath = FileManager.mkdir(FileManager.FOLDER_TESSER, getApplicationContext());

		Boolean syncRequired = !FileManager.checkIfExists(tesseractPath, "tessdata/eng.traineddata");
		//syncRequired = syncRequired || !FileManager.checkIfExists(tesseractPath, "tessdata/spa.traineddata");

		if(!assetsLoaded && syncRequired) {
			showProgressDialog("Loading", "Wait while checking OCR files...");

			/* Load OCR assets */
			String baseFolder = FileManager.mkdir(FileManager.FOLDER_BASE, getApplicationContext());

			TesseractDataTask tesserTask = new TesseractDataTask(getAssets(), baseFolder, new AsyncHandler() {

				@Override
				public void postExecute(Boolean isSuccess, Object result) {
					Log.i(TAG, "LOADING SUCCES");
					dialog.dismiss();
					unlockScreenOrientation();
				}

				@Override
				public void postExecute(Boolean isSuccess, String result) {
					Log.i(TAG, "LOADING SUCCES");
					dialog.dismiss();
					unlockScreenOrientation();
				}
			});
			tesserTask.execute();
			assetsLoaded = true;
		}
	}

	protected static final String PHOTO_TAKEN = "photo_taken";

	public class ButtonClickHandler implements View.OnClickListener {
		public void onClick( View view ){
			startCameraActivity();
		}
	}

	protected void startCameraActivity() {
		File file = new File(imagePath);
		Uri outputFileUri = Uri.fromFile(file);

		Log.i(TAG, String.format("URI [%s] [%s]", file.getAbsolutePath(), file.getPath()));

		Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
		intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

		enableButtons(R.id.add_card, false);
		startActivityForResult(intent, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		Log.i(TAG, "resultCode: " + resultCode);
		switch(resultCode) {
		case 0:
			Log.i(TAG, "User cancelled");
			break;
		case -1:
			onPhotoTaken();
			break;
		}
	}

	private void enableButtons(Integer id, Boolean enable) {
		ImageButton b1 = (ImageButton) findViewById(id);
		b1.setVisibility(enable ? View.VISIBLE : View.GONE);
	}

	protected void onPhotoTaken() {
		taken = true;
		if(imagePath != null) {
			/* [START] - OCR */
			try {
				executeOCR(imagePath);
			} catch (Exception e) {
				Log.e(TAG, String.format("Error: [%s]", e.getMessage()));
				ActivityUtils.showToast(getApplicationContext(), "Error: " + e.getMessage());
			}
			/* [ END ] - OCR */
			enableButtons(R.id.add_card, false);
		}
	}

	private void showProgressDialog(String title, String message) {
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		if(!dialog.isShowing()) {
			lockScreenOrientation();
			dialog.show();
		}
	}

	private void executeOCR(String path) throws IOException {
		showProgressDialog("OCR engine", "Wait while processing image...");

		final TessBaseAPI baseApi = new TessBaseAPI();
		baseApi.init(tesseractPath, "eng");
		//baseApi.init(tesseractPath, "spa");

		OCREngine ocrEngine = new OCREngine(baseApi, new AsyncHandler() {

			@Override
			public void postExecute(Boolean isSuccess, Object result) {
				ocrResult = (OcrResult) result;
				Log.i(TAG, String.format("END OCR [%s] [%s]", isSuccess, ocrResult));
				if(ocrResult != null) {
					textView.setText(ocrResult.getText());
					imageView.setImageBitmap(ocrResult.getBitmap());
				}
				baseApi.end();
				dialog.dismiss();
				enableButtons(R.id.add_card, true);
				unlockScreenOrientation();
			}

			@Override
			public void postExecute(Boolean isSuccess, String result) {
				Log.i(TAG, String.format("END OCR [%s] [%s]", isSuccess, result));
				baseApi.end();
				dialog.dismiss();
			}
		});

		ocrEngine.execute(imagePath);
	}

	public void createCard(View view) {
		Log.i(TAG, "CreateCard");

		if(ocrResult == null) {
			ActivityUtils.showToast(getApplicationContext(), "OCR execution was wrong... Try again.");
		} else {
			Intent intent = new Intent(this, WizzardActivity.class);
			Log.i(TAG, "Changing to WizzardActivity");

			IntentModule.Factory.persist(module, intent);
			intent.putExtra(ActivityUtils.OCR_TEXT, ocrResult.getText());
			intent.putExtra(ActivityUtils.CARD_PARENT, currentFolder);
			startActivity(intent);
			this.finish();
		}
	}

	@Override
	protected void onSaveInstanceState( Bundle outState ) {
		Log.i(TAG, "onSaveInstanceState()");
		outState.putBoolean(PHOTO_TAKEN, taken);
		outState.putBoolean(ASSETS_LOAD, assetsLoaded);
		outState.putString(ACTUAL_PHOTO, imagePath);
	}
	@Override 
	protected void onRestoreInstanceState( Bundle savedInstanceState) {
		Log.i(TAG, "onRestoreInstanceState()");
		if(savedInstanceState.getBoolean(PHOTO_TAKEN)) {
			onPhotoTaken();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/* Lock screen orientation and avoid any change */
	private void lockScreenOrientation() {
		int currentOrientation = getResources().getConfiguration().orientation;
		if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
	}

	private void unlockScreenOrientation() {
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}
}
