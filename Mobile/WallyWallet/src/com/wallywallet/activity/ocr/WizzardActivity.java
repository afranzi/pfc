package com.wallywallet.activity.ocr;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Splitter;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.Maps;
import com.wallywallet.R;
import com.wallywallet.activity.card.CreateCardActivity;
import com.wallywallet.activity.content.RowCheckAdapter;
import com.wallywallet.models.Card;
import com.wallywallet.utils.ActivityUtils;
import com.wallywallet.utils.IntentModule;

public class WizzardActivity extends Activity {

	private final static String TAG = "WIZZARD";
	private IntentModule module;
	private String ocrText;
	private EditText editText;
	private TextView textView;
	private String currentParent;
	
	RowCheckAdapter<String> adapter = null;

	private Map<FieldType, String> fields = Maps.newHashMap();
	private Integer currentField = 0;

	private final static Splitter SPLITTER = Splitter.onPattern("(([\\n\\t\\r\\s]+[^A-Za-z0-9]{1,3}[\\n\\t\\r\\s]+)|([\\n\\t\\r\\s]))").omitEmptyStrings().trimResults();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizzard);

		Intent intent = getIntent();
		module = IntentModule.Factory.build(intent);
		ocrText = intent.getStringExtra(ActivityUtils.OCR_TEXT);
		currentParent = intent.getStringExtra(ActivityUtils.CARD_PARENT);		

		editText = (EditText) findViewById(R.id.field_value);
		textView = (TextView) findViewById(R.id.field_name);

		refreshList(SPLITTER.splitToList(ocrText));

		for(FieldType fieldType: FieldType.values()) {
			fields.put(fieldType, new String());
		}
		
		/* Set initial field to be set */
		FieldType field = fieldTypeByOrder.get(0);
		textView.setText(field.getName());
	}

	public void setNextField(Integer nextField) {	
		FieldType field = fieldTypeByOrder.get(currentField);
		fields.put(field, editText.getText().toString().trim());
		editText.setText("");

		nextField = Math.max(0, nextField);		
		if(nextField < fieldTypeByOrder.size()) {
			currentField = nextField;
			field = fieldTypeByOrder.get(nextField);
			
			Log.i(TAG, field.getValue());

			textView.setText(field.getName());
		} else if(nextField >= fieldTypeByOrder.size()) {
			//Finished
			Intent intent = new Intent(this, CreateCardActivity.class);
			Log.i(TAG, "Changing to CreateCardActivity");

			Card card = new Card();
			card.setCompany(fields.get(FieldType.COMPANY));
			card.setCountry(fields.get(FieldType.COUNTRY));			
			card.setCity(fields.get(FieldType.CITY));
			card.setStreet(fields.get(FieldType.STREET));
			card.setEmail(fields.get(FieldType.EMAIL));
			card.setPhone(fields.get(FieldType.PHONE));
			card.setName(fields.get(FieldType.GUY_NAME));
			card.setAppointment(fields.get(FieldType.GUY_ROLE));	
			card.setParent(currentParent);
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				ObjectNode jsonCard = mapper.valueToTree(card);
				intent.putExtra(ActivityUtils.CARD_PARENT, currentParent);
				intent.putExtra(ActivityUtils.CARD_JSON, jsonCard.toString());
				intent.putExtra(ActivityUtils.CARD_OWNER, module.getFacebookID());
				intent.putExtra(ActivityUtils.CARD_OCR, true);

				startActivity(intent);

				this.finish();
			} catch (Exception e) {
				throw Throwables.propagate(e);
			}
		}
	}

	public void previousField(View view) {
		setNextField(currentField - 1);
		editText.setText("");
		refreshList(SPLITTER.splitToList(ocrText));
	}

	public void cleanText(View view) {
		editText.setText("");
		refreshList(SPLITTER.splitToList(ocrText));
	}

	public void nextField(View view) {
		setNextField(currentField + 1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	private void refreshList(List<String> candidates) {		
		List<String> candidateClone = Lists.newCopyOnWriteArrayList(candidates);		
		
		// 1. pass context and data to the custom adapter
		if(adapter == null) {
			adapter = new RowCheckAdapter<String>(this, candidateClone);

			// 2. Get ListView from activity_main.xml
			final ListView listView = (ListView) findViewById(R.id.list);

			// 3. setListAdapter
			listView.setAdapter(adapter);

			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
					String item = (String) listView.getItemAtPosition(position);
					Log.i(TAG, String.format("Click item [%s] at position [%d]", item, position));

					editText.setText(editText.getText() + " " + item);
				}
			});
		} else {
			adapter.clear();			
			adapter.addItems(candidateClone);
		}

	}

	private enum FieldType {
		COMPANY(0, "Company", "company"),
		STREET(1, "Street", "address.street"),
		CITY(2, "City", "address.city"),
		COUNTRY(3, "Country", "address.country"),
		EMAIL(4, "Email", "email"),
		PHONE(5, "Phone", "phone"),
		GUY_NAME(6, "Guy name", "guy.name"),
		GUY_ROLE(7, "Guy role", "guy.appointment"),;

		String name;
		String value;
		Integer order;

		FieldType(Integer order, String name, String value) {
			this.name = name;
			this.value = value;
			this.order = order;
		}

		String getName() {
			return name;
		}

		String getValue() {
			return value;
		}

		Integer getOrder() {
			return order;
		}
	}

	private Map<Integer, FieldType> fieldTypeByOrder;
	{
		Builder<Integer, FieldType> builder = ImmutableMap.builder();
		for(FieldType fieldType: FieldType.values()) {
			builder.put(fieldType.getOrder(), fieldType);
		}
		fieldTypeByOrder = builder.build();
	}
}
