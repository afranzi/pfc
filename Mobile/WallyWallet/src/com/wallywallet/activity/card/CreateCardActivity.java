package com.wallywallet.activity.card;

import java.io.UnsupportedEncodingException;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.loopj.android.http.RequestParams;
import com.wallywallet.R;
import com.wallywallet.models.Card;
import com.wallywallet.network.CustomAsyncHttpResponseHandler;
import com.wallywallet.network.RestClient;
import com.wallywallet.utils.ActivityUtils;

public class CreateCardActivity extends Activity {

	private String TAG = "CREATE_CARD";

	private Card card = null;
	private String parentFolder = null;
	private String ownerID = null;
	private Boolean isCreate = false;
	private Boolean isOcr = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_card);

		Intent intent = getIntent();		
		parentFolder = intent.getStringExtra(ActivityUtils.CARD_PARENT);
		ownerID = intent.getStringExtra(ActivityUtils.CARD_OWNER);
		isCreate = intent.getBooleanExtra(ActivityUtils.CARD_CREATE, false);
		isOcr = intent.getBooleanExtra(ActivityUtils.CARD_OCR, false);

		String jsonCard = intent.getStringExtra(ActivityUtils.CARD_JSON);
		ObjectMapper mapper = new ObjectMapper();
		if(jsonCard != null && !jsonCard.isEmpty()) {
			try {
				card = mapper.readValue(jsonCard, Card.class);
				setValue(R.id.company, card.getCompany());
				setValue(R.id.street, card.getStreet());
				setValue(R.id.city, card.getCity());
				setValue(R.id.country, card.getCountry());
				setValue(R.id.phone, card.getPhone());
				setValue(R.id.email, card.getEmail());
				setValue(R.id.guy_name, card.getName());
				setValue(R.id.guy_role, card.getAppointment());
			} catch (Exception e) {
				throw Throwables.propagate(e);
			}
			Log.i(TAG, card.toString());
		}
	}

	private Boolean onProcess = false;

	public void saveCard(View view) {
		Boolean showErrorMessage = true;

		if(getValue(R.id.company).isEmpty()) {
			setValue(R.id.error_message, "Company required!");
		} else if (getValue(R.id.street).isEmpty()) {
			setValue(R.id.error_message, "Street required!");
		}else if (getValue(R.id.city).isEmpty()) {
			setValue(R.id.error_message, "City required!");
		}else if (getValue(R.id.country).isEmpty()) {
			setValue(R.id.error_message, "Country required!");
		}else if (getValue(R.id.phone).isEmpty()) {
			setValue(R.id.error_message, "Phone required!");
		}else if (getValue(R.id.email).isEmpty()) {
			setValue(R.id.error_message, "Email required!");
		}else if(!isValidEmail(getValue(R.id.email))) {
			setValue(R.id.error_message, "Wrong mail format!");
		} else {
			showErrorMessage = false;
		}

		TextView errorMessage = (TextView) findViewById(R.id.error_message);
		errorMessage.setVisibility(showErrorMessage ? View.VISIBLE : View.GONE);

		if(!showErrorMessage && !onProcess) {
			//Save card and go back			
			RequestParams params = new RequestParams();
			params.put("company", getValue(R.id.company));
			params.put("address.street", getValue(R.id.street));
			params.put("address.city", getValue(R.id.city));
			params.put("address.country", getValue(R.id.country));
			params.put("phone", getValue(R.id.phone));
			params.put("email", getValue(R.id.email));
			params.put("parent", parentFolder);
			params.put("owner", ownerID);
			params.put("guy.name", getValue(R.id.guy_name));
			params.put("guy.appointment", getValue(R.id.guy_role));

			final Activity activity = this;

			RestClient client = RestClient.Factory.build(getApplicationContext());

			String cardUrl = "cards";			
			if(!isCreate && !isOcr) {
				cardUrl += "/save";
				params.put("_id", card.getId());
			}

			client.post(cardUrl, params, new CustomAsyncHttpResponseHandler() {			

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
					super.onFailure(statusCode, headers, errorResponse, e);
					onProcess = false;
					String message = "Unknow";
					try {
						message = new String(errorResponse, "UTF-8");
					} catch (Exception e2) {}
					showAlert(R.string.error, message);
				}

				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] response) {
					Boolean saveSuccess = false;
					try {
						String message = new String(response, "UTF-8");
						Log.i(TAG, message);

						if(statusCode == 200) {
							saveSuccess = true;
						} else {
							showAlert(R.string.error, message);
						}
					} catch (UnsupportedEncodingException e) {
						Log.i(TAG, "Error on convert Response");
					}

					onProcess = false;
					if(saveSuccess) {
						Toast.makeText(activity.getApplicationContext(), "Card saved correctly", Toast.LENGTH_SHORT).show();
						activity.finish();
					}
				}
			});
		}
	}

	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) return false;
		return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	public String getValue(Integer id) {
		TextView editText = (TextView) findViewById(id);
		return editText.getText().toString();
	}

	public void setValue(Integer id, String value) {
		TextView editText = (TextView) findViewById(id);
		editText.setText(value);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return true;
	}

	private void showAlert(Integer titleID, String message) {
		showAlert(getResources().getString(titleID), message);
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.ok, null)
		.show();
	}
}
