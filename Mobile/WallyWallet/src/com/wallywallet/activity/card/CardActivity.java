package com.wallywallet.activity.card;

import java.io.UnsupportedEncodingException;

import org.apache.http.Header;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Throwables;
import com.wallywallet.R;
import com.wallywallet.models.Card;
import com.wallywallet.network.CustomAsyncHttpResponseHandler;
import com.wallywallet.network.RestApi;
import com.wallywallet.network.RestApiHandler;
import com.wallywallet.network.RestClient;
import com.wallywallet.utils.ActivityUtils;

public class CardActivity extends Activity {

	private String TAG = "CARD";
	private String currentUser = null;
	private Card card = null;
	private Menu menu = null;
	private RestApi restApi = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_card);

		// Get the message from the intent
		Intent intent = getIntent();
		currentUser = intent.getStringExtra(ActivityUtils.FACEBOOK_ID);
		String jsonCard = intent.getStringExtra(ActivityUtils.CARD_JSON);
		ObjectMapper mapper = new ObjectMapper();

		restApi = new RestApi(getApplicationContext());

		try {
			card = mapper.readValue(jsonCard, Card.class);

			setValue(R.id.company, card.getCompany());
			setValue(R.id.street, card.getStreet());
			setValue(R.id.city, card.getCity());
			setValue(R.id.country, card.getCountry());

			setValue(R.id.phone, card.getPhone());
			setValue(R.id.email, card.getEmail());
			setValue(R.id.guy_name, card.getName());
			setValue(R.id.guy_role, card.getAppointment());

		} catch (Exception e) {
			throw Throwables.propagate(e);
		}

		Log.i(TAG, card.toString());
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public void setValue(Integer id, String value) {
		TextView textView = (TextView) findViewById(id);
		textView.setText(value);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.card, menu);
		this.menu = menu;

		activateMenuButtons();
		return true;
	}

	private void activateMenuButtons() {
		if(menu != null && card != null) {
			Boolean isUs = card.getOwner().equals(currentUser);
			enableMenuButton(R.id.action_edit_card, isUs);
			enableMenuButton(R.id.action_remove_card, isUs);
			enableMenuButton(R.id.action_import_card, !isUs);
		}
	}

	private void enableMenuButton(Integer id, Boolean enable) {
		menu.findItem(id).setEnabled(enable);
		menu.findItem(id).setVisible(enable);
	}

	public void removeCard() {
		final Activity activity = this;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Confirm Delete...");
		builder.setMessage("Are you sure you want to delete this card?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {			

				RestClient client = RestClient.Factory.build(getApplicationContext());			
				client.post("cards/delete/" + card.getId(), null, new CustomAsyncHttpResponseHandler() {			

					@Override
					public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
						super.onFailure(statusCode, headers, errorResponse, e);
						String message = "Unknow";
						try {
							message = new String(errorResponse, "UTF-8");
						} catch (Exception e2) {}
						showAlert(R.string.error, message);
					}

					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] response) {
						Boolean saveSuccess = false;
						try {
							String message = new String(response, "UTF-8");
							Log.i(TAG, message);

							if(statusCode == 200) {
								saveSuccess = true;
							} else {
								showAlert(R.string.error, message);
							}
						} catch (UnsupportedEncodingException e) {
							Log.i(TAG, "Error on convert Response");
						}

						if(saveSuccess) {
							Toast.makeText(activity.getApplicationContext(), "Card deleted correctly", Toast.LENGTH_SHORT).show();
							activity.finish();
						}
					}
				});
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id == android.R.id.home) onBackPressed();
		else if (id == R.id.action_remove_card) {
			removeCard();
			return true;
		} else if(id == R.id.action_edit_card) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				ObjectNode jsonCard = mapper.valueToTree(card);
				Intent intent = new Intent(this, CreateCardActivity.class);
				Log.i(TAG, "Changing to CreateCardActivity");
				intent.putExtra(ActivityUtils.CARD_JSON, jsonCard.toString());
				intent.putExtra(ActivityUtils.CARD_PARENT, card.getParent());
				intent.putExtra(ActivityUtils.CARD_OWNER, card.getOwner());
				intent.putExtra(ActivityUtils.CARD_CREATE, false);

				startActivity(intent);

				this.finish();
			} catch (Exception e) {
				throw Throwables.propagate(e);
			}
		} else if(id == R.id.action_import_card){
			restApi.importCard(card.getId(), card.getOwner(), new RestApiHandler() {
				@Override
				public void onFinishCall(Boolean isSuccess, String message) {
					if(isSuccess) {
						ActivityUtils.showToast(getApplicationContext(), "Card imported correctly.");
					} else {
						ActivityUtils.showToast(getApplicationContext(), message);
					}
				}
			});
		}
		return super.onOptionsItemSelected(item);
	}

	private void showAlert(Integer titleID, String message) {
		showAlert(getResources().getString(titleID), message);
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.ok, null)
		.show();
	}
}
