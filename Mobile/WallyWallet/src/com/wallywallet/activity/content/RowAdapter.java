package com.wallywallet.activity.content;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wallywallet.R;
import com.wallywallet.models.AbstractItem;
import com.wallywallet.models.Item.Type;

public class RowAdapter <T extends AbstractItem> extends ArrayAdapter<T> {
 
        private final Activity context;
        private final List<T> itemsArrayList;
        
        static class ViewHolder { //Holder container
        	public TextView company;
        	public TextView name;
        	public ImageView icon;
        }
 
        public RowAdapter(Activity context, List<T> cards) { 
            super(context, R.layout.row, cards);
 
            this.context = context;
            this.itemsArrayList = cards;
        }
        
        public void addItem(T item) {
        	itemsArrayList.add(item);
        	this.notifyDataSetChanged();
        }
        
        public void addItems(List<T> items) {
        	itemsArrayList.addAll(items);
        	this.notifyDataSetChanged();
        }
        
        public void clear() {
        	itemsArrayList.clear();
        	this.notifyDataSetChanged();
        }
 
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        	View rowView = convertView;
        	//reuse views ~15% faster -> Holder Pattern
            if (rowView == null) {
              LayoutInflater inflater = context.getLayoutInflater();
              rowView = inflater.inflate(R.layout.row, null);
              // configure view holder
              ViewHolder viewHolder = new ViewHolder();
              viewHolder.company = (TextView) rowView.findViewById(R.id.company);
              viewHolder.name = (TextView) rowView.findViewById(R.id.name);
              viewHolder.icon = (ImageView) rowView.findViewById(R.id.icon_row);
              
              rowView.setTag(viewHolder);
            }

            // fill data
            ViewHolder holder = (ViewHolder) rowView.getTag();
            holder.name.setText(itemsArrayList.get(position).getValue());
            holder.company.setText(itemsArrayList.get(position).getTitle());
    
            Type itemtype = itemsArrayList.get(position).getType();
            if(Type.FOLDER.equals(itemtype)) {
                holder.icon.setImageResource(R.drawable.folder_open);
            } else if (Type.CARD.equals(itemtype)){
                holder.icon.setImageResource(R.drawable.folder_icon);
            } else {
            	holder.icon.setImageResource(R.drawable.user_icon);
            }
            
            return rowView;
        }
}