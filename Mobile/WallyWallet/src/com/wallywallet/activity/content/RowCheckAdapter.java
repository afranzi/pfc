package com.wallywallet.activity.content;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wallywallet.R;

public class RowCheckAdapter <T> extends ArrayAdapter<T> {

	private final Activity context;
	private final List<T> itemsArrayList;

	static class ViewHolder { //Holder container
		public TextView check;
	}

	public RowCheckAdapter(Activity context, List<T> items) { 
		super(context, R.layout.row_check, items);

		this.context = context;
		this.itemsArrayList = items;
	}

	public void addItem(T item) {
		itemsArrayList.add(item);
		this.notifyDataSetChanged();
	}

	public void addItems(List<T> items) {
		itemsArrayList.addAll(items);
		this.notifyDataSetChanged();
	}

	public void clear() {
		itemsArrayList.clear();
		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		//reuse views ~15% faster -> Holder Pattern
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.row_check, null);
			// configure view holder
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.check = (TextView) rowView.findViewById(R.id.candidate);

			rowView.setTag(viewHolder);
		}

		// fill data
		ViewHolder holder = (ViewHolder) rowView.getTag();
		holder.check.setText(itemsArrayList.get(position).toString());            
		return rowView;
	}
}