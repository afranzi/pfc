package com.wallywallet.activity.content;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

import java.util.Iterator;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.wallywallet.R;
import com.wallywallet.activity.card.CardActivity;
import com.wallywallet.activity.card.CreateCardActivity;
import com.wallywallet.activity.ocr.CaptureActivity;
import com.wallywallet.activity.search.SearchActivity;
import com.wallywallet.models.AbstractItem;
import com.wallywallet.models.Card;
import com.wallywallet.models.Folder;
import com.wallywallet.models.Item.Type;
import com.wallywallet.models.beans.CardRealm;
import com.wallywallet.models.beans.FolderRealm;
import com.wallywallet.network.RestApi;
import com.wallywallet.network.RestApiHandler;
import com.wallywallet.network.RestClient;
import com.wallywallet.utils.ActivityUtils;
import com.wallywallet.utils.IntentModule;

public class ContentActivity extends Activity {

	private String TAG = "CONTENT";
	private IntentModule module;

	//Info about user that has been showed
	private String showedUserName = null;
	private String showedUserID = null;

	//Info about where are we navigating
	private String currentFolder = null;
	private String parentFolder = null;
	private String folderName = null;

	private Integer cardCount = 0;
	private Integer folderCount = 0;

	RowAdapter<AbstractItem> adapter = null;
	private Boolean flagLoading = false;

	private Integer pages = 0;
	private Boolean moreCards = true;
	private Boolean moreFolders = true;
	private Boolean isOnline = false;
	private Boolean realmLoad = false;

	private RestApi restApi;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		// Get the message from the intent
		Intent intent = getIntent();
		module = IntentModule.Factory.build(intent);
		isOnline = module.isOnline();

		showedUserID = intent.getStringExtra(ActivityUtils.CURRENT_USER_ID);
		showedUserName = intent.getStringExtra(ActivityUtils.CURRENT_USER_NAME);

		restApi = new RestApi(getApplicationContext());

		setContentHeader();
		
	    ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		refresh();
	}

	public void refreshContent(String userID) {
		moreCards = true;
		moreFolders = true;
		pages = 0;
		refreshRealm(Type.CARD);
		refreshRealm(Type.FOLDER);
		refreshContent(userID, 0);
	}

	public void refreshContent(String userID, Integer page) {
		Button parentButton = (Button) findViewById(R.id.parent_folder);
		parentButton.setVisibility((currentFolder == null || currentFolder.isEmpty()) ? View.GONE : View.VISIBLE);

		if(isOnline) {
			if(moreCards) getItems(Type.CARD, userID, page);
			if(moreFolders) getItems(Type.FOLDER, userID, page);
		} else if(!realmLoad){
			cardCount = getItemsFromRealm(Type.CARD);
			folderCount = getItemsFromRealm(Type.FOLDER);
			realmLoad = true;
		}
	}

	private Integer getItemsFromRealm(Type type) {
		// Obtain a Realm instance
		Realm realm = Realm.getInstance(this, type.name() + ".realm");

		String parentSearch = (currentFolder == null) ? "" : currentFolder;
		RealmResults<? extends RealmObject> result = realm.where(type.getRealmClass())
				.equalTo("parent", parentSearch)
				.findAll();

		List<AbstractItem> itemList = Lists.newArrayList();
		for(RealmObject card: result) {
			if(Type.CARD.equals(type)) {
				itemList.add(new Card().copy((CardRealm)card));
			}else {
				itemList.add(new Folder().copy((FolderRealm)card));
			}
		}

		refreshList(itemList, type);

		return itemList.size();
	}

	private void getItems(final Type type, String userId, final Integer page) {
		restApi.getItems(currentFolder, type, userId, page, new RestApiHandler() {

			@Override
			public void onFinishCall(List<AbstractItem> itemsList, Boolean moreResults) {
				if(!moreResults) { //Not found more results to get from server
					if(Type.CARD.equals(type)) {
						moreCards = false;
					} else {
						moreFolders = false;
					}
				}
				refreshList(itemsList, type);
				flagLoading = false;
			}
		});		
	}

	private void refreshRealm(Type type) {
		if(pages == 0 && isOnline) {
			//Realm.deleteRealmFile(this, type.name() + ".realm");
			Realm realm = Realm.getInstance(this, type.name() + ".realm");		
			realm.beginTransaction();
			String parentSearch = (currentFolder == null) ? "" : currentFolder;

			Log.i(TAG, String.format("Child to remove with parent [%s]", parentSearch));
			RealmResults<? extends RealmObject> toRemove = realm.where(type.getRealmClass()).equalTo("parent", parentSearch).findAll();
			Log.i(TAG, String.format("[%s] To remove [%d] elements", type.name(), toRemove.size()));
			Iterator<? extends RealmObject> it = toRemove.iterator();
			while(it.hasNext()) {
				it.next();
				it.remove();
			}

			RealmResults<? extends RealmObject> toRemove2 = realm.where(type.getRealmClass()).equalTo("parent", parentSearch).findAll();
			Log.i(TAG, String.format("[%s] To remove [%d] elements", type.name(), toRemove.size()));
			realm.commitTransaction();
		}
	}

	private void refreshList(List<AbstractItem> cards, Type type) {		
		//Realm.deleteRealmFile(this, type.name() + ".realm");
		Realm realm = Realm.getInstance(this, type.name() + ".realm");		
		realm.beginTransaction();

		/* Save items in realm to be accessible in offline mode */
		if(Type.CARD.equals(type) && isOnline) {
			for(AbstractItem item: cards) {
				Card card = (Card) item;
				CardRealm itemRealm = realm.createObject(CardRealm.class);
				itemRealm = card.clone(itemRealm);
			}
		} else if(Type.FOLDER.equals(type) && isOnline) {
			for(AbstractItem item: cards) {
				Folder folder = (Folder) item;
				FolderRealm itemRealm = realm.createObject(FolderRealm.class);
				itemRealm = folder.clone(itemRealm);
			}
		}

		realm.commitTransaction();
		
		// 0. check that items received are from current folder
		if(cards.size() > 0 && isOnline) {
			if(currentFolder == null && cards.get(0).getParent() == null) {
				//Items from root folder - All ok
			}else if((currentFolder == null && cards.get(0).getParent() != null) || !currentFolder.equals(cards.get(0).getParent())) {
				return;//Received items from old current folder - User does not have patience...
			}
		}

		// 1. pass context and data to the custom adapter
		if(adapter == null) {
			adapter = new RowAdapter<AbstractItem>(this, cards);

			// 2. Get ListView from activity_main.xml
			final ListView listView = (ListView) findViewById(R.id.list);
			final Activity activity = this;

			// 3. setListAdapter
			listView.setAdapter(adapter);

			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
					AbstractItem item = (AbstractItem) listView.getItemAtPosition(position);
					Log.i(TAG, String.format("Click item [%s] at position [%d]", item, position));

					if(item.getType().equals(Type.FOLDER)) {
						parentFolder = currentFolder;
						currentFolder = item.getId();

						refresh();
					} else {
						ObjectMapper mapper = new ObjectMapper();
						try {
							ObjectNode jsonCard = mapper.valueToTree(item);
							Intent intent = new Intent(activity, CardActivity.class);
							Log.i(TAG, "Changing to CardActivity");

							intent.putExtra(ActivityUtils.FACEBOOK_ID, module.getFacebookID());
							intent.putExtra(ActivityUtils.CARD_JSON, jsonCard.toString());
							startActivity(intent);
						} catch (Exception e) {
							throw Throwables.propagate(e);
						}
					}
				}
			});

			// 4. set listenes on scroll to load in lazy mode
			listView.setOnScrollListener(new OnScrollListener() {

				public void onScrollStateChanged(AbsListView view, int scrollState) {}

				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
					if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0) {
						if(!flagLoading && (moreCards || moreFolders)) {
							Log.i(TAG, String.format("First[%d] - Visible[%d] - Total[%d]", firstVisibleItem, visibleItemCount, totalItemCount));
							flagLoading = true;
							++pages;
							refreshContent(getUserCandidate(), pages);
						}
					}
				}
			});
		} else {
			adapter.addItems(cards);
		}		
	}

	public void goToParent(View view) {
		currentFolder = parentFolder;
		refresh();
	}

	private void refresh() {
		realmLoad = false;

		if(adapter != null) adapter.clear();
		String userCandidate = getUserCandidate();

		if(isOnline) getCurrentFolder(userCandidate);
		else getCurrentRealmFolder();
		refreshContent(userCandidate);
		activateMenuButtons();
	}

	private void setContentHeader() {
		TextView currentUserView = (TextView) findViewById(R.id.current_user);
		String header = "";

		if(showedUserID != null && showedUserID != module.getFacebookID()) {
			header = String.format("User: %s  |  Folder: %s", showedUserName, (folderName == null) ? "Root" : folderName);
		} else {
			header = String.format("Folder: %s", (folderName == null) ? "Root" : folderName);
		}		
		if(!isOnline) {
			header += " (Offline)";
		}

		currentUserView.setText(header);
	}

	private void activateMenuButtons() {
		if(menu != null){
			Boolean isCurrentFolder = (currentFolder != null);
			Boolean isUs = module.getFacebookID().equals(showedUserID) || showedUserID == null;

			enableMenuButton(R.id.action_edit_folder, isUs && isCurrentFolder && isOnline);
			enableMenuButton(R.id.action_remove_folder, isUs && isCurrentFolder && isOnline);
			enableMenuButton(R.id.action_folder, isUs && isOnline);
			enableMenuButton(R.id.action_card, isUs && isOnline);
			enableMenuButton(R.id.action_ocr, isUs && isOnline);
			enableMenuButton(R.id.action_back, !isUs);
			enableMenuButton(R.id.action_search, isOnline);
		}
	}

	private void enableMenuButton(Integer id, Boolean enable) {
		menu.findItem(id).setEnabled(enable);
		menu.findItem(id).setVisible(enable);
	}

	private String getUserCandidate() {
		return (showedUserID == null) ? module.getFacebookID() : showedUserID;
	}

	@Override
	public void onBackPressed() {
		if(currentFolder != null){
			goToParent(null);
		} else if(showedUserID != null && showedUserID != module.getFacebookID()){
			super.onBackPressed();
		} else if(!isOnline) {
			super.onBackPressed();
		} else {
			ActivityUtils.showToast(getApplicationContext(), "Already in root folder");
		}
	}

	private Menu menu = null;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		this.menu = menu;
		activateMenuButtons();
		return super.onCreateOptionsMenu(menu);
	}

	public void getCurrentRealmFolder() {	
		// Obtain a Realm instance
		Realm realm = Realm.getInstance(this, Type.FOLDER.name() + ".realm");

		String folderToSearch = (currentFolder == null) ? "" : currentFolder;
		FolderRealm folder = realm.where(FolderRealm.class)
				.equalTo("id", folderToSearch)
				.findFirst();	

		Log.i(TAG, folderToSearch);
		if(folder != null) {
			Log.i(TAG, folder.toString());

			//Card & Folder Counter
			cardCount = folder.getCardsCounts(); //Set countes value, as default size return by getRealmItems.
			folderCount = folder.getFoldersCounts();

			//Set new parentFolder
			parentFolder = folder.getParent();
			folderName = folder.getName();
		} else {
			parentFolder = null;
			folderName = "Root";
		}
		
		//Set info in content view about how many cards and folders we can access in current folder
		TextView cardCountView = (TextView) findViewById(R.id.cardsCount);
		cardCountView.setText(cardCount + "");

		TextView folderCountView = (TextView) findViewById(R.id.folderCount);
		folderCountView.setText(folderCount + "");

		setContentHeader();
	}


	public void getCurrentFolder(String userId) {	
		String getUrl = null;

		if(currentFolder != null) {
			getUrl =  String.format("folder/%s/%s", userId, currentFolder);
		} else getUrl =  String.format("rootinfo/%s", userId);	

		Log.i(TAG, String.format("GET [%s]", getUrl));		
		RestClient client = RestClient.Factory.build(getApplicationContext());
		client.get(getUrl, null, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				try {
					//Card & Folder Counter
					cardCount = response.getInt("cardsCount");
					folderCount = response.getInt("foldersCount");

					//Set info in content view about how many cards and folders we can access in current folder
					TextView cardCountView = (TextView) findViewById(R.id.cardsCount);
					cardCountView.setText(cardCount + "");

					TextView folderCountView = (TextView) findViewById(R.id.folderCount);
					folderCountView.setText(folderCount + "");

					//Set new parentFolder
					parentFolder = response.has("parent") ? response.getString("parent") : null;
					folderName = response.has("name") ? response.getString("name") : null;
					if(parentFolder != null && parentFolder.equals("null")) parentFolder = null;
					Log.i(TAG, String.format("Info [%d]:[%d]", cardCount, folderCount));	

					setContentHeader();
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray cards) {
				ObjectMapper mapper = new ObjectMapper();
				List<Folder> cardsList = Lists.newArrayList();
				try {
					for(int i = 0; i < cards.length(); ++i) {
						JSONObject jsonCard = cards.getJSONObject(i);

						Folder folder = mapper.readValue(jsonCard.toString(), Folder.class);     
						cardsList.add(folder);
						Log.i(TAG, String.format("Folder [%s]", folder));	

						setContentHeader();
					}					
				} catch(Exception e) {
					System.out.println(e.getStackTrace());
					throw Throwables.propagate(e);
				} finally {

				}
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		Intent intent;
		switch(id) {
		case android.R.id.home:
	        onBackPressed();
	        break;
		case R.id.action_card:
			Log.i(TAG, "action_cadr");
			intent = new Intent(this, CreateCardActivity.class);
			intent.putExtra(ActivityUtils.CARD_PARENT, currentFolder);
			intent.putExtra(ActivityUtils.CARD_OWNER, module.getFacebookID());
			intent.putExtra(ActivityUtils.CARD_CREATE, true);
			Log.i(TAG, "Changing to CreateCardActivity");
			startActivity(intent);
			break;
		case R.id.action_folder:
			Log.i(TAG, "action_folder");
			createFolder(null);
			break;
		case R.id.action_search:
			Log.i(TAG, "action_search");
			intent = new Intent(this, SearchActivity.class);
			IntentModule.Factory.persist(module, intent);

			Log.i(TAG, "Changing to SearchActivity");
			startActivity(intent);
			break;
		case R.id.action_settings:
			Log.i(TAG, "action_settings");
			break;
		case R.id.action_remove_folder:			
			if(folderCount > 0) {
				showAlert(R.string.error, "Cannot remove folder with subfolders!");
			} else if(currentFolder == null) {
				showAlert(R.string.error, "Cannot remove root folder!");
			} else {
				removeFolder();
			}
			break;
		case R.id.action_edit_folder:
			Log.i(TAG, "action_edit_folder");
			createFolder(folderName);
			break;
		case R.id.action_back:
			Log.i(TAG, "action_back");
			this.finish();
			break;
		case R.id.action_ocr:
			Log.i(TAG, "action_ocr");
			intent = new Intent(this, CaptureActivity.class);
			IntentModule.Factory.persist(module, intent);
			intent.putExtra(ActivityUtils.CARD_PARENT, currentFolder);

			Log.i(TAG, "Changing to CaptureActivity");
			startActivity(intent);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	public void createFolder(String currentName) {
		final Activity activity = this;
		final boolean isEdit = currentName != null;

		// Use an EditText view to get user input.
		final EditText input = new EditText(this);

		if(isEdit) {
			input.setText(currentName);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(isEdit ? "Edit folder name" : "Create new folder");
		builder.setMessage("Please introduce folder name:");
		builder.setIcon(R.drawable.folder_open);
		builder.setView(input);
		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				Log.d(TAG, "User name: " + value);

				if(value.isEmpty()) {
					Toast.makeText(activity.getApplicationContext(), "Folder with non name...", Toast.LENGTH_SHORT).show();
					return;
				}

				String folderID = null;
				String parentID = null;
				if(isEdit) {
					folderID = currentFolder;
					parentID = parentFolder;
				} else {
					parentID = currentFolder;
				}				

				restApi.createFolder(module.getFacebookID(), value, folderID, parentID, isEdit, new RestApiHandler() {

					@Override
					public void onFinishCall(Boolean success, String message) {
						if(success) {
							ActivityUtils.showToast(activity.getApplicationContext(), "Folder saved correctly", Toast.LENGTH_SHORT);
							refresh();
						} else {
							showAlert(R.string.error, message);
						}
					}					
				});
				return;
			}
		});

		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				return;
			}
		});

		AlertDialog alert = builder.create();
		alert.show();
	}

	public void removeFolder() {
		final Activity activity = this;

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Confirm Delete...");
		builder.setMessage("Are you sure you want to delete this folder?")
		.setCancelable(false)
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {	
				restApi.deleteFolder(currentFolder, new RestApiHandler() {

					@Override
					public void onFinishCall(Boolean success, String message) {
						if(success) {
							ActivityUtils.showToast(activity.getApplicationContext(), "Folder deleted correctly", Toast.LENGTH_SHORT);
							goToParent(null);
						} else {
							showAlert(R.string.error, message);
						}
					}
				});
			}
		})
		.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void showAlert(Integer titleID, String message) {
		showAlert(getResources().getString(titleID), message);
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.ok, null)
		.show();
	}
}
