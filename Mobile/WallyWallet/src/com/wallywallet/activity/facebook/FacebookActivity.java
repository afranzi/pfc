package com.wallywallet.activity.facebook;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.Header;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AppEventsLogger;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.google.common.collect.ImmutableList;
import com.loopj.android.http.RequestParams;
import com.wallywallet.R;
import com.wallywallet.activity.content.ContentActivity;
import com.wallywallet.network.CustomAsyncHttpResponseHandler;
import com.wallywallet.network.RestClient;
import com.wallywallet.utils.IntentModule;

public class FacebookActivity extends FragmentActivity {

	private static final List<String> PERMISSIONS = ImmutableList.of("email", "public_profile", "user_friends");
	private static final String TAG = "LOGIN"; //Debug purpose

	private LoginButton loginButton;
	private ViewGroup controlsContainer;
	private GraphUser graphUser;
	private UiLifecycleHelper uiHelper;
	private Session session;

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
		@Override
		public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
			showAlert("Error", error.toString());
		}

		@Override
		public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
			Log.d("HelloFacebook", "Success!");
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);

		setContentView(R.layout.facebook_login);

		loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setReadPermissions(PERMISSIONS);
		loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
			@Override
			public void onUserInfoFetched(GraphUser user) {
				if(user != null) {
					graphUser = user;	
					if(session != null) goToContent();
				}
			}
		});

		controlsContainer = (ViewGroup) findViewById(R.id.main_ui_container);

		final FragmentManager fm = getSupportFragmentManager();

		// Listen for changes in the back stack so we know if a fragment got popped off because the user
		// clicked the back button.
		fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {
				if (fm.getBackStackEntryCount() == 0) {
					// We need to re-show our UI.
					controlsContainer.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		AppEventsLogger.activateApp(this);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
		AppEventsLogger.deactivateApp(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	private Boolean onProcess = false;

	private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
		Log.i(TAG, String.format("State [%s]", state.name()));  
		this.session = session;
		
		if(state.isOpened()) {
			if(graphUser != null) {
				goToContent();
			}
		} else if(SessionState.CLOSED_LOGIN_FAILED.equals(state)) {
			goToContentOffLine("Cannot connect with facebook. \nOffline mode only permit read cards.");
		}
	}
	
	public void goToContent() {
		RequestParams params = new RequestParams();
		params.put("facebookID", graphUser.getId());
		params.put("token", session.getAccessToken());
		params.put("user", graphUser.getUsername());

		if(!onProcess) {
			onProcess = true;
			final Activity activity = this;
			final String token = session.getAccessToken();

			final RestClient client = RestClient.Factory.build(getApplicationContext());
			client.post("login", params, new CustomAsyncHttpResponseHandler() {			

				@Override
				public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
					super.onFailure(statusCode, headers, errorResponse, e);
					onProcess = false;
					String message = "Unknow";
					try {
						message = new String(errorResponse, "UTF-8");
					} catch (Exception e2) {}	

					goToContentOffLine(message);
				}

				@Override
				public void onSuccess(int statusCode, Header[] headers, byte[] response) {
					Boolean startContent = false;
					try {
						String message = new String(response, "UTF-8");
						Log.i(TAG, message);

						if(message.contains("OK")) {
							startContent = true;
						} else {
							showAlert(R.string.error, R.string.error_login);
						}
					} catch (UnsupportedEncodingException e) {
						Log.i(TAG, "Error on convert Response");
					}
					
					onProcess = false;
					if(startContent) {
						Intent intent = new Intent(activity, ContentActivity.class);
						Log.i(TAG, "Changing to ContentActivity");
						IntentModule.Factory.persist(graphUser, token, session, true, intent);
						
						startActivity(intent);
					}
				}
			});
		}
	}
	
	private void goToContentOffLine(String error) {
		final Activity activity = this;
		
		new AlertDialog.Builder(this)
		.setTitle(getResources().getString(R.string.error))
		.setMessage(error)
		.setPositiveButton(R.string.ok, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(activity, ContentActivity.class);
				Log.i(TAG, "Changing to ContentActivity");
				IntentModule.Factory.persist(graphUser, null, session, false, intent);
				
				startActivity(intent);
			}
		})
		.setNegativeButton(R.string.cancel, null)
		.show();
	}

	private void showAlert(Integer titleID, Integer messageID) {
		showAlert(getResources().getString(titleID), getResources().getString(messageID));
	}

	private void showAlert(Integer titleID, String message) {
		showAlert(getResources().getString(titleID), message);
	}

	private void showAlert(String title, String message) {
		new AlertDialog.Builder(this)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.ok, null)
		.show();
	}
}
