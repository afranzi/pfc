package com.wallywallet.network;

import java.util.List;
import android.os.Handler;
import com.wallywallet.models.AbstractItem;

public abstract class RestApiHandler extends Handler {

	public void onFinishCall(List<AbstractItem> itemsList, Boolean moreResults){}
	public void onFinishCall(Boolean isSuccess, String message) {}
	
}	