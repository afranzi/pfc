package com.wallywallet.network;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.google.common.collect.Lists;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.wallywallet.models.AbstractItem;
import com.wallywallet.models.Item.Type;


public class RestApi {

	private static Integer ITEMS_PAGE = 20;
	private static String TAG = "REST_API";

	private final RestClient client;
	private final Context context;

	public RestApi(Context context) {
		client = RestClient.Factory.build(context);
		this.context = context;
	}

	public void getItems(String folder, final Type type, String userId, final Integer page, final RestApiHandler handler) {
		String getItemsUrl = String.format("%s/%s/%d/%d/", type.getUserBase(), userId, page, ITEMS_PAGE);
		if(folder != null) getItemsUrl += folder;

		Log.i(TAG, String.format("GET [%s]", getItemsUrl));		
		client.get(getItemsUrl, null, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject response) {} //Not used

			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray items) {
				ObjectMapper mapper = new ObjectMapper();
				List<AbstractItem> itemsList = Lists.newArrayList();

				try {
					for(int i = 0; i < items.length(); ++i) {
						JSONObject jsonCard = items.getJSONObject(i);
						AbstractItem item = (AbstractItem) mapper.readValue(jsonCard.toString(), type.getTypeClass());     
						itemsList.add(item);
					}	
					handler.onFinishCall(itemsList, itemsList.size() != 0);

				} catch(Exception e) {
					System.out.println(e.getStackTrace());
					throw Throwables.propagate(e);
				} 
			}
		});
	}
	
	public void importCard(String cardID, String ownerID, final RestApiHandler handler) {
		String url = String.format("card/import/%s/%s", cardID, ownerID);
		client.post(url, null, new CustomAsyncHttpResponseHandler() {			

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
				super.onFailure(statusCode, headers, errorResponse, e);
				String message = "Unknow";
				try {
					message = new String(errorResponse, "UTF-8");
				} catch (Exception e2) {}
				finally {
					handler.onFinishCall(false, message);
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				Boolean saveSuccess = false;
				String message = "Unknow";
				try {
					message = new String(response, "UTF-8");
					Log.i(TAG, message);

					if(statusCode == 200) {
						saveSuccess = true;
					}
				} catch (UnsupportedEncodingException e) {
					Log.i(TAG, "Error on convert Response");
				} finally {
					handler.onFinishCall(saveSuccess, message);
				}
			}
		});
		return;
	}
	

	public void getRealmItems(Type type) {
		// Obtain a Realm instance
		Realm realm = Realm.getInstance(context, type.name());

		// Pull all the cities from the realm
		RealmResults<RealmObject> items = realm.where(RealmObject.class).findAll();

		for(RealmObject item: items) {
			Log.i(TAG, item.toString());
		}
	}

	public void deleteFolder(String folder, final RestApiHandler handler) {		
		client.post("folders/delete/" + folder, null, new CustomAsyncHttpResponseHandler() {			

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
				super.onFailure(statusCode, headers, errorResponse, e);
				String message = "Unknow";
				try {
					message = new String(errorResponse, "UTF-8");
				} catch (Exception e2) {}
				finally {
					handler.onFinishCall(false, message);
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				Boolean saveSuccess = false;
				String message = "Unknow";
				try {
					message = new String(response, "UTF-8");
					Log.i(TAG, message);

					if(statusCode == 200) {
						saveSuccess = true;
					}
				} catch (UnsupportedEncodingException e) {
					Log.i(TAG, "Error on convert Response");
				} finally {
					handler.onFinishCall(saveSuccess, message);
				}
			}
		});
	}

	public void createFolder(String owner, String folderName, String folderID, String parentFolder, Boolean isEdit, final RestApiHandler handler) {

		RequestParams params = new RequestParams();
		params.put("name", folderName);
		if(isEdit) {
			params.put("parent", parentFolder);
			params.put("_id", folderID);
		} else {
			params.put("parent", parentFolder);
		}
		params.put("owner", owner);

		String urlFolder = "folders";
		if(isEdit) urlFolder += "/save";
		client.post(urlFolder, params, new CustomAsyncHttpResponseHandler() {			

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
				super.onFailure(statusCode, headers, errorResponse, e);
				String message = "Unknow";
				try {
					message = new String(errorResponse, "UTF-8");
				} catch (Exception e2) {}
				finally {
					handler.onFinishCall(false, message);
				}
			}

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				Boolean saveSuccess = false;
				String message = "Unknow";
				try {
					message = new String(response, "UTF-8");
					Log.i(TAG, message);

					if(statusCode == 200) {
						saveSuccess = true;
					}
				} catch (UnsupportedEncodingException e) {
					Log.i(TAG, "Error on convert Response");
				} finally {
					handler.onFinishCall(saveSuccess, message);
				}
			}
		});
		return;

	}




}