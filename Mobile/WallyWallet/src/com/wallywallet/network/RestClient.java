package com.wallywallet.network;

import org.apache.http.impl.cookie.BasicClientCookie;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

public class RestClient {
	private static final String BASE_URL = "http://www.wallywallet.net/";

	private AsyncHttpClient client = new AsyncHttpClient();
	private PersistentCookieStore cookieStore;

	private RestClient(Context context) {
		cookieStore = new PersistentCookieStore(context);
		client.setCookieStore(cookieStore);
	}
	
	public void setCookie(BasicClientCookie newCookie) {
		cookieStore.addCookie(newCookie);
	}

	public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

	public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.setEnableRedirects(true);
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_URL + relativeUrl;
	}
	
	public static class Factory {
		
		private static RestClient client = null;
		
		public static RestClient build(Context context) {
			if(client == null) {
				client = new RestClient(context);
			}
			return client;
		}
		
	}
}