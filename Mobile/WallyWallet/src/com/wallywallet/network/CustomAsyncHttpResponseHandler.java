package com.wallywallet.network;

import org.apache.http.Header;

import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wallywallet.activity.facebook.FacebookActivity;

public abstract class CustomAsyncHttpResponseHandler extends AsyncHttpResponseHandler{

	@Override
	public void onStart() {
		Log.i(FacebookActivity.class.getName(), "Start " + this.getRequestURI());
	}
	
    @Override
    public void onRetry(int retryNo) {
    	Log.i(FacebookActivity.class.getName(), "Retry......");
	}
    
	@Override
	public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
		// called when response HTTP status is "4XX" (eg. 401, 403, 404)
		Log.i(FacebookActivity.class.getName(), String.format("Status [%s]", statusCode));
		try {
			Log.i(FacebookActivity.class.getName(), String.format("ErrorResponse [%s]", new String(errorResponse, "UTF-8")));
		} catch (Exception e1) {
			Log.i(FacebookActivity.class.getName(), "Error on convert errorResponse");
		}
	}
	
}