package com.wallywallet.utils;

import android.os.Handler;

public abstract class AsyncHandler extends Handler {
	
	public abstract void postExecute(Boolean isSuccess, String result);
	public abstract void postExecute(Boolean isSuccess, Object result);
}