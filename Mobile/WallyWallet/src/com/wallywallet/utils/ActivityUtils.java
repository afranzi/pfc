package com.wallywallet.utils;

import android.app.Activity;
import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;


public class ActivityUtils {
	
	public static String FACEBOOK_ID       = "FACEBOOK_ID";
	public static String FACEBOOK_TOKEN    = "FACEBOOK_TOKEN";
	public static String FACEBOOK_SESSION  = "FACEBOOK_SESSION";
	public static String FACEBOOK_USER     = "FACEBOOK_USER";
	public static String FACEBOOK_ONLINE   = "FACEBOOK_ONLINE";
	
	public static String CARD_JSON         = "CARD_JSON";
	public static String CARD_PARENT       = "CARD_PARENT";
	public static String CARD_OWNER        = "CARD_OWNER";
	public static String CARD_CREATE       = "CARD_CREATE";
	public static String CARD_OCR          = "CARD_OCR";
	
	public static String CURRENT_USER_ID   = "CURRENT_USER_ID";
	public static String CURRENT_USER_NAME = "CURRENT_USER_NAME";
	
	public static String OCR_TEXT          = "OCR_TEXT";
	
	public static String getTestFromTextView(Activity activity, Integer id) {
		TextView textView = (TextView) activity.findViewById(id);
		String message = textView.getText().toString();
		
		return message;
	}	
	
	public static void showToast(Context context, String message) {
		showToast(context, message, Toast.LENGTH_SHORT);
	}
	
	public static void showToast(Context context, String message, Integer duration) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}