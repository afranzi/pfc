package com.wallywallet.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class FileManager {

	private static final String TAG = "FILE_MANAGER";

	public static String FOLDER_BASE = "/WallyWallet";
	public static String FOLDER_IMAGES = FOLDER_BASE + "/images";
	public static String FOLDER_TESSER = FOLDER_BASE + "/tesseract";
	
	public static Boolean checkIfExists(String folder, String path) {
		File dummy = new File(folder, path);
		return dummy.exists();
	}
	
	public static String mkdir(String path, Context context) {
		String folderPath = null;
		
		/* Manage external storage*/
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
			String externalDirectory = Environment.getExternalStorageDirectory().getPath().replace("emulated/0", "emulated/legacy");

			File folder = new File(externalDirectory, path);
			Log.i(TAG, folder.getAbsolutePath());
			boolean success = true;
			if (!folder.exists()) {
				success = folder.mkdirs();
			}
			if (success) {
				// Do something on success
				return folder.getAbsolutePath();
			} else {
				Toast.makeText(context, String.format("Cannot create folder dir [%s]", path), Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(context, "NO EXTERNAL STORAGE FOUND",Toast.LENGTH_LONG).show();
		}
		
		return folderPath;
	}
	
	/**
	 * Copy the asset at the specified path to this app's data directory. If the
	 * asset is a directory, its contents are also copied.
	 * 
	 * @param path
	 * Path to asset, relative to app's assets directory.
	 */
	public static void copyAsset(String assetPath, String destPath, AssetManager manager) {

		// If we have a directory, we make it and recurse. If a file, we copy its
		// contents.
		try {
			String[] contents = manager.list(assetPath);

			// The documentation suggests that list throws an IOException, but doesn't
			// say under what conditions. It'd be nice if it did so when the path was
			// to a file. That doesn't appear to be the case. If the returned array is
			// null or has 0 length, we assume the path is to a file. This means empty
			// directories will get turned into files.
			if (contents == null || contents.length == 0)
				throw new IOException();

			// Make the directory.
			File dir = new File(destPath, assetPath);
			dir.mkdirs();

			// Recurse on the contents.
			for (String entry : contents) {
				Log.i(TAG, entry);
				copyAsset(assetPath + "/" + entry, destPath, manager);
			}
		} catch (IOException e) {
			copyFileAsset(assetPath, destPath, manager);
		}
	}

	/**
	 * Copy the asset file specified by path to app's data directory. Assumes
	 * parent directories have already been created.
	 * 
	 * @param path
	 * Path to asset, relative to app's assets directory.
	 */
	private static void copyFileAsset(String path, String destPath, AssetManager manager) {
		File file = new File(destPath, path);
		
		Log.i(TAG, String.format("FILE [%s]", file.getAbsolutePath()));
		Log.i(TAG, String.format("DST [%s]", destPath));
		
		if(file.exists()) {
			Log.i(TAG, String.format("File [%s] already exists", file.getAbsolutePath()));
			return;
		}
		try {
			InputStream in = manager.open(path);
			OutputStream out = new FileOutputStream(file);
			byte[] buffer = new byte[1024];
			int read = in.read(buffer);
			while (read != -1) {
				out.write(buffer, 0, read);
				read = in.read(buffer);
			}
			out.close();
			in.close();
		} catch (IOException e) {
			Log.e(TAG, e.getMessage());
		}
	}
}