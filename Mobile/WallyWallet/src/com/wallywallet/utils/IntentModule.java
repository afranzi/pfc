package com.wallywallet.utils;

import org.json.JSONObject;

import android.content.Intent;

import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.google.common.base.Throwables;

public class IntentModule {

	private String facebookID = null;
	private String token = null;
	private Session session = null;
	private GraphUser user = null;
	private Boolean isOnline = false;

	IntentModule(String facebookID, String token, Session session, GraphUser user, Boolean isOnline) {
		this.facebookID = facebookID;
		this.token = token;
		this.session = session;
		this.user = user;
		this.isOnline = isOnline;
	}

	public String getFacebookID() {
		return facebookID;
	}

	public String getToken() {
		return token;
	}

	public Session getSession() {
		return session;
	}

	public GraphUser getUser() {
		return user;
	}

	public Boolean isOnline() {
		return isOnline;
	}

	public static class Factory {

		public static IntentModule build(Intent intent) {
			Boolean isOnline = intent.getBooleanExtra(ActivityUtils.FACEBOOK_ONLINE, false);

			String token = intent.getStringExtra(ActivityUtils.FACEBOOK_TOKEN);
			Session session = (Session) intent.getSerializableExtra(ActivityUtils.FACEBOOK_SESSION);

			String facebookID = "Offline";
			GraphUser user = null;
			if(isOnline) {
				facebookID = intent.getStringExtra(ActivityUtils.FACEBOOK_ID);
				try {
					user = (GraphUser) GraphObject.Factory.create(new JSONObject(intent.getStringExtra(ActivityUtils.FACEBOOK_USER)), GraphUser.class);
				} catch (Exception e) {
					throw Throwables.propagate(e);
				}
			} else {
				//user = (GraphUser) GraphObject.Factory.create(GraphUser.class);
			}

			return new IntentModule(facebookID, token, session, user, isOnline);
		}

		public static void persist(IntentModule module, Intent intent) {
			persist(module.user, module.token, module.session, module.isOnline, intent);
		}

		public static void persist(GraphUser user, String token, Session session, Boolean isOnline, Intent intent) {
			intent.putExtra(ActivityUtils.FACEBOOK_ONLINE, isOnline);
			intent.putExtra(ActivityUtils.FACEBOOK_TOKEN, token);
			intent.putExtra(ActivityUtils.FACEBOOK_SESSION, session);
			if(isOnline) {
				intent.putExtra(ActivityUtils.FACEBOOK_ID, user.getId());
				intent.putExtra(ActivityUtils.FACEBOOK_USER, user.getInnerJSONObject().toString());
			}
		}
	}
}